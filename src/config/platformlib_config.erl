%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.05.2021
%%% @doc Facade to cluster configuration. Implements caching.

-module(platformlib_config).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([]).
-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------------
%% Return current configuration hashsum
%% --------------------------------------------
- spec get_cfg_hash() -> non_neg_integer().
%% --------------------------------------------
get_cfg_hash() ->
    ?CFGF:get_cfg_hash().

%% --------------------------------------------
%% Return current site name
%% --------------------------------------------
- spec get_current_site() -> [MsvcData::map()].
%% --------------------------------------------
get_current_site() ->
    <<"main_site">>.

%% --------------------------------------------
%% Return current node name
%% --------------------------------------------
- spec get_current_node_name() -> NodeName::string().
%% --------------------------------------------
get_current_node_name() ->
    lists:takewhile(fun($@) -> false; (_) -> true end, atom_to_list(node())).

%% --------------------------------------------
%% Return list of configured microservices data
%% --------------------------------------------
- spec get_nodes_data() -> [NodeData::map()].
%% --------------------------------------------
get_nodes_data() ->
    ?CFGF:get_nodes_data().

%% --------------------------------------------
%% Return all configured nodes
%% --------------------------------------------
- spec get_nodes() -> [Node::node()].
%% --------------------------------------------
get_nodes() ->
    F = fun() -> lists:map(fun(M) -> maps:get(<<"node">>,M) end, get_nodes_data()) end,
    cache(?FUNCTION_NAME, [], F).

%% --------------------------------------------
%% Return nodes where selected microservice is configured.
%% --------------------------------------------
- spec get_nodes_by_msvc(Msvc::binary()) -> [Node::node()].
%% --------------------------------------------
get_nodes_by_msvc(MsvcType) ->
    F = fun() -> lists:map(fun(M) -> maps:get(<<"node">>,M) end,
                           lists:filter(fun(M) ->
                                            Msvcs = maps:get(<<"microservices">>,M),
                                            lists:member(MsvcType, lists:map(fun(Msvc) -> maps:get('type',Msvc) end, Msvcs))
                                        end, get_nodes_data()))
        end,
    cache(?FUNCTION_NAME, [MsvcType], F).

%% --------------------------------------------
%% Return nodes where selected microservice is configured.
%% --------------------------------------------
- spec get_nodes_data_by_msvc(Msvc::binary()) -> [Node::node()].
%% --------------------------------------------
get_nodes_data_by_msvc(MsvcType) ->
    F = fun() -> lists:filter(fun(M) ->
                                  Msvcs = maps:get(<<"microservices">>,M),
                                  lists:member(MsvcType, lists:map(fun(Msvc) -> maps:get('type',Msvc) end, Msvcs))
                              end, get_nodes_data())
        end,
    cache(?FUNCTION_NAME, [MsvcType], F).

%% --------------------------------------------
%% Return opts of all samples of selected microservice type.
%% --------------------------------------------
- spec get_msvc_opts(Msvc::binary()) -> [{Node::node(),Params::map()}].
%% --------------------------------------------
get_msvc_opts(MsvcType) ->
    F = fun() -> lists:filtermap(fun(M) ->
                                    Msvcs = maps:get(<<"microservices">>,M),
                                    case [MsvcItem || MsvcItem <- Msvcs, maps:get('type',MsvcItem)==MsvcType] of
                                        [] -> false;
                                        [MsvcItem] -> {true,{maps:get(<<"node">>,M),MsvcItem}}
                                    end end, get_nodes_data())
        end,
    cache(?FUNCTION_NAME, [MsvcType], F).

%% --------------------------------------------
%% Return opts of selected microservice type, configured on selected node
%% --------------------------------------------
- spec get_msvc_opts(Node::node(),Msvc::binary()) -> {ok,map()} | false.
%% --------------------------------------------
get_msvc_opts(Node,MsvcType) ->
    F = fun() ->
            All = get_msvc_opts(MsvcType),
            ?BU:get_by_key(Node,All,false)
        end,
    cache(?FUNCTION_NAME, [MsvcType], F).

%% --------------------------------------------
%% Return current server options
%% --------------------------------------------
get_server_info() ->
    ?CFGF:get_current_server_info().

%% --------------------------------------------
%% Return alias value
%% --------------------------------------------
- spec get_alias_value(Alias::binary()) -> {ok,Value::term()} | {error,Reason::term()}.
%% --------------------------------------------
get_alias_value(Alias) ->
    F = fun() -> ?CFGF:get_alias_value(Alias) end,
    cache(?FUNCTION_NAME, [Alias], F).


%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------
%% Caching for complex functions
%% -------------------------------
cache(FunName,Args,Fun) ->
    H = get_cfg_hash(),
    Key = {config,FunName,Args},
    case ?BLstore:find_u(Key) of
        {_,{H,V}} -> V;
        _ ->
            V = Fun(),
            ?BLstore:store_u(Key,{H,V}),
            V
    end.