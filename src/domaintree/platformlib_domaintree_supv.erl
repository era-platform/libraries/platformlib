%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.05.2021
%%% @doc DomainTree helper general supervisor
%%%   Checks callback module and always starts root domain (child supv). Over it whole domain tree is loaded.
%%%   Root domain srv attached to DMS root domain, and if 'domains' collection found, then starts subdomain supervisor for every item in collection.
%%%   Every server subscribes for domains collection, apply changes in OTP tree recursively.
%%%   Allows to resync all data by facade call. Sometimes resync automatically.
%%%   If domain's dms is not available then doesn't changes anything. Removes domain only if it was deleted from parent's collection.
%%%   Start parameters:
%%%       CallbackModule
%%%       Opts
%%%   CallbackModule should export:
%%%       app(Opts::map()) -> binary() | atom().
%%%       log(Opts::map(),Fmt,Args) -> ok.
%%%       domain_child_spec(Opts::map(),Domain::binary() | atom()) -> child_spec().
%%%   CallbackModule may export:
%%%       filter_domain(Opts::map(),Domain::binary() | atom(), DomainItem::map()) -> boolean().
%%%   Opts may contain:
%%%       fast_mode :: boolean() (=false) - if fast then no cache global-names used, all timeouts are minimal and growing. If not then caching global_names and timeout is higher.
%%%       resync_timeout :: non_neg_integer() | 'infinity' - automatically force resync timeout
%%%       ANY :: term() could be accessed in callback functions.

-module(platformlib_domaintree_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/2]).
-export([init/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------
%% OTP start_link
%% ---------------------------------------
-spec start_link(CallbackModule::atom(),Opts::map()) -> {ok,Pid::pid()} | ignore | {error,Reason::term()}.
%% ---------------------------------------
start_link(CallbackModule,Opts) when is_atom(CallbackModule), is_map(Opts) ->
    case check_exports(CallbackModule) of
        ok ->
            Opts1 = Opts#{'callback_module' => CallbackModule},
            supervisor:start_link(?MODULE, Opts1);
        {error,_}=Err -> Err
    end.

%% ====================================================================
%% Callback functions (supervisor)
%% ====================================================================

init(Opts) ->
    CallbackModule = maps:get('callback_module',Opts),
    Domain = 'root',
    Opts1 = Opts#{'domain' => Domain},
    ChildSpec = [{Domain, {?DTREE_DSUPV, start_link, [Opts1]}, permanent, 1000, supervisor, [?DTREE_DSUPV]}],
    App = CallbackModule:app(Opts),
    CallbackModule:log(Opts,"~ts. DomainTree supervisor inited", [App]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
check_exports(CallbackModule) ->
    Fs = [{log,3},
          {app,1},
          {domain_child_spec,2}],
    Res = lists:foldl(fun({Fname,Arity}=Finfo,true) ->
                            case ?BU:function_exported(CallbackModule,Fname,Arity) of
                                true -> true;
                                false -> {false,Finfo}
                            end;
                         (_,Acc) -> Acc
                      end, true, Fs),
    case Res of
        true -> ok;
        {false,Finfo} ->
            {Fname,Arity} = Finfo,
            {error, ?BU:str("Expected domaintree callback module should export '~ts'/~p",[Fname,Arity])}
    end.