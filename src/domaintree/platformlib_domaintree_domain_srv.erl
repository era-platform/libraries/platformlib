%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.06.2021
%%% @doc DomainTree node supervisor
%%%   Attaches to DMS domain, and if its internal 'domains' collection found, then starts subdomain supervisor for every item in this collection.
%%%   Allows to resync all data by facade call. Sometimes resync automatically.
%%%   If domain's dms is not available then doesn't change anything. Removes domain only if it was deleted from parent's collection.
%%%     Opts:
%%%         callback_module - module for callbacks
%%%         domain - current domain
%%%         supv - supervisor for subdomains
%%%     CallbackModule should export:
%%%         app(Opts::map()) -> binary() | atom().
%%%         log(Opts::map(),Fmt,Args) -> ok.
%%%     CallbackModule may contain:
%%%         filter_domain(Opts::map(),Domain::binary() | atom(), DomainItem::map()) -> boolean().

-module(platformlib_domaintree_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-record(state, {
    callback_module :: atom(),
    opts :: map(),
    domain :: binary() | atom(),
    sync_ref :: reference(),
    supv :: atom(),
    ref :: reference(),
    subscrid_classes :: binary(),
    subscrid_domains :: binary(),
    hc_classes :: non_neg_integer(),
    hc_domains :: non_neg_integer(),
    items_classes :: [map()],
    items_domains :: [map()],
    status :: initial | inactive | active,
    timeout_candidate :: non_neg_integer(),
    timerref :: reference()
}).

-define(TimeoutInactive, 300000).
-define(TimeoutActive, 300000).
-define(TimeoutFastRetry, 1000).
-define(TimeoutLongRetry, 30000).
-define(TimerOnReload, 2000).

%% ====================================================================
%% Public functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    CallbackModule = ?BU:get_by_key('callback_module',Opts),
    App = CallbackModule:app(Opts),
    Domain = ?BU:get_by_key('domain',Opts),
    SupvForDomains = ?BU:get_by_key('supv',Opts),
    {Self, SyncRef} = {self(), make_ref()},
    Ref = make_ref(),
    State1 = #state{callback_module = CallbackModule,
                    opts = Opts,
                    domain = Domain,
                    sync_ref = SyncRef,
                    supv = SupvForDomains,
                    subscrid_classes = ClassesSubscrId=?BU:strbin("sid_DOMAINTREE:~p;n:~ts;dom:~ts;cn:~ts",[node(),App,Domain,?ClassesCN]),
                    subscrid_domains = DomainsSubscrId =?BU:strbin("sid_DOMAINTREE:~p;n:~ts;dom:~ts;cn:~ts",[node(),App,Domain,?DomainsCN]),
                    items_domains = [],
                    items_classes = [],
                    status = 'initial',
                    ref=Ref,
                    timerref = erlang:send_after(0, self(), {timer,'initial',Ref})},
    % ----------------
    SubscrOptsClasses = #{<<"filter">> => classes_filter(),
                          <<"mask">> => classes_mask(),
                          fun_report => fun(Report) -> Self ! {'subscriber_report',SyncRef,Report} end},
    SubscrOptsDomains = #{fun_report => fun(Report) -> Self ! {'subscriber_report',SyncRef,Report} end},
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?ClassesCN,self(),ClassesSubscrId,SubscrOptsClasses),
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?DomainsCN,self(),DomainsSubscrId,SubscrOptsDomains),
    CallbackModule:log(Opts,"~ts. DomainTree '~ts' srv inited", [App, Domain]),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {reply, undefined, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% --------------
%% on timer expired. Mode:
%%   'initial' - before dms ensured for controlled classes
%%   'actual' - after dms ensured successfully
handle_info({timer,Status,Ref}, #state{ref=Ref,domain=Domain,opts=Opts}=State) ->
    {Status1,State3} =
        case ?PCFG:get_nodes_by_msvc(?MsvcDc) of
            [] ->
                State2 = case State of
                             #state{items_classes=[],hc_classes=-1,items_domains=[],hc_domains=-1} -> State;
                             _ ->
                                 State1 = stop_domains(State),
                                 State1#state{items_classes=[],hc_classes=-1,items_domains=[],hc_domains=-1}
                         end,
                {'inactive',State2#state{timeout_candidate=?TimeoutInactive}};
            [_|_] ->
                ?LOG('$trace',"DTREE '~ts' checking classes coll...",[Domain]),
                case ?DMS_CACHE_SYNC:check_sync(Domain,?ClassesCN,sync_opts(State)) of
                    {error,_} -> {Status,State#state{timeout_candidate=?TimeoutFastRetry}};
                    {ok,_} ->
                        ?LOG('$trace',"DTREE '~ts' loading classes...",[Domain]),
                        case load_classes(State) of
                            {retry,State1} -> {Status,State1#state{timeout_candidate=?TimeoutLongRetry}};
                            {false,State1} ->
                                State2 = stop_domains(State1),
                                TimeoutInactive = maps:get('resync_timeout',Opts,?TimeoutInactive),
                                {'inactive',State2#state{timeout_candidate=TimeoutInactive}};
                            {true,State1} ->
                                ?LOG('$trace',"DTREE '~ts' checking domains coll...",[Domain]),
                                case ?DMS_CACHE_SYNC:check_sync(Domain,?DomainsCN,sync_opts(State)) of
                                    {error,_} -> {Status,State#state{timeout_candidate=?TimeoutFastRetry}};
                                    {ok,_} ->
                                        ?LOG('$trace',"DTREE '~ts' loading domains",[Domain]),
                                        TimeoutActive = maps:get('resync_timeout',Opts,?TimeoutActive),
                                        case load_domains(State1#state{timeout_candidate=TimeoutActive}) of
                                            {retry,State2} -> {'active',State2#state{timeout_candidate=?TimeoutLongRetry}};
                                            {not_changed,State2} -> {'active',State2};
                                            {ok,State2} ->
                                                ?LOG('$trace',"DTREE '~ts' LOADED DOMAINS!",[Domain]),
                                                {'active',State2}
                                        end end end end end,
    ?LOG('$trace',"DTREE '~ts' load iteration finished",[Domain]),
    Ref1 = make_ref(),
    TimerRef1 = case State3#state.timeout_candidate of
                    infinity -> undefined;
                    Timeout -> erlang:send_after(Timeout, self(), {timer,Status1,Ref1})
                end,
    State4 = State3#state{status = Status1,
                          ref = Ref1,
                          timerref = TimerRef1},
    {noreply, State4};

%% --------------
%% subscribe helper report (state changed)
handle_info({'subscriber_report',SyncRef,Report}, #state{sync_ref=SyncRef,timerref=TimerRef}=State) ->
    State1 = case Report of
                 ok ->
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#state{status = 'active',
                                 ref = Ref1,
                                 timerref = erlang:send_after(0, self(), {timer,'active',Ref1})};
                 _ -> State
             end,
    {noreply, State1};

%% --------------
%% when something changed in 'classes' collection on dms
handle_info({notify,SubscrId,EventInfo}, #state{domain=Domain,subscrid_classes=SubscrId,timerref=TimerRef}=State) ->
    ?LOG('$trace', "DTREE '~ts' notified: ~120tp", [Domain, EventInfo]),
    Data = maps:get(<<"data">>,EventInfo),
    ?ClassesCN = maps:get(<<"classname">>,Data),
    ?BU:cancel_timer(TimerRef),
    Ref1 = make_ref(),
    State1 = State#state{status = 'active',
                         ref = Ref1,
                         timerref = erlang:send_after(0, self(), {timer,'active',Ref1})},
    {noreply,State1};

%% --------------
%% when something changed in 'domains' collection on dms
handle_info({notify,SubscrId,EventInfo}, #state{domain=Domain,subscrid_domains=SubscrId,items_domains=ItemsDomains,timerref=TimerRef}=State) ->
    ?LOG('$trace', "DTREE '~ts' notified: ~120tp", [Domain, EventInfo]),
    Data = maps:get(<<"data">>,EventInfo),
    ?DomainsCN = maps:get(<<"classname">>,Data),
    Operation = maps:get(<<"operation">>,Data),
    State2 = case Operation of
                 'create' ->
                     ItemNew = maps:get(<<"entity">>,Data),
                     State1 = State#state{items_domains=[ItemNew|ItemsDomains]},
                     start_domain(ItemNew,State1);
                 'update' ->
                     ItemNew = maps:get(<<"entity">>,Data),
                     Id = maps:get(<<"id">>,ItemNew),
                     case lists:filter(fun(Item) -> maps:get(<<"id">>,Item)==Id end, ItemsDomains) of
                         [] ->
                             State1 = State#state{items_domains=[ItemNew|ItemsDomains]},
                             start_domain(ItemNew,State1);
                         [ItemOld] ->
                             State1 = State#state{items_domains=[ItemNew|lists:delete(ItemOld,ItemsDomains)]},
                             update_domain(ItemOld,ItemNew,State1)
                     end;
                 'delete' ->
                     ItemOld = maps:get(<<"entity">>,Data),
                     Id = maps:get(<<"id">>,ItemOld),
                     State1 = State#state{items_domains=lists:filter(fun(Item) -> maps:get(<<"id">>,Item)/=Id end,ItemsDomains)},
                     stop_domain(maps:get(<<"entity">>,Data),State1);
                 'clear' ->
                     stop_domains(State);
                 O when O=='reload';O=='corrupt' ->
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#state{status = 'active',
                                 ref = Ref1,
                                 timerref = erlang:send_after(?TimerOnReload, self(), {timer,'active',Ref1})}
             end,
    {noreply,State2};

%% --------------
%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
classes_filter() -> [<<"==">>,[<<"property">>,<<"classname">>],<<"domains">>].

%% @private
classes_mask() -> [<<"id">>,<<"name">>].

%% @private
sync_opts(#state{opts=Opts}) ->
    case maps:get('fast_mode',Opts,false) of
        true ->
            F = fun(Acc) when not is_integer(Acc) -> 10;
                   (Acc) when Acc*2 > 200 -> 200;
                   (Acc) -> Acc*2
                end,
            #{'cache_globalname' => false,
              'timeout_sync' => 5000,
              'attempts' => 20,
              'timeout_between_attempts' => F};
        false ->
            #{'cache_globalname' => true,
              'timeout_sync' => 5000,
              'attempts' => 10,
              'timeout_between_attempts' => 200}
    end.

%% @private
load_classes(#state{domain=Domain,hc_classes=HC,items_classes=Items}=State) ->
    %Owner = <<"owner">> => {self(),node(),Domain,State#state.opts},
    ReadOpts = #{<<"filter">> => classes_filter(),
                 <<"mask">> => classes_mask()},
    case ?DMS_CACHE_SYNC:read_cache_sync(Domain,?ClassesCN,ReadOpts,HC,sync_opts(State)) of
        {error,_}=Err ->
            ?LOG('$error',"DTREE '~ts' read classes error: ~120tp",[Domain,Err]),
            {retry,State};
        {ok,not_changed} when Items==[] -> {false,State};
        {ok,not_changed} -> {true,State};
        {ok,[],HC1} -> {false,State#state{items_classes=[],hc_classes=HC1}};
        {ok,Items1,HC1} -> {true,State#state{items_classes=Items1,hc_classes=HC1}}
    end.

%% @private
load_domains(#state{domain=Domain,hc_domains=HC,items_domains=Items}=State) ->
    case ?DMS_CACHE_SYNC:read_cache_sync(Domain,?DomainsCN,#{<<"order">> => [<<"name">>]},HC,sync_opts(State)) of
        {error,_}=Err ->
            ?LOG('$error',"DTREE '~ts' read domains error: ~120tp",[Domain,Err]),
            {retry,State};
        {ok,not_changed} -> {not_changed,State};
        {ok,Items1,HC1} ->
            State1 = State#state{items_domains=Items1,hc_classes=HC1},
            {ok,Created,Deleted,_NonModified,ModifiedEx} = ?BU:juxtapose_ex(Items,Items1,fun(Item) -> maps:get(<<"name">>,Item) end),
            State2 = lists:foldl(fun(Item,StateX) -> start_domain(Item,StateX) end, State1, Created),
            State3 = lists:foldl(fun(Item,StateX) -> stop_domain(Item,StateX) end, State2, Deleted),
            State4 = lists:foldl(fun({ItemOld,ItemNew},StateX) -> update_domain(ItemOld,ItemNew,StateX) end, State3, ModifiedEx),
            {ok,State4}
    end.

%%% ---------------
%% @private
stop_domains(#state{items_domains=ItemsDomains}=State) ->
    State1 = lists:foldl(fun(Item,StateX) -> stop_domain(Item,StateX) end, State, ItemsDomains),
    State1#state{items_domains=[],hc_domains=-1}.

%%% ---------------
%% @private
%% prepare start params (readonly_opts -> read-only in-domain settings), start domain_supv
start_domain(DomainItem,#state{callback_module=CallbackModule,opts=Opts}=State) ->
    Subdomain = maps:get(<<"name">>,DomainItem),
    case ?BU:function_exported(CallbackModule,filter_domain,3) of
        true ->
            case CallbackModule:filter_domain(Opts,Subdomain,DomainItem) of
                true -> start_domain_1(DomainItem,State);
                false -> State
            end;
        false ->
            start_domain_1(DomainItem,State)
    end.

%% @private
start_domain_1(DomainItem,#state{callback_module=CallbackModule,opts=Opts,supv=SupvForDomains,items_domains=ItemsDomains}=State) ->
    App = CallbackModule:app(Opts),
    Subdomain = maps:get(<<"name">>,DomainItem),
    SubOpts = Opts#{domain => Subdomain},
    RegKey = reg_key(Subdomain),
    ChildSpec = {RegKey,{?DTREE_DSUPV,start_link,[SubOpts]},permanent,1000,supervisor,[?DTREE_DSUPV]},
    CallbackModule:log(Opts,"~ts. DomainTree starting child '~ts'.",[App,Subdomain]),
    case supervisor:start_child(SupvForDomains,ChildSpec) of
        {ok,_Child} -> State;
        {ok,_Child,_Info} -> State;
        {error,already_present} ->
            ?LOG('$warning',"~ts. DomainTree add supervisor child '~ts' error: already_present. Trying to terminate and restart.",[App,Subdomain]),
            catch supervisor:terminate_child(SupvForDomains,RegKey),
            catch supervisor:delete_child(SupvForDomains,RegKey),
            start_domain(DomainItem,State);
        {error,{already_started,Reason}} ->
            ?LOG('$warning',"~ts. DomainTree add supervisor child '~ts' error: already_started ~120tp. Trying to terminate and restart.",[App,Subdomain,Reason]),
            catch supervisor:terminate_child(SupvForDomains,RegKey),
            catch supervisor:delete_child(SupvForDomains,RegKey),
            start_domain(DomainItem,State);
        {error,_}=Err ->
            ?LOG('$crash',"~ts. DomainTree add supervisor child '~ts' error: ~n\t~120tp",[App,Subdomain,Err]),
            State#state{hc_domains=-1,items_domains=lists:delete(DomainItem,ItemsDomains)}
    end.

%%% ---------------
%% @private
%% stop domain_supv.
stop_domain(DomainItem,#state{supv=SupvForDomains,callback_module=CallbackModule,opts=Opts}=State) ->
    Subdomain = maps:get(<<"name">>,DomainItem),
    RegKey = reg_key(Subdomain),
    catch supervisor:terminate_child(SupvForDomains,RegKey),
    catch supervisor:delete_child(SupvForDomains,RegKey),
    App = CallbackModule:app(Opts),
    CallbackModule:log(Opts,"~ts. DomainTree '~ts' supv stopped.",[App,Subdomain]),
    State.

%%% ---------------
%% @private
%% check and restart supv if significant properties have been changed
update_domain(DomainItemOld,DomainItemNew,#state{}=State) ->
    case lists:filter(fun(K) -> maps:get(K,DomainItemOld,undefined) /= maps:get(K,DomainItemNew,unefined) end,
                      [<<"name">>,<<"readonly_options">>,<<"master_dbstrings">>])
    of
        [] -> State;
        _ ->
            State1 = stop_domain(DomainItemOld,State),
            start_domain(DomainItemNew,State1)
    end.

%% @private
reg_key(Domain) -> ?BU:to_atom_new(Domain).