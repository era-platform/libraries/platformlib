%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.06.2021
%%% @doc DomainTree OTP domain supervisor
%%%   Attached to DMS root domain, and if 'domains' collection found, then starts subdomain supervisor for every item in collection.
%%%   Always starts supervisor for root domain.
%%%   Subscribes for domains collection, apply changes in OTP tree.
%%%   Allows to resync all data by facade call. Sometimes resync automatically.
%%%   If domain's dms is not available then doesn't changes anything. Removes domain only if it was deleted from parent's collection

%%%     Opts:
%%%         callback_module
%%%         domain
%%%     CallbackModule should export:
%%%         app(Opts::map()) -> binary() | atom()
%%%         log(Opts::map(),Fmt::string(),Args::list()) -> ok
%%%         domain_child_spec(Opts::map(),Domain::binary() | atom()) -> child_spec().

-module(platformlib_domaintree_domain_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1]).
-export([init/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------
%% OTP start_link, when regname is automatically generated
%% ---------------------------------------
-spec start_link(Opts::map()) -> {ok,pid()} | ignore | {error,Reason::term()}.
%% ---------------------------------------
start_link(Opts) ->
    Domain = ?BU:get_by_key(domain,Opts),
    CallbackModule = ?BU:get_by_key(callback_module,Opts),
    App = CallbackModule:app(Opts),
    RegName = reg_name(App,Domain),
    supervisor:start_link({local,RegName}, ?MODULE, Opts#{'supv' => RegName}).

%% @private
reg_name(App,Domain) ->
    ?BU:to_atom_new(?BU:strbin("dtree_supv;~ts;dom:~ts",[App,Domain])).

%% ====================================================================
%% Callback functions (supervisor)
%% ====================================================================

init(Opts) ->
    CallbackModule = ?BU:get_by_key('callback_module',Opts),
    Domain = ?BU:get_by_key('domain',Opts),
    TreeChildSpec = {'$srv', {?DTREE_DSRV, start_link, [Opts]}, permanent, 1000, worker, [?DTREE_DSRV]},
    App = CallbackModule:app(Opts),
    SubChildSpec0 = CallbackModule:domain_child_spec(Opts,Domain),
    DefaultSpec = #{restart => permanent,
                    shutdown => 1000,
                    type => supervisor,
                    modules => []},
    SubChildSpec = maps:merge(DefaultSpec, maps:merge(SubChildSpec0, #{id => '$child'})),
    CallbackModule:log(Opts,"~ts. DomainTree '~ts' supervisor inited", [App,Domain]),
    {ok, {{one_for_one, 10, 2}, [TreeChildSpec,SubChildSpec]}}.
