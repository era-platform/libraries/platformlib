%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.05.2021
%%% @doc Leader app common utils

-module(platformlib_leader_app_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_app_nodes/2, get_app_nodes/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% --------------------------------------
%% Return sorted list of nodes, where selected msvc is configured.
%%   Checks contains current node.
%%   Filters by current node's group, sorted by order.
%% --------------------------------------
get_app_nodes(App, MsvcType) -> get_app_nodes(App, MsvcType, undefined).
get_app_nodes(App, MsvcType, FunLog) ->
    F = fun() ->
            Group = ?BU:get_env(App,'group',undefined),
            MsvcsList = ?PCFG:get_msvc_opts(MsvcType),
            NodesOrders = lists:filtermap(fun({Node,Params}) ->
                                                case maps:get('group',Params,undefined) of
                                                    Group -> {true,{maps:get('order',Params,undefined),erlang:phash2(Node),Node}};
                                                    _ -> false
                                                end end, MsvcsList),
            SortedNodes = lists:map(fun({_,_,Node}) -> Node end, NodesOrders),
            case lists:member(node(),SortedNodes) of
                false -> throw({error,current_node_absent});
                true when is_function(FunLog,2) ->
                    FunLog("Nodes: ~120tp", [SortedNodes]),
                    SortedNodes;
                true ->
                    SortedNodes
            end end,
    ?PCFG:cache({?MODULE,?FUNCTION_NAME},[MsvcType],F).

%% ====================================================================
%% Internal functions
%% ====================================================================