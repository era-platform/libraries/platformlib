%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.05.2021
%%% @doc Leader app supervisor.
%%%      Should be added as general app supv to apps, when failover-takeover is necessary.
%%%      Main app's supv would be added as child only on leader after leader-selection.
%%%    Start params:
%%%        AppModule -
%%%        RegName -
%%%    Requires app module callback to export:
%%%      - get_app_nodes/0
%%%      - get_top_supv_mfa/1 (RemoteState) - should store and use when first takeover start
%%%      - log/2 (Fmt,Args)
%%%    Optional app module callbacks:
%%%      - get_takeover_state/1 (FromNode)
%%%      - before_start/0
%%%      - after_start/0
%%%      - before_stop/0
%%%      - after_stop/0

-module(platformlib_leader_app_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1, start_link/2]).
-export([init/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(SupvKey, top_supv).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------
%% OTP start_link, when regname is automatically generated
%% ---------------------------------------
-spec start_link(AppModule::atom()) -> {ok,Pid::pid()} | {error,Reason::term()}.
%% ---------------------------------------
start_link(AppModule) ->
    start_link(AppModule,reg_name_srv(AppModule)).

%% ---------------------------------------
%% OTP start_link, when regname is passed
%% ---------------------------------------
-spec start_link(AppModule::atom(),RegName::atom()) -> {ok,Pid::pid()} | {error,Reason::term()}.
%% ---------------------------------------
start_link(AppModule,RegName) when is_atom(AppModule), is_atom(RegName) ->
    case check_exports(AppModule) of
        ok ->
            Opts = #{app_module => AppModule,
                     reg_name => RegName},
            SupvName = reg_name_supv(AppModule),
            supervisor:start_link({local,SupvName}, ?MODULE, Opts);
        {error,_}=Err -> Err
    end.

%% @private
reg_name_supv(App) ->
    ?BU:to_atom_new(?BU:strbin("~ts_leader_app_supv",[App])).

%% @private
reg_name_srv(App) ->
    ?BU:to_atom_new(?BU:strbin("~ts_leader_app_srv",[App])).

%% ====================================================================
%% Callback functions (supervisor)
%% ====================================================================

init(Opts) ->
    AppModule = maps:get(app_module,Opts),
    RegName = maps:get(reg_name,Opts),
    FunTOS = case ?BU:function_exported(AppModule,get_takeover_state,1) of
                 true -> fun(FromNode) -> AppModule:get_takeover_state(FromNode) end;
                 false -> undefined
             end,
    FunStart = fun(RemoteState) -> start_top_supv(AppModule,RegName,RemoteState) end,
    FunStop = fun() -> stop_top_supv(AppModule,RegName) end,
    StartArgs = [{regname, RegName},
                 {fun_nodes, fun AppModule:get_app_nodes/0},
                 {fun_start, FunStart},
                 {fun_stop, FunStop},
                 {ping_timeout, 2000},
                 {heartbeat_timeout, 1000},
                 {autosort, false},
                 {waittimes, 5},
                 {monitor, true},
                 {dbl_restart_master, true},
                 {heartbeat, true},
                 {takeover, true},
                 {fun_takeover_state, FunTOS},
                 {fun_out, fun AppModule:log/2}],
    ChildSpec = [{RegName, {?LeaderSrv, start_link, [StartArgs]}, permanent, 1000, worker, [?LeaderSrv]}],
    AppModule:log("LEADER '~ts'. Application supervisor inited (~p)", [RegName, self()]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
check_exports(AppModule) ->
    Fs = [{get_top_supv_mfa,1},
          {get_app_nodes,0},
          {log,2}],
    Res = lists:foldl(fun({Fname,Arity}=Finfo,true) ->
        case ?BU:function_exported(AppModule,Fname,Arity) of
            true -> true;
            false -> {false,Finfo}
        end;
        (_,Acc) -> Acc
                      end, true, Fs),
    case Res of
        true -> ok;
        {false,Finfo} ->
            {Fname,Arity} = Finfo,
            {error, ?BU:str("Expected application leader callback module should export '~ts'/~p",[Fname,Arity])}
    end.

%% @private
start_top_supv(AppModule,RegName,RemoteState) ->
    AppModule:log("LEADER '~ts'. Starting application master...", [RegName]),
    Fok = fun() ->
                AppModule:log("LEADER '~ts'. Application master started", [RegName]),
                do_after_start(AppModule),
                ok
          end,
    {M,F,A}= AppModule:get_top_supv_mfa(RemoteState),
    ChildSpec = {?SupvKey, {M,F,A}, permanent, 1000, supervisor, [M]},
    SupvName = reg_name_supv(AppModule),
    do_before_start(AppModule),
    case catch supervisor:start_child(SupvName, ChildSpec) of
        {'EXIT',Err} ->
            AppModule:log("LEADER '~ts'. Application master start exception: ~120tp. Stop node..", [RegName, Err]),
            ?BU:stop_node(1000);
        {error,_}=Err ->
            AppModule:log("LEADER '~ts'. Application master start error: ~120tp, Stop node..", [RegName, Err]),
            ?BU:stop_node(1000);
        {ok,_} -> Fok();
        {ok,_,_} -> Fok()
    end.

% @private
stop_top_supv(AppModule,RegName) ->
    SupvName = reg_name_supv(AppModule),
    do_before_stop(AppModule),
    catch supervisor:terminate_child(SupvName,?SupvKey),
    catch supervisor:delete_child(SupvName,?SupvKey),
    do_after_stop(AppModule),
    AppModule:log("LEADER '~ts'. Application master stopped", [RegName]).

%% ----------------------------

% callback before start
do_before_start(App) ->
    call(App,before_start,[]).

% callback after start
do_after_start(App) ->
    call(App,after_start,[]).

% callback before stop
do_before_stop(App) ->
    call(App,before_stop,[]).

% callback after start
do_after_stop(App) ->
    call(App,after_stop,[]).

%% ------------------------
%% @private
%% callback on some
call(App,Fun,Args) ->
    case ?BU:function_exported(App,Fun,length(Args)) of
        true -> erlang:apply(App,Fun,Args);
        false -> ok
    end.

