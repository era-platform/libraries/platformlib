%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2021
%%% @doc Implements service of unique strategy running. (choosing leader and activating it)
%%%        For example several nodes should share only one example of strategy gen_srv.
%%%        Then current srv executes and sync by configuration other nodes and applies state
%%%        Use mini fsm of undefined=inactive|'started'|'active'
%%%        Could be attached to any supv by local name. So any local name should not be used more than one time on node.
%%%        Options on start:
%%%             fun_nodes (/0)				 % () -> [Node]. Function to get synchronized nodes in sequence of decreasing priority.
%%%             fun_start (/0|/1)			 % ()|(RemoteState) -> ok | {error,Reason::term()}. Function to activate current example.
%%%             fun_stop (/0)				 % () -> ok. Function to deactivate current example on takeover.
%%%             [regname (for local naming)] % default ?MODULE=basiclib_leader_svc_srv. Should be used to allow several servers in node.
%%%             [waittimes],				 % default 0. How many times ping to active node could fail before node starts.
%%%             [ping_timeout (ms)]			 % default 1000. Interval for wait after last activity before ping. Every activity restarts timer.
%%%             [heartbeat],     			 % default false. Uses heartbeat from active to slaves.
%%%             [heartbeat_timeout],	     % default 500. Timeout between heartbeat from active to slaves. Should be less than ping_timeout.
%%%             [monitor],			    	 % default false. Use erlang:monitor_node(ActiveNode).
%%%             [autosort], 			     % default true. Set to use autosort nodes by name instead of direct sequence from fun_nodes().
%%%             [dbl_restart_master],	     % default false. When found 2 active nodes, then secondary priority uses restart to reconnect as slave.
%%%             [takeover],		        	 % default false. Uses takeover mechanism by more prioritized node on start.
%%%             [fun_takeover_state (/1)],	 % default undefined, (FromNode::atom()) -> RemoteState::term(). Returns state from active node on takeover.
%%%             [fun_out (/2)],			     % default undefined, (Fmt::string(),Args::list()) -> ok. Write to log/console operation.

-module(platformlib_leader_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([get_state/2, check_node/2, takeover/2, activate_takeover/2, heartbeat/2, print/1, get_active_node/1]).

-export([debug_timeout/2,
         debug_check_disconnect_to_all/1,
         debug_throw/1]).

%% ====================================================================
%% Define
%% ====================================================================

-record(funs, {
    fun_nodes,
    fun_start,
    fun_stop,
    fun_takeover_state,
    fun_out
}).

-record(opts, {
    ping_timeout=1000,
    heartbeat_timeout=500,
    use_monitor=false,
    use_heartbeat=false,
    use_takeover=false,
    dbl_restart_master=false,
    use_waittimes=0,
    autosort=true
}).

-record(state, {
    regname,
    state = started,
    statestartts = 0,
    ets,
    funs=#funs{},
    opts=#opts{},
    last_nodes=[],
    active_node=undefined,
    wait_times=0,
    monitors=[],
    inited=false,
    ping_ref,
    takeover_ref,
    checknode_ref,
    heartbeat_ref,
    activate_ref,
    timerref
}).

-include("app.hrl").

-define(PING_TIMEOUT, 1000).
-define(PING_MSG, {ping_timer}).
-define(HEARTBEAT_TIMEOUT, 500).
-define(HEARTBEAT_MSG, {heartbeat_timer}).
-define(QUERY_TIMEOUT, 1000).
-define(TAKEOVER_TIMEOUT, 3000).

-define(OUTF(State,Fmt,Args), out(State,Fmt,Args)).

%% ====================================================================
%% API functions
%% ====================================================================

%% start gen_srv and link to supv or owner
% regname (for local naming)
% fun_nodes (/0)
% fun_start (/0 | /1)
% fun_stop (/0)
% ping_timeout (ms)
start_link(StartOpts) ->
    case ?BU:get_by_key(fun_nodes,StartOpts,undefined) of
        F when is_function(F,0) -> start_link_1(StartOpts);
        _ -> {error, {invalid_params, <<"Invalid 'fun_nodes'. Expected function of 0 args">>}}
    end.
start_link_1(StartOpts) ->
    case ?BU:get_by_key(fun_start,StartOpts,undefined) of
        F when is_function(F,0) -> start_link_2(StartOpts);
        F when is_function(F,1) -> start_link_2(StartOpts);
        _ -> {error, {invalid_params, <<"Invalid 'fun_start'. Expected function of 0/1 args">>}}
    end.
start_link_2(StartOpts) ->
    case ?BU:get_by_key(fun_stop,StartOpts,undefined) of
        F when is_function(F,0) -> start_link_3(StartOpts);
        _ -> {error, {invalid_params, <<"Invalid 'fun_stop'. Expected function of 0 args">>}}
    end.
start_link_3(StartOpts) ->
    RegName = ?BU:get_by_key(regname,StartOpts,?MODULE),
    Opts = case StartOpts of
               _ when is_map(StartOpts) -> maps:to_list(StartOpts);
               _ when is_list(StartOpts) -> StartOpts
           end,
    gen_server:start_link({local,RegName}, ?MODULE, Opts, []).

%% returns state of current sync FSM
%get_state(FromNode) -> get_state(?MODULE, FromNode).
get_state(RegName, FromNode) ->
    gen_server:call(RegName, {get_state,FromNode}).

% checks if node is ok, service is ok (process hangs)
check_node(RegName, _FromNode) ->
    case whereis(RegName) of
        undefined -> not_found;
        Pid -> {ok,Pid}
    end.

% stops node service to handle over other node
%takeover(FromNode) -> takeover(?MODULE, FromNode).
takeover(RegName, FromNode) ->
    gen_server:call(RegName, {takeover,FromNode}).

% active node send takeover event when heartbeat model is used and found more priority node
activate_takeover(RegName, FromNode) ->
    gen_server:cast(RegName, {activate_takeover,FromNode}).

% register heartbeat
heartbeat(RegName, FromNode) ->
    gen_server:call(RegName, {heartbeat,FromNode}).

%% Print state
print(RegName) ->
    gen_server:call(RegName, {print}).

%% returns last known active nodename after last ping
get_active_node(RegName) ->
    gen_server:call(RegName, {get_active_node}).

% @debug
debug_timeout(RegName,Timeout) ->
    gen_server:cast(RegName, {debug_timeout,Timeout}).

% @debug
debug_check_disconnect_to_all(RegName) ->
    gen_server:cast(RegName, {debug_check_disconnect_to_all}).

% @debug
debug_throw(RegName) ->
    gen_server:cast(RegName, {debug_throw}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    [FunNodes,FunStart,FunStop] = ?BU:extract_required_props([fun_nodes,fun_start,fun_stop], Opts),
    [RegName, PingTimeout, HeartbeatTimeout, AutoSort, WaitTimes, Monitor, DblRestartMaster, Heartbeat, Takeover, TOS, OUTF] =
        ?BU:extract_optional_default([{regname,?MODULE},
                                      {ping_timeout,?PING_TIMEOUT},
                                      {heartbeat_timeout,?HEARTBEAT_TIMEOUT},
                                      {autosort,true},
                                      {waittimes,0},
                                      {monitor,false},
                                      {dbl_restart_master,false},
                                      {heartbeat,false},
                                      {takeover,false},
                                      {fun_takeover_state,undefined},
                                      {fun_out,undefined}], Opts),
    Ref = make_ref(),
    State = #state{regname=RegName,
                   ets=ets:new(env, [set]),
                   state=started,
                   statestartts=?BU:timestamp(),
                   funs=#funs{fun_nodes=FunNodes,
                              fun_start=FunStart,
                              fun_stop=FunStop,
                              fun_takeover_state=TOS,
                              fun_out=OUTF},
                   opts=#opts{ping_timeout=PingTimeout,
                              heartbeat_timeout=HeartbeatTimeout,
                              use_monitor=Monitor,
                              use_takeover=Takeover,
                              use_heartbeat=Heartbeat,
                              use_waittimes=WaitTimes,
                              dbl_restart_master=DblRestartMaster,
                              autosort=AutoSort},
                   ping_ref=Ref,
                   timerref=erlang:send_after(0, self(), {?PING_MSG,Ref})},
    ?OUTF(State, "LEADER '~s'. inited (~p)", [RegName, self()]),
    {ok, State}.

%% ------------------------------
%% Call
%% ------------------------------

%%
handle_call({get_state,_FromNode}, _From, #state{state=S}=State) ->
    Reply = {ok,S},
    {reply, Reply, State};

% handle takeover command from most priority
handle_call({takeover,FromNode}, _From, #state{state=active}=State) ->
    ?OUTF(State, "LEADER '~s'. Stop on takeover.", [State#state.regname]),
    {ok,Res,State1} = stop_strategy_service(FromNode, State),
    {reply, Res, State1#state{takeover_ref=undefined}};
handle_call({takeover,_FromNode}, _From, #state{}=State) ->
    Reply = {invalid_state},
    {reply, Reply, State};

%% handle heartbeat from active
handle_call({heartbeat, Node}, _From, #state{state=started,timerref=undefined}=State) ->
    State1 = set_active_node(Node,State),
    Reply = {ok,started},
    {reply, Reply, State1};
handle_call({heartbeat, Node}, _From, #state{state=started,timerref=TimerRef}=State) ->
    erlang:cancel_timer(TimerRef),
    State1 = wait(set_active_node(Node,State)),
    Reply = {ok,started},
    {reply, Reply, State1};
handle_call({heartbeat, _Node}, _From, #state{state=S}=State) ->
    Reply = {ok,S},
    {reply, Reply, State};

%%
handle_call({print}, _From, #state{regname=RegName}=State) ->
    ?OUTF(State, "LEADER '~s'. State: ~120p", [RegName, State]),
    {reply, State, State};

handle_call({get_active_node}, _From, #state{active_node=ActiveNode, regname=_RegName}=State) ->
    %?OUTF(State, "LEADER '~s'. Active node: '~s'", [_RegName, ActiveNode]),
    {reply, ActiveNode, State};

%% other
handle_call(_Request, _From, State) ->
    %?OUTF(State, "LEADER '~s'. handle_call(~p)", [State#state.regname,_Request]),
    Reply = ok,
    {reply, Reply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% handle start takeover internal command
handle_cast({start_takeover, Node}, #state{state=takeover}=State) ->
    State1 = start_takeover(Node,State),
    {noreply, State1};

%% handle activate_takeover event from active node
handle_cast({activate_takeover, FromNode}, #state{state=started,active_node=FromNode,timerref=undefined}=State) ->
    ?OUTF(State, "LEADER '~s'. Got takeover event.", [State#state.regname]),
    {noreply, State};
handle_cast({activate_takeover, FromNode}, #state{state=started,active_node=FromNode,timerref=TimerRef}=State) ->
    ?OUTF(State, "LEADER '~s'. Got takeover event.", [State#state.regname]),
    erlang:cancel_timer(TimerRef),
    State1 = start_ping_group(State#state{timerref=undefined}),
    {noreply, State1};
% #193
handle_cast({activate_takeover,FromNode}, #state{state=active,statestartts=STS}=State) ->
    ?OUTF(State, "LEADER '~s'. Got takeover event from '~s' while active! Seems to be problem of global names (no conflict)", [State#state.regname,FromNode]),
    case ?BU:timestamp() of
        X when X > STS, X - STS > 5000 ->
            ?OUTF(State, "LEADER '~s'. Stop node", [State#state.regname]),
            init:stop();
        _ -> ok
    end,
    {noreply, State};
% #193
handle_cast({activate_takeover,FromNode}, #state{state=S}=State) ->
    ?OUTF(State, "LEADER '~s'. Got takeover event from '~s' while ~s!", [State#state.regname,FromNode,S]),
    {noreply, State};

%% @debug
handle_cast({debug_timeout,Timeout}, State) ->
    timer:sleep(Timeout),
    {noreply, State};

%% @debug
handle_cast({debug_check_disconnect_to_all}, State) ->
    check_disconnect_to_all(debug, State),
    {noreply, State};

%% @debug
handle_cast({debug_throw}, _State) ->
    throw(debug);

%% other
handle_cast(_Request, State) ->
    %?OUTF(State, "LEADER '~s'. handle_cast(~120p)", [State#state.regname,_Request]),
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

% ping timer
handle_info({?PING_MSG,Ref}, #state{state=started,inited=true,opts=#opts{use_heartbeat=true},ping_ref=Ref}=State) ->
    ?OUTF(State, "LEADER '~s'. Heartbeat timeout.", [State#state.regname]),
    State1 = start_ping_group(State),
    {noreply, State1};
handle_info({?PING_MSG,Ref}, #state{state=started,ping_ref=Ref}=State) ->
    State1 = start_ping_group(State),
    {noreply, State1};
handle_info({?PING_MSG,Ref}, #state{state=active,ping_ref=Ref}=State) ->
    State1 = start_ping_group(State),
    {noreply, State1};

% heartbeat timer
handle_info({?HEARTBEAT_MSG,Ref}, #state{state=active,heartbeat_ref=Ref}=State) ->
    State1 = start_heartbeat_group(State),
    {noreply, State1};

% ping reply event
handle_info({ping_reply,Ref,Res},#state{ping_ref=Ref,state=started,inited=false,timerref=undefined}=State) ->
    State1 = do_handle_result_started(Res,State#state{ping_ref=undefined}),
    {noreply, State1#state{inited=true}};
handle_info({ping_reply,Ref,Res},#state{ping_ref=Ref,state=started,timerref=undefined}=State) ->
    State1 = do_handle_result_started(Res,State#state{ping_ref=undefined}),
    {noreply, State1};
handle_info({ping_reply,Ref,Res},#state{ping_ref=Ref,state=active,timerref=undefined}=State) ->
    State1 = do_handle_result_active(Res,State#state{ping_ref=undefined}),
    {noreply, State1};

% takeover reply event
handle_info({takeover_reply,Ref,Res},#state{takeover_ref=Ref,state=takeover,timerref=undefined}=State) ->
    State1 = do_handle_result_takeover(Res,State#state{takeover_ref=undefined}),
    {noreply, State1};

% checknode reply event
handle_info({checknode_reply,Ref,Res},#state{checknode_ref=Ref,state=started,timerref=undefined}=State) ->
    State1 = do_handle_checknode_result_started(Res,State#state{checknode_ref=undefined}),
    {noreply, State1};

% heartbeat reply event
handle_info({heartbeat_reply,Ref,Res},#state{heartbeat_ref=Ref,state=active}=State) ->
    State1 = do_handle_heartbeat_result_active(Res,State), % ref not cleared, same on timer and async reply
    {noreply, State1};

% activate reply event
handle_info({activate_reply,Ref,Res},#state{activate_ref=Ref,state=activating}=State) ->
    State1 = do_handle_activate(Res,State#state{activate_ref=undefined}),
    {noreply, State1};

% down spawned event
handle_info({'DOWN',_,process,_,normal},#state{}=State) ->
    {noreply, State};
handle_info({'DOWN',_,process,_,_},#state{state=takeover}=State) ->
    State1 = do_handle_result_takeover(undefined,State),
    {noreply, State1#state{takeover_ref=undefined}};
handle_info({'DOWN',_,process,_,_},#state{state=S}=State) when S==active;S==started->
    State1 = start_ping_group(State),
    {noreply, State1#state{ping_ref=undefined}};
handle_info({'DOWN',_,process,_,_},#state{state=activating}=State) ->
    {ok,_,State1} = stop_strategy_service(undefined, State),
    {noreply, State1#state{activate_ref=undefined}};

% down of active_node
handle_info({nodedown,Node}, #state{state=active,monitors=M}=State) ->
    % erlang:monitor_node(Node,false), % if cookie differs, then looped connect/down
    State1 = State#state{monitors=lists:keydelete(Node, 1, M)},
    {noreply, State1};
handle_info({nodedown,Node}, #state{active_node=Node,state=takeover}=State) ->
    handle_info({nodedown,Node}, State#state{state=started});
handle_info({nodedown,Node}, #state{active_node=Node,monitors=M,checknode_ref=CNRef,timerref=TimerRef}=State) ->
    ?OUTF(State, "LEADER '~s'. Active node DOWN '~s'.", [State#state.regname,Node]),
    % erlang:monitor_node(Node,false), % locks for 5-6 sec timeout
    State1 = State#state{active_node=undefined,
                         monitors=lists:keydelete(Node, 1, M),
                         takeover_ref=undefined,
                         checknode_ref=undefined,
                         wait_times=0},
    case TimerRef of undefined -> ok; _ -> erlang:cancel_timer(TimerRef) end,
    case CNRef of undefined -> ok; _ -> check_disconnect_to_all(Node, State#state{checknode_ref=undefined}) end,
    State2 = start_ping_group(State1),
    {noreply, State2};

% other
handle_info(_Info, State) ->
    %?OUTF(State, "LEADER '~s'. handle_info(~120p)", [State#state.regname, _Info]),
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.


%% ====================================================================
%% Internal functions
%% ====================================================================

% @private
% returns nodes to synchronize unique strategy example started
get_synchronized_nodes(#state{funs=#funs{fun_nodes=FNodes}}=_State)
    when is_function(FNodes,0) ->
    case catch FNodes() of
        {'EXIT',_} -> [];
        T -> T
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initiates ping of synchronized nodes
start_ping_group(State) ->
    % @todo from config or local nodes
    {Self,Ref,Node} = {self(),make_ref(),node()},
    Ns = get_synchronized_nodes(State),
    State1 = State#state{last_nodes=Ns},
    case sort_nodes(Ns, State1) of
        [] -> wait(State1);
        [Node] ->
            Self ! {ping_reply,Ref,[]},
            State1#state{ping_ref=Ref,timerref=undefined};
        AllNodes ->
            Nodes = lists:delete(Node, AllNodes),
            #state{regname=RegName}=State1,
            spawn_monitor(fun() -> Self ! {ping_reply,Ref,get_states_from_nodes(Nodes,RegName,?QUERY_TIMEOUT)} end),
            State1#state{ping_ref=Ref,timerref=undefined}
    end.

% --------------------------------------------------
% async ping result
%    state = started
do_handle_result_started(Res, #state{opts=#opts{use_takeover=true}}=State) ->
    case lists:keysearch(takeover, 2, Res) of
        false -> do_handle_result_started_1(Res,State);
        {value, {TNode,_}} ->
            outstart({takeover,TNode},State),
            ?OUTF(State, "LEADER '~s'. Found takeover node '~s'.", [State#state.regname,TNode]),
            case lists:keysearch(active, 2, Res) of
                false -> wait(set_active_node(TNode,State));
                {value, {ANode1, active}} -> wait(set_active_node(ANode1,State))
            end end;
do_handle_result_started(Res, #state{opts=#opts{use_takeover=false}}=State) ->
    do_handle_result_started_1(Res,State).
%
do_handle_result_started_1(Res, State) ->
    case lists:keysearch(activating, 2, Res) of
        false -> do_handle_result_started_2(Res,State);
        {value, {ANode,_}} ->
            outstart({activating,ANode},State),
            ?OUTF(State, "LEADER '~s'. Found activating node '~s'.", [State#state.regname,ANode]),
            case lists:keysearch(active, 2, Res) of
                false -> wait(set_active_node(ANode,State));
                {value, {ANode1, active}} -> wait(set_active_node(ANode1,State))
            end end.
%
do_handle_result_started_2(Res, #state{opts=#opts{use_takeover=Takeover}}=State) ->
    MyNode = node(),
    % if any node found active - ok or takeover
    % several started nodes uses common algorythm to choose master
    case lists:keysearch(active, 2, Res) of
        false ->
            StartedNodes = lists:filtermap(fun({Node,started}) -> {true,Node}; (_) -> false end, Res),
            case sort_nodes([MyNode|StartedNodes], State) of
                [MyNode|_] ->
                    outstart(master,State),
                    checknode_start(State); % start_strategy_service(undefined,State);
                [Node|_] ->
                    outstart({wait_master,Node},State),
                    wait(State)
            end;
        {value, {ANode,active}} when ANode/=MyNode, Takeover ->
            StartedNodes = lists:filtermap(fun({Node,started}) -> {true,Node}; (_) -> false end, Res),
            case sort_nodes([ANode,MyNode|StartedNodes], State) of
                [MyNode|_] ->
                    outstart({slave,ANode},State),
                    gen_server:cast(self(), {start_takeover,ANode}),
                    State1 = set_active_node(ANode,State),
                    State1#state{state=takeover,
                                 statestartts=?BU:timestamp(),
                                 timerref=undefined};
                _ ->
                    outstart({active,ANode},State),
                    wait(set_active_node(ANode,State))
            end;
        {value, {ANode,active}} ->
            outstart({active,ANode},State),
            wait(set_active_node(ANode,State))
    end.

% --------
% async ping result
%    state = active
do_handle_result_active(Res, #state{regname=RegName,opts=#opts{dbl_restart_master=RestartMaster}}=State) ->
    MyNode = node(),
    ActiveNodes = lists:filtermap(fun({Node,active}) -> {true,Node}; (_) -> false end, Res),
    case sort_nodes([MyNode|ActiveNodes], State) of
        [MyNode] -> wait_active(State);
        [MyNode,NextNode|_] when RestartMaster ->
            ?OUTF(State, "LEADER '~s'. Auto DOWN (double active, master).", [RegName]),
            activate_takeover(RegName, NextNode),
            {ok,_,State2} = stop_strategy_service(NextNode,State),
            State2;
        [MyNode|_] -> wait_active(State);
        _ -> % only if several active and mynode is not most priority
            ?OUTF(State, "LEADER '~s'. Auto DOWN (double active, slave).", [RegName]),
            {ok,_,State1} = stop_strategy_service(undefined,State),
            State1
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% initiates heartbeat of synchronized nodes
start_heartbeat_group(#state{state=active,heartbeat_ref=HBRef}=State) ->
    {Self,Node} = {self(),node()},
    Ref = case HBRef of undefined -> make_ref(); _ -> HBRef end,
    Ns = get_synchronized_nodes(State),
    State1 = State#state{last_nodes=Ns},
    case sort_nodes(Ns, State1) of
        [] -> wait_active(State1);
        [Node] -> wait_active(State1);
        AllNodes ->
            Nodes = lists:delete(Node, AllNodes),
            #state{regname=RegName}=State1,
            spawn_monitor(fun() -> Self ! {heartbeat_reply,Ref,send_heartbeat(Nodes,RegName,?QUERY_TIMEOUT)} end),
            wait_active(State1#state{heartbeat_ref=Ref})
    end.

% --------------------------------------------------
% async heartbeat result
do_handle_heartbeat_result_active(Res,#state{state=active,regname=RegName}=State) ->
    case lists:keysearch(takeover,2,Res) of
        {_,_} -> ok;
        false ->
            MyNode = node(),
            AliveNodes = lists:filtermap(fun({Node,started}) -> {true,Node};
                                            ({Node,active}) -> {true,Node};
                                            ({Node,activating}) -> {true,Node};
                                            (_) -> false end, Res),
            case sort_nodes([MyNode|AliveNodes], State) of
                [MyNode|_] -> ok;
                [Node|_] ->
                    ?OUTF(State, "LEADER '~s'. Send takeover event to '~s'.", [State#state.regname,Node]),
                    spawn(fun() -> send_takeover_event(Node,RegName,1000) end)
            end end,
    State.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% @private
% start takeover operation
start_takeover(Node, #state{regname=RegName}=State) ->
    ?OUTF(State, "LEADER '~s'. Start takeover from '~s'.", [State#state.regname,Node]),
    {Self,Ref} = {self(),make_ref()},
    spawn_monitor(fun() -> Self ! {takeover_reply,Ref,call_takeover(Node,RegName,?TAKEOVER_TIMEOUT)} end),
    State#state{state=takeover,
                statestartts=?BU:timestamp(),
                takeover_ref=Ref,
                timerref=undefined}.

% --------------------------------------------------
% after got takeover result
% if state = takeover
do_handle_result_takeover(undefined, #state{state=takeover}=State) ->
    wait(State#state{state=started,
                     statestartts=?BU:timestamp()});
do_handle_result_takeover({invalid_state}, #state{state=takeover}=State) ->
    wait(State#state{state=started,
                     statestartts=?BU:timestamp()});
do_handle_result_takeover({ok,RemoteState}, #state{state=takeover}=State) ->
    ?OUTF(State, "LEADER '~s'. Apply takeover.", [State#state.regname]),
    start_strategy_service(RemoteState,State).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% try to call active_node if no active found on ping
checknode_start(#state{state=started,active_node=undefined}=State) ->
    ?OUTF(State, "LEADER '~s'. Start as master.", [State#state.regname]),
    start_strategy_service(undefined,State#state{wait_times=0});
checknode_start(#state{regname=RegName,state=started,active_node=ANode}=State) ->
    ?OUTF(State, "LEADER '~s'. Check node '~s'.", [State#state.regname,ANode]),
    {Self,Ref} = {self(),make_ref()},
    spawn_monitor(fun() -> Self ! {checknode_reply,Ref,call_check_node(ANode,RegName,?QUERY_TIMEOUT)} end),
    State#state{checknode_ref=Ref,timerref=undefined}.

% --------------------------------------------------
% async checknode result (when no active found, but active_node is setup)
do_handle_checknode_result_started({ok,_}=Res,#state{state=started,wait_times=WS,opts=#opts{use_waittimes=MWS}}=State) when WS<MWS ->
    ?OUTF(State, "LEADER '~s'. Check node ~120p times remain (result: ~120p).", [State#state.regname,MWS-WS,Res]),
    wait(State#state{wait_times=WS+1});
do_handle_checknode_result_started(_,#state{state=started,active_node=ANode}=State) ->
    ?OUTF(State, "LEADER '~s'. Failover.", [State#state.regname]),
    catch check_disconnect_to_all(ANode,State),
    start_strategy_service(undefined,State#state{wait_times=0}).

% @private
check_disconnect_to_all(undefined,_) -> ok;
check_disconnect_to_all(_ANode,State) ->
    case ?BU:is_open_node() of
        false -> ok;
        true ->
            MyNode = node(),
            RegName = State#state.regname,
            Nodes = nodes() ++ [node()],
            ?OUTF(State, "LEADER '~s'. FULL-MESH check nodes.", [RegName]),
            Opts = #{disconnect => true,
                     set_group_leader => true,
                     out => ?BU:str("PING GLOBAL GROUP by '~s' from '~s'",[RegName, MyNode]),
                     timeout => 800},
            {R,E} = ?BLmulticall:map_result_rpc_multicall(?BLmulticall:call_direct(Nodes, {?BLping, ping_open, [{500,100}, Opts]}, 1000)),
            ?OUTF(State, "LEADER '~s'. FULL-MESH check nodes result: ~n\tok: ~120p~n\terr: ~120p", [RegName, lists:zip(Nodes--E,R), E]),
            ok
    end.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% wait for next ping timer event
wait(#state{timerref=TimerRef}=State) ->
    ?BU:cancel_timer(TimerRef),
    Ref = make_ref(),
    State#state{ping_ref=Ref,
                timerref=erlang:send_after((State#state.opts)#opts.ping_timeout, self(), {?PING_MSG,Ref})}.

% wait for next heartbeat timer event
wait_active(#state{opts=#opts{use_heartbeat=false}}=State) -> wait(State);
wait_active(#state{heartbeat_ref=undefined}=State) -> wait_active(State#state{heartbeat_ref=make_ref()});
wait_active(#state{heartbeat_ref=HBRef}=State) ->
    State#state{timerref=erlang:send_after((State#state.opts)#opts.heartbeat_timeout, self(), {?HEARTBEAT_MSG,HBRef})}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% start strategy
start_strategy_service(RemoteState, #state{funs=#funs{fun_start=FStart}}=State)
    when is_function(FStart) ->
    {Self,Ref} = {self(),make_ref()},
    spawn_monitor(fun() -> Self ! {activate_reply,Ref,activate(RemoteState,FStart)} end),
    ?OUTF(State, "LEADER '~s'. Activating.", [State#state.regname]),
    State#state{state=activating,
                statestartts=?BU:timestamp(),
                activate_ref=Ref,
                timerref=undefined}.

% @private
activate(_,FStart) when is_function(FStart,0) -> catch FStart();
activate(RemoteState,FStart) when is_function(FStart,1) -> catch FStart(RemoteState).

% ----------------
do_handle_activate({error,_}=Res,State) -> do_handle_activate_err(Res,State);
do_handle_activate({'EXIT',_}=Res,State) -> do_handle_activate_err(Res,State);
do_handle_activate(Res,State) -> do_handle_activate_ok(Res,State).

do_handle_activate_err(Res,#state{funs=#funs{fun_stop=FStop}}=State) ->
    ?OUTF(State, "LEADER '~s'. Start error: ~120p", [State#state.regname, Res]),
    catch FStop(),
    State1 = State#state{state=started,
                         statestartts=?BU:timestamp(),
                         timerref=undefined},
    start_ping_group(State1).

do_handle_activate_ok(_Res,#state{regname=RegName,funs=#funs{fun_stop=FStop},last_nodes=Nodes}=State) ->
    MyNode = node(),
    State1 = State#state{state=active,
                         statestartts=?BU:timestamp(),
                         active_node=MyNode,
                         timerref=undefined},
    case FStop of
        _ when is_function(FStop,0) -> ?BLmonitor:append_fun(self(),RegName,fun() -> catch FStop() end)
    end,
    spawn_monitor(fun() -> send_heartbeat(Nodes--[MyNode],RegName,?QUERY_TIMEOUT) end),
    wait_active(State1).

%%%%%%%%%%%%%%%%%%%%%%%%%%%

% stop strategy
stop_strategy_service(FromNode,#state{regname=RegName,funs=#funs{fun_stop=FStop,fun_takeover_state=FTOS},timerref=TimerRef}=State)
    when is_function(FStop,0) ->
    Res = case is_function(FTOS,1) of
              true -> {ok, case catch FTOS(FromNode) of {'EXIT',_} -> undefined; T -> T end};
              _ -> {ok,undefined}
          end,
    catch FStop(),
    ?BLmonitor:drop_fun(self(),RegName),
    case TimerRef of undefined -> ok; _ -> erlang:cancel_timer(TimerRef) end,
    State1 = set_active_node(FromNode,State),
    State2 = wait(State1#state{state=started,
                               statestartts=?BU:timestamp(),
                               ping_ref=undefined,
                               heartbeat_ref=undefined,
                               takeover_ref=undefined}),
    {ok,Res,State2};
stop_strategy_service(_,State) ->
    {ok,{error,<<"Stop function undefined">>},State}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

set_active_node(Node, #state{active_node=Node}=State) -> State#state{wait_times=0};
set_active_node(undefined, #state{}=State) -> State#state{active_node=undefined,wait_times=0};
set_active_node(Node, #state{opts=#opts{use_monitor=UseMonitor},monitors=M}=State) ->
    State1 = State#state{active_node=Node,wait_times=0},
    case lists:keyfind(Node,1,M) of
        {_,_} -> State1;
        false when UseMonitor ->
            MRef = erlang:monitor_node(Node, true),
            State1#state{monitors=lists:keystore(Node,1,M,{Node,MRef})};
        false -> State1
    end.

% -----
% @private
sort_nodes(Nodes,#state{opts=#opts{autosort=false},last_nodes=LNodes}) ->
    LNodes--LNodes--lists:usort(Nodes);
sort_nodes(Nodes,_State) ->
    lists:usort(Nodes).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% @private
get_states_from_nodes(Nodes,RegName,Timeout) ->
    SelfNode = node(),
    Res = ?BLmulticall:call_direct(Nodes, {?MODULE, get_state, [RegName,SelfNode]}, Timeout),
    lists:map(fun({Node,{ok,S}}) -> {Node,S}; (R) -> R end, Res).

% @private
send_heartbeat(Nodes,RegName,Timeout) ->
    SelfNode = node(),
    Res = ?BLmulticall:call_direct(Nodes, {?MODULE, heartbeat, [RegName,SelfNode]}, Timeout),
    lists:map(fun({Node,{ok,S}}) -> {Node,S}; (R) -> R end, Res).

% @private
call_takeover(Node,RegName,Timeout) ->
    SelfNode = node(),
    rpc_call(Node, ?MODULE, takeover, [RegName,SelfNode], Timeout).

% @private
call_check_node(Node,RegName,Timeout) ->
    SelfNode = node(),
    rpc_call(Node, ?MODULE, check_node, [RegName,SelfNode], Timeout).

% @private
send_takeover_event(Node,RegName,Timeout) ->
    SelfNode = node(),
    rpc_call(Node, ?MODULE, activate_takeover, [RegName,SelfNode], Timeout).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% @private
rpc_call(Node,Module,Fun,Args,Timeout) ->
    case catch ?BLrpc:call(Node, Module, Fun, Args, Timeout) of
        {'EXIT',_} -> undefined;
        {badrpc,_} -> undefined;
        {error,_} -> undefined;
        T -> T
    end.

% @private
out(#state{funs=#funs{fun_out=undefined}},Fmt,Args) -> ?LOG('$info',Fmt,Args);
out(#state{funs=#funs{fun_out=OutF}},Fmt,Args) -> OutF(Fmt,Args).

% @private
% log starting state after first ping
outstart(_,#state{inited=true}) -> ok;
outstart({takeover,Node},State) ->
    ?OUTF(State, "LEADER '~s'. START. Found takeover node '~s'.", [State#state.regname,Node]);
outstart({activating,Node},State) ->
    ?OUTF(State, "LEADER '~s'. START. Found activating node '~s'.", [State#state.regname,Node]);
outstart(master,State) ->
    ?OUTF(State, "LEADER '~s'. START. Empty, as master", [State#state.regname]);
outstart({wait_master,Node},State) ->
    ?OUTF(State, "LEADER '~s'. START. Empty, as slave, master node '~s'.", [State#state.regname,Node]);
outstart({slave,Node},State) ->
    ?OUTF(State, "LEADER '~s'. START. Found slave node '~s'.", [State#state.regname,Node]);
outstart({active,Node},State) ->
    ?OUTF(State, "LEADER '~s'. START. Found master node '~s'.", [State#state.regname,Node]).
