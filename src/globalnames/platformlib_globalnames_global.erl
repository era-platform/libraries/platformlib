%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2021
%%% @doc Global naming over gnr (instead of erlang's global).
%%%      Using gnr server + local caching instead of storing everywhere and opened nodes

-module(platformlib_globalnames_global).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([global_participant/0,
         registered_names_local/0]).

-export([registered_names/0,
         sys_get_state/1]).

-export([whereis_name/1,
         register_name/2,
         unregister_name/1,
         send/2]).

-export([gen_server_start_link/4]).

-export([gen_server_call/2,
         gen_server_call_nocache/2,
         gen_server_call/3,
         gen_server_call_nocache/3,
         gen_server_cast/2,
         gen_server_cast_nocache/2,
         gen_server_stop/1,
         gen_server_stop_nocache/1]).

-export([call/2,
         call/3,
         cast/2,
         start_link/4]).

-export([registered_names_hash/0]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -------------------------------------
%% returns true if this node participate in global naming
%% -------------------------------------
global_participant() ->
    case ?GN_REG:registered_names_local_size() of
        N when N > 0 -> true;
        _ -> false
    end.

%% -------------------------------------
%% return all registered names stored from/on current node
%% -------------------------------------
registered_names_local() ->
    ?GN_REG:registered_names_local().

%% -------------------------------------
%% return all registered names (from local and gnr server)
%% -------------------------------------
registered_names() ->
    ?GN_REG:registered_names().

%% -------------------------------------
%% sys:get_state implementation
%% -------------------------------------
sys_get_state({global,Name}) -> sys_get_state(Name);
sys_get_state(Name) ->
    case whereis_name(Name) of
        undefined -> exit({noproc,{sys,get_state,[Name]}});
        Pid -> sys:get_state(Pid)
    end.

%% ====================================================================
%% Callback functions
%% ====================================================================

%% -------------------------------------
%% Registers globally named OTP process
%% -------------------------------------
register_name(Name,Pid) ->
    ?GN_REG:register_name(Name,Pid).

%% -------------------------------------
%% Unregisters processes from name. Using filter by current node
%% -------------------------------------
unregister_name(Name) ->
    ?GN_REG:unregister_name(Name).

%% -------------------------------------
%% Returns any registered process
%% -------------------------------------
whereis_name(Name) ->
    ?GN_REG:whereis_name_nocache(Name).

%% -------------------------------------
%% Send message to any globally named process
%% -------------------------------------
send(Name,Msg) ->
    ?GN_REG:send(Name,Msg).

%% ====================================================================
%% OTP process functions
%% ====================================================================

%% -------------------------------------
%% start_link
%% -------------------------------------
gen_server_start_link({global,Name}, Module, Arg, Opts) -> gen_server_start_link(Name, Module, Arg, Opts);
gen_server_start_link(Name, Module, Arg, Opts) when not is_tuple(Name) ->
    gen_server_start_link({gnr,single,Name}, Module, Arg, Opts);
gen_server_start_link({gnr,multi,Name}, Module, Arg, Opts) ->
    case gen_server:start_link(Module,Arg,Opts) of
        {error,_}=Err -> Err;
        ignore -> ignore;
        {ok,Pid} ->
            ?GN_REG:register_name(Name,Pid),
            {ok,Pid}
    end;
gen_server_start_link({gnr,single,Name}, Module, Arg, Opts) ->
    StartLinkArgs = [{via,?MODULE,Name}, Module, Arg, Opts],
    gen_server_start_link_1(StartLinkArgs, 0).

%% @private
%% retry count, not unlimited
gen_server_start_link_1(StartLinkArgs, Retry) ->
    [GnParams,Module,Arg,Opts] = StartLinkArgs,
    case gen_server:start_link(GnParams,Module,Arg,Opts) of
        {error, {already_started, _Pid}}=Err when Retry>=13 -> Err;
        {error, {already_started, _Pid}} ->
            % when restart srv gen_server checks if name-pid exists in gnr,
            %      async reglib name server returns previous value from ets while 'DOWN' is being handled
            timer:sleep(?BU:growing_pause(Retry,4,50)), % summary 8 sec
            gen_server_start_link_1(StartLinkArgs, Retry+1);
        R -> R
    end.

%% -------------------------------------
%% call with default timeout
%% -------------------------------------
gen_server_call({global,Name},Msg) -> gen_server_call(Name,Msg);
gen_server_call(Name,Msg) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name(Name) of
        undefined -> exit({noproc,{gen_server,call,[Name,Msg]}});
        Pid -> gen_server:call(Pid,Msg)
    end.

%%
gen_server_call_nocache({global,Name},Msg) -> gen_server_call_nocache(Name,Msg);
gen_server_call_nocache(Name,Msg) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name_nocache(Name) of
        undefined -> exit({noproc,{gen_server,call,[Name,Msg]}});
        Pid -> gen_server:call(Pid,Msg)
    end.

%% -------------------------------------
%% call with custom timeout
%% -------------------------------------
gen_server_call({global,Name},Msg,Timeout) -> gen_server_call(Name,Msg,Timeout);
gen_server_call(Name,Msg,Timeout) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name(Name) of
        undefined -> exit({noproc,{gen_server,call,[Name,Msg]}});
        Pid -> gen_server:call(Pid,Msg,Timeout)
    end.

%%
gen_server_call_nocache({global,Name},Msg,Timeout) -> gen_server_call_nocache(Name,Msg,Timeout);
gen_server_call_nocache(Name,Msg,Timeout) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name_nocache(Name) of
        undefined -> exit({noproc,{gen_server,call,[Name,Msg]}});
        Pid -> gen_server:call(Pid,Msg,Timeout)
    end.

%% -------------------------------------
%% cast
%% -------------------------------------
gen_server_cast({global,Name},Msg) -> gen_server_cast(Name,Msg);
gen_server_cast(Name,Msg) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name(Name) of
        undefined -> ok;
        Pid -> gen_server:cast(Pid,Msg)
    end.

%%
gen_server_cast_nocache({global,Name},Msg) -> gen_server_cast_nocache(Name,Msg);
gen_server_cast_nocache(Name,Msg) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name_nocache(Name) of
        undefined -> ok;
        Pid -> gen_server:cast(Pid,Msg)
    end.

%% -------------------------------------
%% stop
%% -------------------------------------
gen_server_stop({global,Name}) -> gen_server_stop(Name);
gen_server_stop(Name) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name(Name) of
        undefined -> exit({noproc,{gen_server,stop,[Name]}});
        Pid -> gen_server:stop(Pid)
    end.

%%
gen_server_stop_nocache({global,Name}) -> gen_server_stop_nocache(Name);
gen_server_stop_nocache(Name) ->
    % use local cache (monitored)
    case ?GN_REG:whereis_name_nocache(Name) of
        undefined -> exit({noproc,{gen_server,stop,[Name]}});
        Pid -> gen_server:stop(Pid)
    end.

%% ====================================================================
%% API functions (common)
%% ====================================================================

%% -------------------------------------
%% mappings
%% -------------------------------------

start_link(Name, Module, Arg, Opts) -> gen_server_start_link(Name, Module, Arg, Opts).

call(Name, Msg) -> gen_server_call(Name, Msg).
call(Name, Msg, Timeout) -> gen_server_call(Name, Msg, Timeout).
cast(Name, Msg) -> gen_server_cast(Name, Msg).

%% -------------------------------------
%% common
%% -------------------------------------

%% returns hash of registered names data (union from local and server)
registered_names_hash() ->
    case global_participant() of
        false -> false;
        true ->
            L = lists:usort(registered_names()),
            {length(L), erlang:phash2(L)}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

