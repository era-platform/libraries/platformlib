%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 22.05.2021
%%% @doc Facade to global name registrar. Implements caching.

-module(platformlib_globalnames_registrar).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([registered_pids/0,
         registered_names_local_size/0,
         registered_names_local/0,
         registered_names/0,
         register_name/2,
         unregister_name/1,
         whereis_name/1, whereis_name_nocache/1,
         whereis_name_all/1, whereis_name_all_nocache/1,
         send/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% Return list of all registered pids in local reglib's name server.
%%     If this is gnr node, then it would return count of all registered names from current cluster
%% -------------------------------------
registered_pids() ->
    ?REGLIB:registered_pids().

%% -------------------------------------
%% Return record count in local reglib's name server.
%%     If this is gnr node, then it would return count of all registered names from current cluster
%% -------------------------------------
registered_names_local_size() ->
    ?REGLIB:registered_names_local_size().

%% -------------------------------------
%% Return all registered names from local server.
%%     If this is gnr node, then it would return all registered names from current cluster
%% -------------------------------------
registered_names_local() ->
    ?REGLIB:registered_names_local().

%% -------------------------------------
%% Return all registered names directly from gnr server
%% -------------------------------------
registered_names() ->
    ?REGLIB:registered_names().

%% -------------------------------------
%% register name to pid
%%      Append pid to name in reglib's server
%% -------------------------------------
register_name(Name,Pid) ->
    ?REGLIB:register_name(Name,Pid).

%% -------------------------------------
%% unregister name
%%     Only pids from current node.
%%     Local reglib's name server would filter processes by current node.
%%     So gnr server would keep processes from other nodes.
%% -------------------------------------
unregister_name(Name) ->
    ?REGLIB:unregister_name(Name).

%% -------------------------------------
%% return registered process by name. The only or the first
%%   Use  local caching
%% -------------------------------------
whereis_name(Name) ->
    HC = gn_whereis,
    case ?BLstore:find_t({HC,Name}) of
        {_,Pid} when is_pid(Pid) -> Pid;
        {_,undefined} -> undefined;
        false ->
            Key = {HC,Name},
            case ?REGLIB:whereis_name(Name) of
                Pid when is_pid(Pid) ->
                    ?BLstore:store_t(Key,Pid,60000),
                    FunClear = fun() -> ?BLstore:delete_t(Key) end,
                    ?BLmonitor:append_fun(Pid, HC, FunClear),
                    Pid;
                undefined ->
                    ?BLstore:store_t(Key,undefined,1000),
                    undefined
            end end.

%% -------------------------------------
%% return registered process by name. The only or the first
%%   Without local caching
%% -------------------------------------
whereis_name_nocache(Name) ->
    ?REGLIB:whereis_name(Name).

%% -------------------------------------
%% return list of registered processes by name
%%   Use local caching
%% -------------------------------------
whereis_name_all(Name) ->
    HC = gn_whereis_all,
    case ?BLstore:find_t({HC,Name}) of
        {_,[]} -> [];
        {_,[_|_]=Pids} -> Pids;
        false ->
            Key = {HC,Name},
            case ?REGLIB:whereis_name_all(Name) of
                [] ->
                    ?BLstore:store_t(Key,[],1000),
                    [];
                [_|_]=Pids ->
                    ?BLstore:store_t(Key,Pids,60000),
                    FunClear = fun() -> ?BLstore:delete_t(Key) end,
                    lists:foreach(fun(Pid) when is_pid(Pid) -> ?BLmonitor:append_fun(Pid,HC,FunClear);
                                     (_) -> ok
                                  end, Pids),
                    Pids
            end end.

%% -------------------------------------
%% return list of registered processes by name
%%   Use local caching
%% -------------------------------------
whereis_name_all_nocache(Name) ->
    ?REGLIB:whereis_name_all(Name).

%% -------------------------------------
%% send to named process (by cache)
%% -------------------------------------
send(Name,Msg) ->
    F = fun(Pid) ->
            Pid ! Msg,
            Pid
        end,
    case whereis_name(Name) of
        undefined -> {badarg, {Name, Msg}};
        Pid when is_pid(Pid) -> F(Pid);
        [Pid] when is_pid(Pid) -> F(Pid);
        [_Pid|_]=Pids when is_pid(_Pid) ->
            F(lists:nth(?BU:random(length(Pids)) + 1, Pids));
        _ -> {badarg, {Name, Msg}}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================