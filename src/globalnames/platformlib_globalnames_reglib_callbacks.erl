%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 19.05.2021
%%% @doc Callback module for regilib

-module(platformlib_globalnames_reglib_callbacks).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_server_nodes_function/0,
         get_sync_nodes_function/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------
%% Return node names list where gnr server is setup by configuration
%% -----------------------
get_server_nodes_function() ->
    {ok,?PCFG:get_nodes_by_msvc(<<"gnr">>)}.

%% -----------------------
%% Return node names list where globally named processes could be active (to sync)
%% -----------------------
get_sync_nodes_function() ->
    {ok,?PCFG:get_nodes()}.

%% ====================================================================
%% Internal functions
%% ====================================================================