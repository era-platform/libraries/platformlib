%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.06.2021
%%% @doc Routines to copy file from another site

-module(platformlib_filestorage_copier_cross).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([copy_file_from_remote_site/4]).

-compile([{no_auto_import,[ref_to_list/1]}]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(COPYU, platformlib_filestorage_copier_utils).

-define(COPYCROSSSRV, r_env_copier_cross_srv).
-define(COPYCROSSSIZE, 65536).


%% ====================================================================
%% Public functions
%% ====================================================================

copy_file_from_remote_site({_FromSite,_FromNode}=_FromDest, _FromPath, _ToPath, _FLOG) ->
    {error,not_implemented}.

%%copy_file_from_remote_site({FromSite,FromNode}=FromDest, FromPath, ToPath, FLOG) ->
%%    FLOG('$trace', "Copy. Start copy file. ~ts/~ts/~ts",[FromSite,FromNode,FromPath]),
%%    case open_file_on_remote_site(FromDest,FromPath) of
%%        {ok,Pid,Ref} ->
%%            FLOG('$trace', "Copy. Open remote file ok.(~120p)",[FromPath]),
%%            RefL = ref_to_list(Ref),
%%            ?BLfilelib:ensure_dir(ToPath),
%%            Temp = lists:append([ToPath,["."],RefL]),
%%            case ?BLfile:open(Temp, [raw, write, {delayed_write,16384,100}]) of
%%                {ok, WF} ->
%%                    FLOG('$trace', "Copy. Open local file ok. (~120tp)", [Temp]),
%%                    Result1 = try copy_next_part(FromDest,Pid,Ref,WF,FromPath)
%%                              catch ErrorType:ErrorCatch -> {error,{ErrorType,ErrorCatch}}
%%                              end,
%%                    case close_file_on_remote_site(FromDest,Pid,Ref) of
%%                        {error,Reason1} -> FLOG('$error', "Copy. Close file on remote site error: ~120tp", [Reason1]);
%%                        _ -> ok
%%                    end,
%%                    ?BLfile:close(WF),
%%                    Result2 = case Result1 of
%%                                  {ok, eof} -> check_file(FromDest,FromPath,Temp);
%%                                  Error1 -> Error1
%%                              end,
%%                    FLOG('$trace', "Copy. Result: ~120tp", [Result2]),
%%                    Result3 = case Result2 of
%%                                  ok -> case ?BU:file_copy(Temp,ToPath) of {ok,_} -> ok; Error2 -> Error2 end;
%%                                  _ -> Result2
%%                              end,
%%                    FLOG('$trace', "Copy. Result: ~120tp", [Result3]),
%%                    ?BLfile:delete(Temp),
%%                    Result3;
%%                {error, Reason3}=Error3 ->
%%                    FLOG('$error', "Copy. Local file open error: '~ts', ~120tp",[ToPath, Reason3]),
%%                    Error3
%%            end;
%%        {error, Reason4}=Error4 ->
%%            FLOG('$error', "Copy. Remote file open error: '~ts', ~120tp",[FromPath, Reason4]),
%%            Error4
%%    end.
%%
%%%% ====================================================================
%%%% Internal functions
%%%% ====================================================================
%%
%%%% @private @cross
%%open_file_on_remote_site({FromSite,FromNode}=FromDest,FromPath) ->
%%    MFA = {?COPYCROSSSRV, open_file, [FromPath]},
%%    case ?ENVCROSS:call_node(FromDest, MFA, undefined, 5000) of
%%        {ok, Pid, _Ref}=Res when is_pid(Pid) -> Res;
%%        {error,_}=Error -> Error;
%%        undefined -> {error, badrpc}
%%    end.
%%
%%%% @private @cross
%%copy_next_part(Dest,Pid,Ref,WF,SrcPath) ->
%%    do_copy_next_part(Dest,Pid,Ref,WF,SrcPath,0,1,ok,1).
%%%% @private @cross
%%do_copy_next_part(_,_,_,_,_,_,4,LastError,_) ->LastError;
%%do_copy_next_part(Dest,Pid,Ref,WF,SrcPath,Pos,ErrorCount,LE,Count) ->
%%    MFA = {?COPYCROSSSRV, read_file, [Pid,Ref,Pos,?COPYCROSSSIZE]},
%%    Res = ?ENVCROSS:call_node(Dest, MFA, undefined, 5000),
%%    case Res of
%%        {ok, Ref, eof} -> {ok, eof};
%%        {ok, Ref, NewPos, Data} ->
%%            case NewPos of
%%                Pos -> ok;
%%                _ -> ?BLfile:write(WF, Data)
%%            end,
%%            ?COPYU:pause_traffic(Count), % limit speed/net traffic load
%%            do_copy_next_part(Dest,Pid,Ref,WF,SrcPath,NewPos,1,LE,Count+1);
%%        {error,_}=Error ->
%%            timer:sleep(1000),
%%            do_copy_next_part(Dest,Pid,Ref,WF,SrcPath,Pos,ErrorCount+1,Error,Count);
%%        undefined ->
%%            timer:sleep(1000),
%%            do_copy_next_part(Dest,Pid,Ref,WF,SrcPath,Pos,ErrorCount+1,{error, badrpc},Count)
%%    end.
%%
%%%% @private @cross
%%check_file(Dest,SrcPath,DestPath) ->
%%    MFA = {?COPYCROSSSRV, file_info, [SrcPath]},
%%    case ?ENVCROSS:call_node(Dest, MFA, undefined, 5000) of
%%        {ok, FI, MD5} ->
%%            MD5L = ?BU:file_md5(DestPath),
%%            case MD5L == MD5 of
%%                true ->
%%                    ?BLfile:write_file_info(DestPath, FI),
%%                    ok;
%%                false ->
%%                    {error, <<"Wrong MD5 hash">>}
%%            end;
%%        {error,_}=Error -> Error;
%%        undefined -> {error, badrpc}
%%    end.
%%
%%%% @private @cross
%%close_file_on_remote_site({_Site,_Node}=Dest,Pid,Ref) ->
%%    MFA = {?COPYCROSSSRV, close_file, [Pid,Ref]},
%%    case ?ENVCROSS:call_node(Dest, MFA, undefined, 5000) of
%%        ok -> ok;
%%        {error,_}=Error -> Error;
%%        undefined -> {error, badrpc}
%%    end.
%%
%%%% @private @cross
%%ref_to_list(Ref) ->
%%    Rl = erlang:ref_to_list(Ref),
%%    [R1,R2,R3,R4] = string:tokens(Rl,"."),
%%    R11 = lists:nthtail(5, R1),
%%    R44 = lists:reverse(lists:nthtail(1, lists:reverse(R4))),
%%    string:join(["Ref", R11, R2, R3, R44], "_").
