%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.06.2021
%%% @doc Client facade to FS msvc.
%%%    Now it is also a server side module.
%%%    * download from fs nodes sequentally until found the same hasha.
%%%    * upload to all fs nodes sequentally.
%%%    * delete from all fs nodes in parallel.

-module(platformlib_filestorage_drivers_fs_client).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    download/4, download/3,
    upload/3,
    delete/2
]).

-export([
    remote_fileinfo/2,
    remote_upload/3,
    remote_delete/2
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(REMOTE, ?MODULE).

%% ====================================================================
%% Public functions
%% ====================================================================

%% -------------------------------------
%% Download file from any node of msvc "fs" having the same FileInfo
%% Call to nodes one by one, check fileinfo. When file is found - start copy and return.
%% -------------------------------------
download(ConfigKey,StorePath,ToFilepath0,CheckFileInfo) ->
    ToFilepath = ?BU:to_unicode_list(ToFilepath0),
    HashA = case is_map(CheckFileInfo) of true -> maps:get(<<"hasha">>,CheckFileInfo,maps:get(hasha,CheckFileInfo,undefined)); _ -> undefined end,
    {M,F,A} = {?REMOTE, remote_fileinfo, [ConfigKey,StorePath]},
    lists:foldl(fun(_,{ok,_}=Acc) -> Acc;
                   (FromNode,Acc) ->
                        case ?BLrpc:call(FromNode, M,F,A, 5000) of
                            {badrpc,_} -> Acc;
                            {error,_}=Err -> Err;
                            {ok,#{hasha:=HashA1}=FileInfo,FromPath} when HashA1==HashA; HashA==undefined ->
                                case ?COPIER:copy_file(FromNode,FromPath,ToFilepath) of
                                    ok -> {ok,FileInfo};
                                    {error,_}=Err -> Err
                                end;
                            {ok,_,_} -> {error,{not_found,<<"File version not found">>}}
                        end end, default_error(), fs_nodes()).

%% -------------------------------------
%% Download file from any node of msvc "fs" having the same FileInfo
%% Call to nodes one by one, check fileinfo. When file is found - start copy and return.
%% -------------------------------------
download(ConfigKey,StorePath,ToFilepath0) ->
    ToFilepath = ?BU:to_unicode_list(ToFilepath0),
    {M,F,A} = {?REMOTE, remote_fileinfo, [ConfigKey,StorePath]},
    lists:foldl(fun(_,{ok,_}=Acc) -> Acc;
                   (FromNode,Acc) ->
                        case ?BLrpc:call(FromNode, M,F,A, 5000) of
                            {badrpc,_} -> Acc;
                            {error,_}=Err -> Err;
                            {ok,FileInfo,FromPath} ->
                                case ?COPIER:copy_file(FromNode,FromPath,ToFilepath) of
                                    ok -> {ok,FileInfo};
                                    {error,_}=Err -> Err
                                end end end, default_error(), fs_nodes()).

%% -------------------------------------
%% Upload file to all nodes of msvc "fs"
%% Call to all fs nodes sequentally, any ok is ok.
%% -------------------------------------
upload(ConfigKey,StorePath,FromFilepath) ->
    {ok,FileInfo} = ?BU:file_info_hasha(FromFilepath),
    {M,F,A} = {?REMOTE, remote_upload, [ConfigKey,StorePath,{node(),FromFilepath,FileInfo}]},
    FAccRes = fun(_,ok) -> ok; (Res,_) -> Res end,
    lists:foldl(fun(Node,Acc) ->
                        case ?BLrpc:call(Node, M,F,A, 30000) of
                            {badrpc,_} -> FAccRes(default_error(),Acc);
                            Res -> FAccRes(Res,Acc)
                        end end, default_error(), fs_nodes()).

%% -------------------------------------
%% Delete file from all nodes of msvc "fs"
%% Call to all fs nodes parallel, any ok is ok
%% -------------------------------------
delete(ConfigKey,StorePath) ->
    MFA = {?REMOTE, remote_delete, [ConfigKey,StorePath]},
    ResMulti = ?BLmulticall:call_direct(fs_nodes(),MFA,5000),
    lists:foldl(fun(_,ok) -> ok;
                   ({_Node,ok},_) -> ok;
                   ({_Node,{error,_}=Err},_) -> Err
                end, default_error(), ResMulti).

%% ====================================================================
%% Remote functions
%% ====================================================================

%% -------------------------------
%% Upload file to remote node. Executed on remote node
%% -------------------------------
remote_fileinfo(ConfigKey,StorePath) ->
    AbsPath = build_storage_path(ConfigKey,StorePath),
    case ?BU:file_info_hasha(AbsPath) of
        {ok,FileInfo} -> {ok,FileInfo,AbsPath};
        {error,_}=Err -> Err
    end.

%% -------------------------------
%% Upload file to remote node. Executed on remote node.
%% Skips upload if same version of file is already found at path.
%% -------------------------------
remote_upload(ConfigKey,StorePath,{FromNode,FromPath0,FileInfo}) ->
    FromPath = ?BU:to_unicode_list(FromPath0),
    ToPath = build_storage_path(ConfigKey,StorePath),
    case ?BU:file_info_hasha(ToPath) of
        {ok,FileInfo} -> ok;
        _ -> ?COPIER:copy_file(FromNode,FromPath,ToPath)
    end.

%% -------------------------------
%% Delete file from remote node
%% -------------------------------
remote_delete(ConfigKey,StorePath) ->
    AbsPath = build_storage_path(ConfigKey,StorePath),
    case filelib:is_regular(AbsPath) of
        true -> file:delete(AbsPath);
        false -> {error,not_found}
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
default_error() ->
    {error,{internal_error,<<"Storage unavailable">>}}.

%% Return nodes of msvc 'fs'
fs_nodes() ->
    ?PCFG:get_nodes_by_msvc(<<"fs">>).

%% -----------------------------
%% Return local absolute path to resource
%% -----------------------------
build_storage_path(ConfigKey, Path) ->
    filename:join([
        get_server_path(ConfigKey),
        ?BU:to_unicode_list(Path)]).

%% -----------------------------
%% return path to server's configured dir
%% TODO: optimize by caching or direct function
%% -----------------------------
get_server_path(ConfigKey) ->
    SrvInfo = ?PCFG:get_server_info(),
    SrvParams = maps:get(<<"params">>,SrvInfo),
    ServerPaths = maps:get(<<"paths">>,SrvParams),
    case maps:get(ConfigKey,ServerPaths,undefined) of
        undefined -> throw({error,{internal_error,?BU:strbin("Invalid configuration. Server path not found: '~ts'",[ConfigKey])}});
        <<"alias://",_/binary>>=Alias ->
            case ?PCFG:get_alias_value(Alias) of
                {ok,Value} -> ?BU:to_list(Value);
                {error,_} -> throw({error,{internal_error,?BU:strbin("Invalid configuration. Alias not found: '~ts'",[Alias])}})
            end;
        Value -> ?BU:to_unicode_list(Value)
    end.

