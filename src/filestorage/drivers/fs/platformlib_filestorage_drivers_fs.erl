%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.06.2021
%%% @doc

-module(platformlib_filestorage_drivers_fs).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    download/3, download/4,
    upload/3,
    delete/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------
%% Download file from S3 storage
%% -----------------------------

%% check fileinfo
download(Storages,StorePath,ToFilepath) ->
    do_download(Storages,StorePath,ToFilepath,undefined,undefined).

download(Storages,StorePath,ToFilepath,CheckFileInfo) ->
    do_download(Storages,StorePath,ToFilepath,CheckFileInfo,undefined).

%% @private
do_download([],_,_,_,LastError) when LastError/=undefined -> LastError;
do_download([Storage|Rest],StorePath,ToFilepath,CheckFileInfo,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err);
                {ok, ConfigKey, Prefix} ->
                    InStoragePath = build_fs_filepath(Prefix, StorePath),
                    case catch ?FS:download(ConfigKey, InStoragePath, ToFilepath, CheckFileInfo) of
                        {ok, _Result} ->
                            ?LOG('$trace', "FS download ok => ~120tp", [?BU:to_unicode_list(InStoragePath)]),
                            {ok, ?BU:to_binary(ToFilepath)};
                        {'EXIT', Exit} ->
                            ?LOG('$crash', "FS download crashed: ~120tp", [Exit]),
                            do_download(Rest,StorePath,ToFilepath,CheckFileInfo,{error,Exit});
                        {error, Reason}=Err ->
                            ?LOG('$error', "FS download error: ~120tp", [Reason]),
                            do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err)
                    end end end.

%% -----------------------------
%% Upload file to S3 storage
%% -----------------------------
upload(Storages,StorePath,FromFilepath) ->
    do_upload(Storages,StorePath,FromFilepath,undefined).

%% @private
do_upload([],_,_,LastError) when LastError/=undefined -> LastError;
do_upload([Storage|Rest],StorePath,FromFilepath,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_upload(Rest,StorePath,FromFilepath,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_upload(Rest,StorePath,FromFilepath,Err);
                {ok, ConfigKey, Prefix} ->
                    InStoragePath = build_fs_filepath(Prefix, StorePath),
                    case ?FS:upload(ConfigKey, InStoragePath, FromFilepath) of
                        ok ->
                            ?LOG('$trace', "FS upload ok => ~120tp", [?BU:to_unicode_list(InStoragePath)]),
                            StorageEntityId = maps:get('storage_entity_id',InternalMap),
                            {ok,?BU:strbin("fs://~ts/~ts",[StorageEntityId,InStoragePath])};
                        {error,Reason}=Err ->
                            ?LOG('$error', "FS upload error: ~120tp", [Reason]),
                            do_upload(Rest,StorePath,FromFilepath,Err)
                    end end end.

%% -----------------------------
%% Delete file from S3 storage
%% -----------------------------
delete(Storages,StorePath) ->
    do_delete(Storages,StorePath,undefined).

%% @private
do_delete([],_,LastError) when LastError/=undefined -> LastError;
do_delete([Storage|Rest],StorePath,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_delete(Rest,StorePath,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_delete(Rest,StorePath,Err);
                {ok, ConfigKey, Prefix} ->
                    InStoragePath = build_fs_filepath(Prefix, StorePath),
                    case ?FS:delete(ConfigKey, InStoragePath) of
                        ok ->
                            ?LOG('$trace', "FS delete ok => ~120tp", [?BU:to_unicode_list(InStoragePath)]),
                            ok;
                        {'EXIT', Exit} ->
                            ?LOG('$crash', "FS delete crashed: ~120tp", [Exit]),
                            do_delete(Rest,StorePath,{error,Exit});
                        {error, Reason}=Err ->
                            ?LOG('$error', "FS delete error: ~120tp", [Reason]),
                            do_delete(Rest,StorePath,Err)
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Return special InternalMap built from params of StorageItem
%% -------------------------------------
extract_storage_params(StorageItem) when is_map(StorageItem) ->
    case maps:get(<<"type">>,StorageItem) of
        <<"fs">> -> extract_storage_params_1(StorageItem);
        _ -> {error,{internal_error,<<"Invalid storage type. Expected 'fs'.">>}}
    end.
%% @private
extract_storage_params_1(StorageItem) when is_map(StorageItem) ->
    Id = maps:get(<<"id">>,StorageItem),
    Params = maps:get(<<"params">>,StorageItem),
    Keys = [<<"configkey">>,<<"prefix">>],
    Vals = lists:map(fun(K) -> case maps:get(K, Params, undefined) of
                                   V when is_binary(V); is_integer(V); is_atom(V) -> ?BU:to_binary(V)
                               end end, Keys),
    [ConfigKey,Prefix] = Vals,
    {ok, #{storage_entity_id => Id,
           configkey => ConfigKey,
           prefix => Prefix}}.

%% -------------------------------------
%% FS routines
%% -------------------------------------
extract_params(InternalMap) ->
    case {maps:get(configkey,InternalMap),maps:get(prefix,InternalMap)} of
        {undefined,_} -> {error, {invalid_params,<<"configkey is not defined">>}};
        {ConfigKey,Prefix} when is_binary(ConfigKey) ->
            {ok,ConfigKey,Prefix}
    end.

%% -----------------------------
%% build in-storage path for nfs
%% -----------------------------
build_fs_filepath(<<>>, StorePath) -> ?BU:to_unicode_list(StorePath);
build_fs_filepath(Prefix,AttachmentPath) ->
    filename:join([
        ?BU:to_unicode_list(Prefix),
        ?BU:to_unicode_list(AttachmentPath)]).