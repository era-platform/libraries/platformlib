%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 12.08.2021
%%% @doc

-module(platformlib_filestorage_drivers_fsync).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    download/3, download/4,
    upload/3,
    delete/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------
%% Download file from FSync storage
%% -----------------------------
download(Storages,StorePath,ToFilepath) ->
    do_download(Storages,StorePath,ToFilepath,undefined,undefined).

download(Storages,StorePath,ToFilepath,CheckFileInfo) ->
    do_download(Storages,StorePath,ToFilepath,CheckFileInfo,undefined).

%% @private
do_download([],_,_,_,LastError) when LastError/=undefined -> LastError;
do_download([Storage|Rest],StorePath,ToFilepath,CheckFileInfo,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err);
                {ok, Prefix} ->
                    case get_fsync_dir(Storage) of
                        undefined -> {error,{not_found,<<"Storage not found (fsync/download)">>}};
                        FSyncDir ->
                            InStoragePath = build_fsync_filepath(Prefix, StorePath),
                            AbsFilepath = ?BU:to_unicode_list(filename:join([FSyncDir,InStoragePath])),
                            case filelib:is_regular(AbsFilepath) of
                                true ->
                                    case ?StorageUtils:check_file_info(ToFilepath,CheckFileInfo) of
                                        ok -> {ok, ?BU:to_binary(AbsFilepath)};
                                        {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err)
                                    end;
                                % % if copy always to temp
                                % true ->
                                %    file:copy(AbsFilepath,ToFilepath),
                                %    ?FILE_AUTODELETE_SRV:register_filepath(ToFilepath,?TempFileTTL),
                                %    {ok, ToFilepath};
                                false -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,{error,{not_found,<<"File not found (fsync/download)">>}})
                            end end end end.

%% -----------------------------
%% Upload file to FSync storage
%% -----------------------------
upload(Storages,StorePath,FromFilepath) ->
    do_upload(Storages,StorePath,FromFilepath,undefined).

%% @private
do_upload([],_,_,LastError) when LastError/=undefined -> LastError;
do_upload([Storage|Rest],StorePath,FromFilepath,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_upload(Rest,StorePath,FromFilepath,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_upload(Rest,StorePath,FromFilepath,Err);
                {ok, Prefix} ->
                    case get_fsync_dir(Storage) of
                        undefined -> {error,{not_found,<<"Storage not found (fsync/download)">>}};
                        FSyncDir ->
                            InStoragePath = build_fsync_filepath(Prefix, StorePath),
                            AbsFilepath = ?BU:to_unicode_list(filename:join([FSyncDir,InStoragePath])),
                            ?BLfilelib:ensure_dir(AbsFilepath),
                            case ?BLfile:copy(FromFilepath,AbsFilepath) of
                                {ok,_} ->
                                    StorageEntityId = maps:get('storage_entity_id',InternalMap),
                                    {ok,?BU:strbin("fsync://~ts/~ts",[StorageEntityId,InStoragePath])};
                                {error,Reason}=Err ->
                                    ?LOG('$error', "FSync put_file error: ~120tp", [Reason]),
                                    do_upload(Rest,StorePath,FromFilepath,Err)
                            end end end end.

%% -----------------------------
%% Delete file from FSync storage
%% -----------------------------
delete(Storages,StorePath) ->
    do_delete(Storages,StorePath,undefined).

%% @private
do_delete([],_,LastError) when LastError/=undefined -> LastError;
do_delete([Storage|Rest],StorePath,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_delete(Rest,StorePath,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_delete(Rest,StorePath,Err);
                {ok, Prefix} ->
                    case get_fsync_dir(Storage) of
                        undefined -> {error,{not_found,<<"Storage not found (fsync/download)">>}};
                        FSyncDir ->
                            InStoragePath = build_fsync_filepath(Prefix, StorePath),
                            AbsFilepath = ?BU:to_unicode_list(filename:join([FSyncDir,InStoragePath])),
                            case filelib:is_regular(AbsFilepath) of
                                true -> ?BLfile:delete(AbsFilepath);
                                false -> do_delete(Rest,StorePath,{error,{not_found,<<"File not found (fsync/delete)">>}})
                            end end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Return special InternalMap built from params of StorageItem
%% -------------------------------------
extract_storage_params(StorageItem) when is_map(StorageItem) ->
    case maps:get(<<"type">>,StorageItem) of
        <<"fsync">> -> extract_storage_params_1(StorageItem);
        _ -> {error,{internal_error,<<"Invalid storage type. Expected 'fsync'.">>}}
    end.
%% @private
extract_storage_params_1(StorageItem) when is_map(StorageItem) ->
    Id = maps:get(<<"id">>,StorageItem),
    Params = maps:get(<<"params">>,StorageItem),
    Keys = [<<"configkey">>,<<"prefix">>],
    Vals = lists:map(fun(K) -> case maps:get(K, Params, undefined) of
                                   V when is_binary(V); is_integer(V); is_atom(V) -> ?BU:to_binary(V)
                               end end, Keys),
    [ConfigKey,Prefix] = Vals,
    {ok, #{storage_entity_id => Id,
           configkey => ConfigKey,
           prefix => Prefix}}.

%% -------------------------------------
%% FSync routines
%% -------------------------------------
extract_params(InternalMap) ->
    case maps:get(prefix,InternalMap) of
        Prefix -> {ok,Prefix}
    end.

%%
get_fsync_dir(Storage) ->
    Domain = maps:get('x_domain',Storage),
    CurAddr = ?BU:node_addr(node()),
    case lists:filter(fun({Node,_}) -> ?BU:node_addr(Node)==CurAddr end, ?PCFG:get_msvc_opts(<<"fsync">>)) of
        [] -> undefined;
        [{_,Opts}] ->
            SyncPath = case maps:get('sync_folder_path',Opts,undefined) of
                           undefined ->
                               {ok,CWD} = file:get_cwd(),
                               filename:join(CWD,"sync_folder");
                           Path -> ?BU:to_list(Path)
                       end,
            SyncDomPath = filename:join([SyncPath, "domains", ?BU:to_list(Domain)]),
            case filelib:is_dir(SyncDomPath) of
                false -> undefined;
                true -> filename:join(SyncDomPath,"storage")
            end end.

%% -----------------------------
%% build in-storage path for fsync
%% -----------------------------
build_fsync_filepath(<<>>, StorePath) -> ?BU:to_unicode_list(StorePath);
build_fsync_filepath(Prefix, StorePath) ->
    filename:join([
        ?BU:to_unicode_list(Prefix),
        ?BU:to_unicode_list(StorePath)]).