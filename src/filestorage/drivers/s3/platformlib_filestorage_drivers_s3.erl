%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.06.2021
%%% @doc FileStorage of type S3 operations.

-module(platformlib_filestorage_drivers_s3).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    download/3, download/4,
    upload/3,
    delete/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------
%% Download file from S3 storage
%% -----------------------------
download(Storages,StorePath,ToFilepath) ->
    do_download(Storages,StorePath,ToFilepath,undefined,undefined).

download(Storages,StorePath,ToFilepath,CheckFileInfo) ->
    do_download(Storages,StorePath,ToFilepath,CheckFileInfo,undefined).

%% @private
do_download([],_,_,_,LastError) when LastError/=undefined -> LastError;
do_download([Storage|Rest],StorePath,ToFilepath,CheckFileInfo,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err);
                {ok, Bucket, Prefix, S3Conf} ->
                    S3FilePath = build_s3_filepath(Prefix, StorePath),
                    case catch ?S3:get_file(?BU:to_unicode_list(Bucket), S3FilePath, ToFilepath, S3Conf) of
                        {ok, _Result} ->
                            case ?StorageUtils:check_file_info(ToFilepath,CheckFileInfo) of
                                ok ->
                                    ?LOG('$trace', "S3 get_file ok from bucket ~120tp => ~120tp", [?BU:to_unicode_list(Bucket), ?BU:to_unicode_list(S3FilePath)]),
                                    {ok, ?BU:to_binary(ToFilepath)};
                                {error,_}=Err -> do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err)
                            end;
                        {'EXIT', Exit} ->
                            ?LOG('$crash', "S3 get_file crashed: ~120tp", [Exit]),
                            do_download(Rest,StorePath,ToFilepath,CheckFileInfo,{error,Exit});
                        {error, Reason}=Err ->
                            ?LOG('$error', "S3 get_file error: ~120tp", [Reason]),
                            do_download(Rest,StorePath,ToFilepath,CheckFileInfo,Err)
                    end end end.

%% -----------------------------
%% Upload file to S3 storage
%% -----------------------------
upload(Storages,StorePath,FromFilepath) ->
    do_upload(Storages,StorePath,FromFilepath,undefined).

%% @private
do_upload([],_,_,LastError) when LastError/=undefined -> LastError;
do_upload([Storage|Rest],StorePath,FromFilepath,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_upload(Rest,StorePath,FromFilepath,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_upload(Rest,StorePath,FromFilepath,Err);
                {ok, Bucket, Prefix, S3Conf} ->
                    S3FilePath = build_s3_filepath(Prefix, StorePath),
                    case catch ?S3:put_file(?BU:to_unicode_list(Bucket), S3FilePath, ?BU:to_unicode_list(FromFilepath), S3Conf) of
                        {ok, _Result} ->
                            ?LOG('$trace', "S3 put_file ok to bucket ~120tp => ~120tp", [Bucket, S3FilePath]),
                            StorageEntityId = maps:get('storage_entity_id',InternalMap),
                            {ok, ?BU:strbin("s3://~ts/~ts",[StorageEntityId,S3FilePath])};
                        {'EXIT', Exit} ->
                            ?LOG('$crash', "S3 put_file crashed: ~120tp", [Exit]),
                            do_upload(Rest,StorePath,FromFilepath,{error,Exit});
                        {error, Reason}=ErrS3 ->
                            ?LOG('$error', "S3 put_file error: ~120tp", [Reason]),
                            do_upload(Rest,StorePath,FromFilepath,ErrS3)
                    end end end.

%% -----------------------------
%% Delete file from S3 storage
%% -----------------------------
delete(Storages,StorePath) ->
    do_delete(Storages,StorePath,undefined).

%% @private
do_delete([],_,LastError) when LastError/=undefined -> LastError;
do_delete([Storage|Rest],StorePath,_) ->
    case extract_storage_params(Storage) of
        {error,_}=Err -> do_delete(Rest,StorePath,Err);
        {ok,InternalMap} ->
            case extract_params(InternalMap) of
                {error,_}=Err -> do_delete(Rest,StorePath,Err);
                {ok, Bucket, Prefix, S3Conf} ->
                    S3FilePath = build_s3_filepath(Prefix, StorePath),
                    case catch ?S3:delete(?BU:to_unicode_list(Bucket), S3FilePath, S3Conf) of
                        {ok, _Result} ->
                            ?LOG('$trace', "S3 delete ok from bucket ~120tp => ~120tp", [?BU:to_unicode_list(Bucket), ?BU:to_unicode_list(S3FilePath)]),
                            ok;
                        {'EXIT', Exit} ->
                            ?LOG('$crash', "S3 delete crashed: ~120tp", [Exit]),
                            do_delete(Rest,StorePath,{error,Exit});
                        {error, Reason}=ErrS3 ->
                            ?LOG('$error', "S3 delete error: ~120tp", [Reason]),
                            do_delete(Rest,StorePath,ErrS3)
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------------------------
%% Return special InternalMap built from params of StorageItem
%% -------------------------------------
extract_storage_params(StorageItem) when is_map(StorageItem) ->
    case maps:get(<<"type">>,StorageItem) of
        <<"s3">> -> extract_storage_params_1(StorageItem);
        _ -> {error,{internal_error,<<"Invalid storage type. Expected 's3'.">>}}
    end.
%% @private
extract_storage_params_1(StorageItem) ->
    Id = maps:get(<<"id">>,StorageItem),
    Params = maps:get(<<"params">>,StorageItem),
    Keys = [<<"bucket">>,<<"key">>,<<"secret">>,<<"region">>,<<"endpoint">>,<<"prefix">>],
    Vals = lists:map(fun(K) -> case maps:get(K, Params, undefined) of
                                   undefined -> undefined;
                                   V when is_binary(V); is_integer(V); is_atom(V) -> ?BU:to_binary(V)
                               end end, Keys),
    [Bucket,Key,Secret,Region,Endpoint,Prefix] = Vals,
    {ok, #{storage_entity_id => Id,
           bucket => Bucket,
           key => Key,
           secret => Secret,
           region => Region,
           endpoint => Endpoint,
           prefix => Prefix}}.

%% -------------------------------------
%% S3 routines
%% -------------------------------------
extract_params(InternalMap) ->
    case {maps:get(bucket,InternalMap),maps:get(prefix,InternalMap)} of
        {undefined,_} -> {error, {invalid_params,<<"bucket is not defined">>}};
        {Bucket,Prefix} when is_binary(Bucket) ->
            S3Conf = prepare_s3_conf(InternalMap),
            {ok, Bucket, Prefix, S3Conf}
    end.

%% @private
prepare_s3_conf(InternalMap) ->
    Opts = prepare_s3_opts_endpoint(InternalMap),
    Mapping = [{key, access_key_id},
               {secret, secret_access_key}],
    lists:foldl(fun({MK,OK}, AccOpts) ->
                        case maps:get(MK, InternalMap) of
                            undefind ->  AccOpts;
                            V -> AccOpts#{OK => ?BU:to_unicode_list(V)}
                        end end, Opts, Mapping).

%% @private
prepare_s3_opts_endpoint(InternalMap) ->
    % if both 'endpoint' and 'region' are unset, pass empty map
    % if 'endpoint' is set, ignore 'region' and pass s3_scheme+s3_host+s3_port after parsing url
    % if 'endpoint' is not set, but 'region' is set, pass aws_region
    Endpoint = maps:get(endpoint, InternalMap),
    Region = maps:get(region, InternalMap),
    case {Endpoint,Region} of
        {undefined,undefined} -> #{};
        {undefined,_} -> #{aws_region => ?BU:to_unicode_list(Region)};
        _ ->
            % endpoint must be complete url like http(s)://hostname:port
            %case http_uri:parse(Endpoint, [{scheme_validation_fun,SchValFun}] % TODO scheme_validation_fun
            case uri_string:parse(Endpoint) of
                {error,Reason} ->
                    ?LOG('$error',"Error parsing S3 endpoint URL '~ts' in domain storage settings: ~tp", [?BU:to_unicode_list(Endpoint), Reason]),
                    #{};
                UriMap when map_size(UriMap)==0 ->
                    ?LOG('$warning',"Empty parsing result S3 endpoint URL '~ts' in domain storage settings: ~tp", [?BU:to_unicode_list(Endpoint)]),
                    #{};
                UriMap when is_map(UriMap) ->
                    [SchemeStr,Host] = ?BU:maps_get([scheme,host],UriMap),
                    SchemeL = ?BU:to_binary(?BU:to_lower(SchemeStr)),
                    Port = case maps:get(port,UriMap,undefined) of
                               undefined when SchemeL== <<"http">> -> 80;
                               undefined when SchemeL== <<"https">> -> 443;
                               P when P/=undefined -> P
                           end,
                    #{s3_scheme => ?BU:str("~s://", [SchemeL]),
                      s3_host => ?BU:to_list(Host),
                      s3_port => Port,
                      s3_bucket_access_method => path}
            end end.

%% -----------------------------
%% build in-storage path for s3
%% -----------------------------
build_s3_filepath(<<>>, StorePath) -> ?BU:to_unicode_list(StorePath);
build_s3_filepath(Prefix, StorePath) ->
    filename:join([
        ?BU:to_unicode_list(Prefix),
        ?BU:to_unicode_list(StorePath)]).

%% ====================================================================
%% Internal functions
%% ====================================================================