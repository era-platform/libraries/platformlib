%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 21.06.2021
%%% @doc

-module(platformlib_filestorage_drivers_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_server_path/1]).
-export([check_file_info/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------
%% return path to server's configured dir
%% TODO: optimize by caching or direct function
%% -----------------------------
get_server_path(ConfigKey) ->
    SrvInfo = ?PCFG:get_server_info(),
    SrvParams = maps:get(<<"params">>,SrvInfo),
    ServerPaths = maps:get(<<"paths">>,SrvParams),
    case maps:get(ConfigKey,ServerPaths,undefined) of
        undefined -> throw({error,{internal_error,?BU:strbin("Invalid configuration. Server path not found: '~ts'",[ConfigKey])}});
        <<"alias://",_/binary>>=Alias ->
            case ?PCFG:get_alias_value(Alias) of
                {ok,Value} -> ?BU:to_list(Value);
                {error,_} -> throw({error,{internal_error,?BU:strbin("Invalid configuration. Alias not found: '~ts'",[Alias])}})
            end;
        Value -> ?BU:to_list(Value)
    end.

%% -----------------------------
%%
%% -----------------------------
check_file_info(_,undefined) -> ok;
check_file_info(Filepath,CheckFileInfo) ->
    HashA = maps:get(hasha,CheckFileInfo,maps:get(<<"hasha">>,CheckFileInfo,undefined)),
    case HashA of
        undefined -> ok;
        _ ->
            case ?BU:file_info_hasha(Filepath) of
                {ok,#{hasha:=HashA}} -> ok;
                {ok,_} -> {error,{not_found,<<"File version not found">>}};
                {error,_}=Err -> Err
            end end.

%% ====================================================================
%% Internal functions
%% ====================================================================