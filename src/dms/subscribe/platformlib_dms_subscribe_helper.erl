%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 25.05.2021
%%% @doc Helps use async subscription for dms events.
%%%     Params:
%%%         ModKey
%%%         Domain
%%%         ClassName
%%%         FromPid
%%%         SubscrId
%%%         Opts
%%%             <<"filter">>
%%%             <<"mask">>
%%%             <<"ttl">>
%%%             <<"ttl_first">>
%%%             fun_report :: function(ok|retry|failure|error|cancelled})

-module(platformlib_dms_subscribe_helper).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([subscribe_async/5,subscribe_async/6]).
-export([loop_subscribe/7]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(NoLogIterations, 6).

%% ====================================================================
%% Public functions
%% ====================================================================

subscribe_async(ModKey,Domain,ClassName,FromPid,SubscrId) ->
    subscribe_async(ModKey,Domain,ClassName,FromPid,SubscrId,#{}).

subscribe_async(ModKey,Domain,ClassName,FromPid,SubscrId,Opts) ->
    spawn_link(fun() ->
                    ?LOG('$info',"~ts. '~ts' Subscribe helper '~ts' inited (sid=~120tp)", [ModKey,Domain,ClassName,SubscrId]),
                    loop_subscribe(ModKey,Domain,ClassName,FromPid,SubscrId,Opts,0)
               end).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
loop_subscribe(ModKey,Domain,ClassName,FromPid,SubscrId,Opts,Iteration) ->
    % ttl. First ttl could be defined to another value
    TTL = maps:get(<<"ttl">>,Opts,300),
    TTL_iter = case Iteration of
                   0 -> maps:get(<<"ttl_first">>,Opts,TTL);
                   _ -> TTL
               end,
    % Main operation
    {Result,Timeout} = case do_subscribe(ModKey,Domain,ClassName,FromPid,SubscrId,TTL_iter,Opts,Iteration) of
                           {ok,SubscrId} ->
                               % millisec and coefficient 4/5
                               {ok, TTL * 800};
                           unsubscribed ->
                               {cancelled, 1000};
                           {retry_after,RetryTimeout,_} ->
                               {retry, RetryTimeout};
                           undefined ->
                               {failure, 5000};
                           {error,_} ->
                               {error, 10000}
                       end,
    % reporting
    Opts1 = case maps:get('last_result',Opts,undefined) of
                Result -> Opts;
                _ ->
                    % send report
                    case maps:get('fun_report',Opts,undefined) of
                        FunReport when is_function(FunReport,1) -> FunReport(Result);
                        _ -> ok
                    end,
                    % store last result
                    Opts#{'last_result' => Result}
            end,
    % wait for expiration
    receive {stop,SubscrId} -> ok
    after Timeout -> ?MODULE:loop_subscribe(ModKey,Domain,ClassName,FromPid,SubscrId,Opts1,Iteration+1)
    end.

%% @private
do_subscribe(ModKey,Domain,ClassName,FromPid,SubscrId,TTL,Opts,Iteration) ->
    Value = build_subscription_value(ClassName,FromPid,Opts),
    PutReq = {put, [SubscrId, Value, TTL]},
    GlobalName = ?GN_NAMING:get_globalname(?MsvcSubscr,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName, PutReq) of
        {'EXIT',_Reason} when Iteration < ?NoLogIterations ->
            undefined;
        {'EXIT',Reason} ->
            ?LOG('$warning',"~ts. '~ts' Subscribe helper call to SUBSCR (put) crashed: ~n\t~120tp",[ModKey,Domain,Reason]),
            undefined;
        {error,_}=Err when Iteration < ?NoLogIterations ->
            Err;
        {error,_}=Err ->
            ?LOG('$error',"~ts. '~ts' Subscribe helper call to SUBSCR (put) error: ~n\t~120tp",[ModKey,Domain,Err]),
            Err;
        {retry_after,_,_}=Retry when Iteration < ?NoLogIterations ->
            Retry;
        {retry_after,_,_}=Retry ->
            ?LOG('$info',"~ts. '~ts' Subscribe helper call to SUBSCR (put) got ~120tp", [ModKey,Domain,Retry]),
            Retry;
        undefined -> undefined;
        ok -> unsubscribed;
        {ok,SubscrId} ->
            {ok,SubscrId}
    end.

%% @private
build_subscription_value(ClassName,FromPid,Opts) ->
    FilterMap = maps:with([<<"filter">>],Opts),
    MaskMap = maps:with([<<"mask">>],Opts),
    ExArgs = maps:merge(FilterMap,MaskMap),
    #{<<"subscriber">> => #{<<"type">> => 'service',
                            <<"site">> => ?PCFG:get_current_site(),
                            <<"node">> => node(),
                            <<"pid">> => FromPid,
                            <<"ts">> => ?BU:timestamp(),
                            <<"opts">> => []
                           },
      <<"events">> => [<<"modelevents.data_changed">>],
      <<"objects">> => [ClassName],
      <<"exargs">> => ExArgs}.
