%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.06.2021
%%% @doc Reading from DMS routines. Uses sync waiting for timeout, if is not ready or something going wrong

-module(platformlib_dms_cache_sync).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check_sync/3]).

-export([read_direct_sync/4,
         read_nocache_sync/5,
         read_cache_sync/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(CacheGN, cache_globalname).
-define(TimeoutSync,timeout_sync).
-define(Attempts,attempts).
-define(TimeoutBetweenAttempts,timeout_between_attempts).
-define(TimeoutBetweenAttemptsAcc,timeout_between_attempts_acc).
-define(StartTS,start_ts).
-define(NowTS,now_ts).
-define(LastResult,last_result).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ---------------------------------------
%% Simple check request to DMS class pid
%%   Sync wait for timeout if something not ready or going wrong
%% SyncOpts keys:
%%   cache_globalname (=true)
%%   timeout_sync (=5000),
%%   attempts (=5),
%%   timeout_between_attempts (=100, could be function/1)
%% ---------------------------------------
-spec check_sync(Domain::binary() | atom(), ClassName::binary(), Opt::TimeoutSync | SyncOpts) ->
    {ok,HC::non_neg_integer()} | {error,Reason::term()}
    when TimeoutSync::non_neg_integer(), SyncOpts::map().
%% ---------------------------------------
check_sync(Domain, ClassName, TimeoutSync) when is_integer(TimeoutSync), TimeoutSync > 0 ->
    do_check_sync(Domain,ClassName,initialize_opts(#{?TimeoutSync => TimeoutSync}));

check_sync(Domain, ClassName, SyncOpts) when is_map(SyncOpts) ->
    do_check_sync(Domain,ClassName,initialize_opts(SyncOpts)).

%% -----
%% @private
do_check_sync(_, _, #{?Attempts:=Attempts}=SyncOpts) when Attempts==0 ->
    build_error(maps:get(?LastResult,SyncOpts));
do_check_sync(_, _, #{?TimeoutSync:=TimeoutSync, ?StartTS:=StartTS, ?NowTS:=NowTS}=SyncOpts) when NowTS-StartTS > TimeoutSync ->
    build_error(maps:get(?LastResult,SyncOpts));
do_check_sync(Domain, ClassName, SyncOpts) ->
    TimeoutSync = maps:get(?TimeoutSync,SyncOpts),
    case catch call_dms(Domain,{get_hc,ClassName,TimeoutSync},TimeoutSync,SyncOpts) of
        {'EXIT',Reason} ->
            do_check_sync(Domain,ClassName,new_opts(SyncOpts,Reason));
        {error,_}=Err ->
            do_check_sync(Domain,ClassName,new_opts(SyncOpts,Err));
        {ok,HC} -> {ok,HC}
    end.

%% ---------------------------------------
%% Request to read from DMS directly without checking cache
%%   Sync wait for timeout if something not ready or going wrong
%% SyncOpts keys:
%%   cache_globalname (=true)
%%   timeout_sync (=5000),
%%   attempts (=5),
%%   timeout_between_attempts (=100, could be function/1)
%% ---------------------------------------
-spec read_direct_sync(Domain::binary() | atom(), ClassName::binary(), ReadOptions::map(), Opt::TimeoutSync | SyncOpts) ->
    {ok,HC::non_neg_integer()} | {error,Reason::term()}
    when TimeoutSync::non_neg_integer(), SyncOpts::map().
%% ---------------------------------------
read_direct_sync(Domain, ClassName, ReadOptions, TimeoutSync) when is_integer(TimeoutSync), TimeoutSync > 0 ->
    do_read_direct_sync(Domain,ClassName,ReadOptions,initialize_opts(#{?TimeoutSync=>TimeoutSync}));

read_direct_sync(Domain, ClassName, ReadOptions, SyncOpts) when is_map(SyncOpts) ->
    do_read_direct_sync(Domain,ClassName,ReadOptions,initialize_opts(SyncOpts)).

%% -----
%% @private
do_read_direct_sync(_, _, _, #{?Attempts:=Attempts}=SyncOpts) when Attempts==0 ->
    build_error(maps:get(?LastResult,SyncOpts));
do_read_direct_sync(_, _, _, #{?TimeoutSync:=TimeoutSync, ?StartTS:=StartTS, ?NowTS:=NowTS}=SyncOpts) when NowTS-StartTS > TimeoutSync ->
    build_error(maps:get(?LastResult,SyncOpts));
do_read_direct_sync(Domain, ClassName, ReadOptions, SyncOpts) ->
    TimeoutSync = maps:get(?TimeoutSync,SyncOpts),
    Args = {'read',#{class => ClassName,
                     object => object(ReadOptions),
                     qs => append_option(ReadOptions,'no_max_limit',true)}},
    case catch call_dms(Domain,{crud,ClassName,Args,TimeoutSync},TimeoutSync,SyncOpts) of
        {'EXIT',Reason} ->
            do_read_direct_sync(Domain,ClassName,ReadOptions,new_opts(SyncOpts,Reason));
        {error,_}=Err ->
            do_read_direct_sync(Domain,ClassName,ReadOptions,new_opts(SyncOpts,Err));
        {ok,Result} ->
            {ok,Result}
    end.

%% ---------------------------------------
%% Request to read from DMS
%%    using hashcode
%%    without local caching
%%   Sync wait for timeout if something not ready or going wrong
%% SyncOpts keys:
%%   cache_globalname (=true)
%%   timeout_sync (=5000),
%%   attempts (=5),
%%   timeout_between_attempts (=100, could be function/1)
%% ---------------------------------------
-spec read_nocache_sync(Domain::binary() | atom(), ClassName::binary(), ReadOptions::map(), HC::term(), Opt::TimeoutSync | SyncOpts) ->
    {ok,[Item::map()],HC::term()} | {ok,not_changed} | {error,Reason::term()}
    when TimeoutSync::non_neg_integer(), SyncOpts::map().
%% ---------------------------------------
read_nocache_sync(Domain,ClassName,ReadOptions,HC,TimeoutSync) when is_integer(TimeoutSync), TimeoutSync > 0 ->
    do_read_nocache_sync(Domain,ClassName,ReadOptions,HC,initialize_opts(#{?TimeoutSync=>TimeoutSync}));

read_nocache_sync(Domain,ClassName,ReadOptions,HC,SyncOpts) ->
    do_read_nocache_sync(Domain,ClassName,ReadOptions,HC,initialize_opts(SyncOpts)).

%% -----
%% @private
do_read_nocache_sync(_, _, _, _, #{?Attempts:=Attempts}=SyncOpts) when Attempts==0 ->
    build_error(maps:get(?LastResult,SyncOpts));
do_read_nocache_sync(_, _, _, _, #{?TimeoutSync:=TimeoutSync, ?StartTS:=StartTS, ?NowTS:=NowTS}=SyncOpts) when NowTS-StartTS > TimeoutSync ->
    build_error(maps:get(?LastResult,SyncOpts));
do_read_nocache_sync(Domain, ClassName, ReadOptions, HC, #{?StartTS:=StartTS,?NowTS:=NowTS}=SyncOpts) ->
    TimeoutSync = maps:get(?TimeoutSync,SyncOpts),
    case catch call_dms(Domain,{get_hc,ClassName,TimeoutSync-(NowTS-StartTS)},TimeoutSync,SyncOpts) of
        {'EXIT',Reason} ->
            do_read_nocache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Reason));
        {error,_}=Err ->
            do_read_nocache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Err));
        {ok,HC} ->
            {ok,not_changed};
        {ok,HC1} ->
            Args = {'read',#{class => ClassName,
                             object => object(ReadOptions),
                             qs => append_option(ReadOptions,'no_max_limit',true)}},
            NowTS1 = ?BU:timestamp(),
            case catch call_dms(Domain,{crud,ClassName,Args,TimeoutSync-(NowTS1-StartTS)},TimeoutSync,SyncOpts) of
                {'EXIT',Reason} ->
                    do_read_nocache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Reason));
                {error,_}=Err ->
                    do_read_nocache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Err));
                {ok,Result} ->
                    {ok,Result,HC1}
            end end.

%% ---------------------------------------
%% Request to read from DMS
%%    using hashcode
%%    using local cache
%%   Sync wait for timeout if something not ready or going wrong
%% SyncOpts keys:
%%   cache_globalname (=true)
%%   timeout_sync (=5000),
%%   attempts (=5),
%%   timeout_between_attempts (=100, could be function/1)
%% ---------------------------------------
-spec read_cache_sync(Domain::binary() | atom(), ClassName::binary(), ReadOptions::map(), HC::term(), Opt::TimeoutSync | SyncOpts) ->
    {ok,[Item::map()],HC::term()} | {ok,not_changed} | {error,Reason::term()}
    when TimeoutSync::non_neg_integer(), SyncOpts::map().
%% ---------------------------------------
read_cache_sync(Domain,ClassName,ReadOptions,HC,TimeoutSync) when is_integer(TimeoutSync), TimeoutSync > 0 ->
    do_read_cache_sync(Domain,ClassName,ReadOptions,HC,initialize_opts(#{?TimeoutSync=>TimeoutSync}));

read_cache_sync(Domain,ClassName,ReadOptions,HC,SyncOpts) ->
    do_read_cache_sync(Domain,ClassName,ReadOptions,HC,initialize_opts(SyncOpts)).

%% -----
%% @private
do_read_cache_sync(_, _, _, _, #{?Attempts:=Attempts}=SyncOpts) when Attempts==0 ->
    build_error(maps:get(?LastResult,SyncOpts));
do_read_cache_sync(_, _, _, _, #{?TimeoutSync:=TimeoutSync, ?StartTS:=StartTS, ?NowTS:=NowTS}=SyncOpts) when NowTS-StartTS > TimeoutSync ->
    build_error(maps:get(?LastResult,SyncOpts));
do_read_cache_sync(Domain, ClassName, ReadOptions, HC, #{?StartTS:=StartTS,?NowTS:=NowTS}=SyncOpts) ->
    TimeoutSync = maps:get(?TimeoutSync,SyncOpts),
    case catch call_dms(Domain,{get_hc,ClassName,TimeoutSync-(NowTS-StartTS)},TimeoutSync,SyncOpts) of
        {'EXIT',Reason} ->
            do_read_cache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Reason));
        {error,_}=Err ->
            do_read_cache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Err));
        {ok,HC} ->
            {ok,not_changed};
        {ok,HC1} ->
            Key = {read_collection,{Domain,ClassName,ReadOptions}},
            case ?BLstore:find_t(Key) of
                {_,{HC1,{error,_}=Err}} -> Err;
                {_,{HC1,Result}} when is_list(Result); is_map(Result) ->
                    ?BLstore:store_t(Key,{HC1,Result},300000),
                    {ok,Result,HC1};
                _ ->
                    Args = {'read',#{class => ClassName,
                                     object => object(ReadOptions),
                                     qs => append_option(ReadOptions,'no_max_limit',true)}},
                    NowTS1 = ?BU:timestamp(),
                    case catch call_dms(Domain,{crud,ClassName,Args,TimeoutSync-(NowTS1-StartTS)},TimeoutSync,SyncOpts) of
                        {'EXIT',Reason} ->
                            do_read_cache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Reason));
                        {error,_}=Err ->
                            do_read_cache_sync(Domain,ClassName,ReadOptions,HC,new_opts(SyncOpts,Err));
                        {ok,Result} ->
                            ?BLstore:store_t(Key,{HC1,Result},300000),
                            {ok,Result,HC1}
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
object(ReadOptions) ->
    case maps:get(object,ReadOptions,undefined) of
        undefined -> 'collection';
        Object -> Object
    end.

%% @private
append_option(Opts,Key,Value) when is_list(Opts) -> [{Key,Value}|Opts];
append_option(Opts,Key,Value) when is_map(Opts) -> Opts#{Key => Value}.

%% @private
initialize_opts(SyncOpts) ->
    NowTS = ?BU:timestamp(),
    SyncOpts#{?CacheGN => maps:get(?CacheGN,SyncOpts,true),
              ?TimeoutSync => maps:get(?TimeoutSync,SyncOpts,5000),
              ?Attempts => maps:get(?Attempts,SyncOpts,5),
              ?TimeoutBetweenAttempts => maps:get(?TimeoutBetweenAttempts,SyncOpts,100),
              ?StartTS => NowTS,
              ?NowTS => NowTS,
              ?LastResult => undefined}.

%% @private
new_opts(SyncOpts,Result) ->
    SyncOpts1 = sleep_if_attempts(SyncOpts),
    Attempts = maps:get(?Attempts,SyncOpts1),
    SyncOpts1#{?NowTS => ?BU:timestamp(),
               ?Attempts => Attempts-1,
               ?LastResult => Result}.

%% @private
sleep_if_attempts(#{?Attempts:=Attempts}=SyncOpts) when Attempts =< 1 -> SyncOpts;
sleep_if_attempts(#{?TimeoutBetweenAttempts:=Timeout}=SyncOpts) when is_integer(Timeout) ->
    timer:sleep(Timeout),
    SyncOpts;
sleep_if_attempts(#{?TimeoutBetweenAttempts:=F}=SyncOpts) when is_function(F,1) ->
    Timeout = F(maps:get(?TimeoutBetweenAttemptsAcc,SyncOpts,undefined)),
    timer:sleep(Timeout),
    SyncOpts#{?TimeoutBetweenAttemptsAcc => Timeout}.

%% @private
call_dms(Domain,Req,Timeout,SyncOpts) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    case maps:get(?CacheGN,SyncOpts,true) of
        true -> ?GLOBAL:gen_server_call(GlobalName,Req,Timeout);
        false -> ?GLOBAL:gen_server_call_nocache(GlobalName,Req,Timeout)
    end.

%% @private
build_error({error,_}=Err) -> Err;
build_error({noproc,_}=Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,noproc};
build_error({timeout,_}=Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,timeout};
build_error({Other,_}=Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,Other};
build_error(Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,unknown_response}.
