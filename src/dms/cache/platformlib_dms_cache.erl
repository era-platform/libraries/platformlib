%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.05.2021
%%% @doc Requests to dms optimized by cache
%%%         If dms has no changes in class, so the same request would return same data.
%%%         This module allows to economy resources on regular calls.
%%%         For example, some module every minute requests for all items of any class, then it would check modification index only

-module(platformlib_dms_cache).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([check/2]).

-export([read_all_by_portions/5,read_all_by_portions/6,
         read_direct/3,
         read_nocache/4,
         read_cache/4]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================


%% ---------------------------------------
%% Simple check request to DMS class pid
%%   Sync wait for timeout if something not ready or going wrong
%% ---------------------------------------
-spec check(Domain::binary() | atom(), ClassName::binary()) -> {ok,HC::non_neg_integer()} | {error,Reason::term()}.
%% ---------------------------------------
check(Domain, ClassName) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName,{get_hc,ClassName}) of
        {'EXIT',Reason} -> {error,Reason};
        {error,_}=Err -> Err;
        {ok,HC} -> {ok,HC}
    end.

%% ---------------------------------------
%% Routine to read all entities from DMS (by portions)
%% ---------------------------------------
-spec read_all_by_portions(Domain::binary(),CN::binary(),{OrderField::binary(),OrderDir::binary()},Mask::[binary()],PortionSize::non_neg_integer())
      -> [Item::map()].
%% -----------------
read_all_by_portions(Domain,CN,{OrderField,OrderDir},Mask,PortionSize) ->
    EtsAcc = ets:new(tmp,[set]),
    ok = read_all_by_portions(Domain,CN,{OrderField,OrderDir},Mask,PortionSize,EtsAcc),
    {_,ItemsAcc} = lists:unzip(ets:tab2list(EtsAcc)),
    ets:delete(EtsAcc),
    ItemsAcc.

%% -----------------
-spec read_all_by_portions(Domain::binary(),CN::binary(),{OrderField::binary(),OrderDir::binary()},Mask::[binary()],PortionSize::non_neg_integer(),EtsAcc::ets:tab())
      -> ok.
%% -----------------
read_all_by_portions(Domain,CN,{OrderField,OrderDir},Mask,PortionSize,EtsAcc) ->
    case read_portion(0,PortionSize,[],{Domain,CN,{OrderField,OrderDir},Mask},EtsAcc) of
        {ok,Items} when length(Items) < PortionSize -> ok;
        {ok,Items} when length(Items) >= PortionSize ->
            [LastItem|_] = lists:reverse(Items),
            LastX = maps:get(OrderField,LastItem),
            read_next_portion(LastX,{Domain,CN,{OrderField,OrderDir},Mask,PortionSize},EtsAcc)
    end.

%% @private
read_portion(Offset,Limit,Filter,{Domain,CN,{OrderField,OrderDir},Mask},EtsAcc) ->
    ReadOpts = #{<<"filter">> => Filter,
                 <<"offset">> => Offset,
                 <<"limit">> => Limit,
                 <<"mask">> => Mask,
                 <<"order">> => [{OrderField,OrderDir}]},
    case ?DMS_CACHE:read_direct(Domain,CN,ReadOpts) of
        {error,_}=Err -> throw(Err);
        {ok,[]} -> {ok,[]};
        {ok,Items} ->
            lists:foreach(fun(Item) -> ets:insert(EtsAcc,{maps:get(<<"id">>,Item),Item}) end, Items),
            {ok,Items}
    end.

%% @private
read_next_portion(LastX,{Domain,CN,{OrderField,OrderDir},Mask,PortionSize},EtsAcc) ->
    Filter = [<<"<=">>,[<<"property">>,OrderField],LastX],
    case read_portion(0,PortionSize,Filter,{Domain,CN,{OrderField,OrderDir},Mask},EtsAcc) of
        {ok,Items} when length(Items) < PortionSize -> ok;
        {ok,Items} when length(Items) >= PortionSize ->
            [LastItem|_] = lists:reverse(Items),
            LastX = maps:get(OrderField,LastItem),
            read_next_portion(LastX,{Domain,CN,{OrderField,OrderDir},Mask,PortionSize},EtsAcc)
    end.

%% ---------------------------------------
%% Request to read from DMS directly without checking hc
%% ---------------------------------------
-spec read_direct(Domain::binary() | atom(), ClassName::binary(), ReadOptions::map()) ->
    {ok,Result::[map()] | map()} | {error,Reason::term()}.
%% ---------------------------------------
read_direct(Domain,ClassName,ReadOptions) ->
    do_read_direct(Domain,ClassName,ReadOptions,1,undefined).

%% @private
do_read_direct(_Domain,_ClassName,_ReadOptions,0,LastResult) -> build_error(LastResult);
do_read_direct(Domain,ClassName,ReadOptions,Attempts,_) when is_map(ReadOptions) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    Args = {'read',#{class => ClassName,
                     object => object(ReadOptions),
                     qs => append_option(ReadOptions,'no_max_limit',true)}},
    case catch ?GLOBAL:gen_server_call(GlobalName,{crud,ClassName,Args}) of
        {'EXIT',Reason} -> do_read_direct(Domain,ClassName,ReadOptions,Attempts-1,Reason);
        {error,_}=Err -> do_read_direct(Domain,ClassName,ReadOptions,Attempts-1,Err);
        {ok,Result} -> {ok,Result}
    end.

%% ---------------------------------------
%% Request to read from DMS
%%    using hashcode
%%    without local caching
%% ---------------------------------------
-spec read_nocache(Domain::binary() | atom(), ClassName::binary(), ReadOptions::map(), HC::term()) ->
    {ok,[Item::map()],HC::term()} | {ok,not_changed} | {error,Reason::term()}.
%% ---------------------------------------
read_nocache(Domain,ClassName,ReadOptions,HC) ->
    do_read_nocache(Domain,ClassName,ReadOptions,HC,1,undefined).

%% @private
do_read_nocache(_Domain,_ClassName,_ReadOptions,_HC,0,LastResult) -> build_error(LastResult);
do_read_nocache(Domain,ClassName,ReadOptions,HC,Attempts,_LastError) when is_map(ReadOptions) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName,{get_hc,ClassName}) of
        {'EXIT',Reason} -> do_read_nocache(Domain,ClassName,ReadOptions,HC,Attempts-1,Reason);
        {error,_}=Err -> do_read_nocache(Domain,ClassName,ReadOptions,HC,Attempts-1,Err);
        {ok,HC} -> {ok,not_changed};
        {ok,HC1} ->
            Args = {'read',#{class => ClassName,
                             object => object(ReadOptions),
                             qs => append_option(ReadOptions,'no_max_limit',true)}},
            case catch ?GLOBAL:gen_server_call(GlobalName,{crud,ClassName,Args}) of
                {'EXIT',Reason} -> do_read_nocache(Domain,ClassName,ReadOptions,HC,Attempts-1,build_error(Reason));
                {error,_}=Err -> do_read_nocache(Domain,ClassName,ReadOptions,HC,Attempts-1,Err);
                {ok,Result} -> {ok,Result,HC1}
            end end.

%% ---------------------------------------
%% Request to read from DMS
%%    using hashcode
%%    using local cache
%% ---------------------------------------
-spec read_cache(Domain::binary() | atom(), ClassName::binary(), ReadOptions::map(), HC::term()) ->
    {ok,[Item::map()],HC::term()} | {ok,not_changed} | {error,Reason::term()}.
%% ---------------------------------------
read_cache(Domain,ClassName,ReadOptions,HC) ->
    do_read_cache(Domain,ClassName,ReadOptions,HC,1,undefined).

%% @private
do_read_cache(_Domain,_ClassName,_ReadOptions,_HC,0,LastResult) -> build_error(LastResult);
do_read_cache(Domain,ClassName,ReadOptions,HC,Attempts,_) ->
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    case catch ?GLOBAL:gen_server_call(GlobalName,{get_hc,ClassName}) of
        {'EXIT',Reason} -> do_read_cache(Domain,ClassName,ReadOptions,HC,Attempts-1,Reason);
        {error,_}=Err -> do_read_cache(Domain,ClassName,ReadOptions,HC,Attempts-1,Err);
        {ok,HC} -> {ok,not_changed};
        {ok,HC1} ->
            Key = {read_collection,{Domain,ClassName,ReadOptions}},
            case ?BLstore:find_t(Key) of
                {_,{HC1,{error,_}=Err}} -> Err;
                {_,{HC1,Result}} when is_list(Result); is_map(Result) ->
                    ?BLstore:store_t(Key,{HC1,Result},300000),
                    {ok,Result,HC1};
                _ ->
                    Args = {'read',#{class => ClassName,
                                     object => object(ReadOptions),
                                     qs => append_option(ReadOptions,'no_max_limit',true)}},
                    case catch ?GLOBAL:gen_server_call(GlobalName,{crud,ClassName,Args}) of
                        {'EXIT',Reason} -> do_read_cache(Domain,ClassName,ReadOptions,HC,Attempts-1,Reason);
                        {error,_}=Err ->
                            ?BLstore:store_t(Key,{HC1,Err},10000),
                            Err;
                        {ok,Result} ->
                            ?BLstore:store_t(Key,{HC1,Result},300000),
                            {ok,Result,HC1}
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
object(ReadOptions) ->
    case maps:get(object,ReadOptions,undefined) of
        undefined -> 'collection';
        Object -> Object
    end.

%% @private
append_option(Opts,Key,Value) when is_list(Opts) -> [{Key,Value}|Opts];
append_option(Opts,Key,Value) when is_map(Opts) -> Opts#{Key => Value}.


%% @private
build_error({error,_}=Err) -> Err;
build_error({noproc,_}=Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,noproc};
build_error({timeout,_}=Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,timeout};
build_error({Other,_}=Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,Other};
build_error(Reason) ->
    ?LOG('$error',"Calling DMS returned ~120tp",[Reason]),
    {error,unknown_response}.