%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc DMS domain fixturer srv. Helps to ensure controlled classes in DMS collection and entities of other collections
%%%     Opts::
%%%         callback_module :: atom(),
%%%         domain :: binary() | atom()
%%%     Callback module should export:
%%%         - fixtures (Domain) -> [{CN::binary(),[map()],/*FunFilterMap*/},{CN::binary(),[map()],FunFilterMap}], when FunFilterMap only for non classes

-module(platformlib_dms_fixturer_domain_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-define(TimeoutFailure, 10000).
-define(TimeoutSuccess, 300000 + ?BU:random(20000)).
-define(TimeoutOnNotify, 3000 + ?BU:random(5000)).

-record(state, {
    callback_module :: atom(),
    domain :: binary() | atom(),
    sync_ref :: reference(),
    started_ts :: non_neg_integer(),
    loaded_ts :: non_neg_integer(),
    failure_timeout :: non_neg_integer(),
    ref :: reference(),
    status :: 'initial' | 'actual',
    timerref :: reference(),
    subscrid :: binary(),
    fixtures :: [map()],
    hc = -1 :: integer()
}).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link(?MODULE, Opts, []).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    CallbackModule = ?BU:get_by_key('callback_module',Opts),
    App = CallbackModule:app(),
    Domain = ?BU:get_by_key('domain',Opts),
    {Self, SyncRef} = {self(), make_ref()},
    Ref = make_ref(),
    State1 = #state{callback_module = CallbackModule,
                    domain = Domain,
                    sync_ref = SyncRef,
                    started_ts = ?BU:timestamp(),
                    failure_timeout = 2,
                    fixtures = CallbackModule:fixtures(Domain),
                    subscrid = SubscrId=?BU:strbin("sid_FIXTURER:~p;dom:~ts",[CallbackModule,Domain]),
                    status = 'initial',
                    timerref = erlang:send_after(0, self(), {timer,'initial',Ref}),
                    ref=Ref},
    % ------
    SubscrOpts = #{fun_report => fun(Report) -> Self ! {'subscriber_report',SyncRef,Report} end},
    ?DMS_SUBSCR:subscribe_async(?APP,Domain,?ClassesCN,self(),SubscrId,SubscrOpts),
    % ------
    CallbackModule:log("~ts. '~ts' Fixturer srv inited", [App, Domain]),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {reply, undefined, State}.

%% ------------------------------
%% Cast
%% ------------------------------

handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% on timer expired. Mode:
%%   'initial' - before dms ensured for controlled classes
%%   'actual' - after dms ensured successfully
handle_info({timer,_Mode,Ref}, #state{ref=Ref,started_ts=StartedTS,failure_timeout=FailureTimeout}=State) ->
    NowTS = ?BU:timestamp(),
    Ref1 = make_ref(),
    State2 = case ensure_fixtures(State) of
                 {ok,State1} ->
                     State1#state{loaded_ts = ?BU:timestamp(),
                                  timerref = erlang:send_after(?TimeoutSuccess, self(), {timer,'actual',Ref1})};
                 {error,_} ->
                     IterationTimeout = FailureTimeout * 2,
                     TimestampTimeout = NowTS - StartedTS,
                     Timeout = erlang:min(?TimeoutFailure,erlang:max(4,erlang:min(IterationTimeout,TimestampTimeout))),
                     State#state{failure_timeout = Timeout,
                                 timerref = erlang:send_after(Timeout, self(), {timer,'initial',Ref1})}
             end,
    {noreply,State2#state{ref=Ref1}};

%% --------------
%% subscribe helper report (state changed)
handle_info({'subscriber_report',SyncRef,Report}, #state{sync_ref=SyncRef,timerref=TimerRef,loaded_ts=LoadedTS}=State) ->
    State1 = case Report of
                 ok ->
                     Timeout = case ?BU:timestamp() of
                                   _ when LoadedTS==undefined -> 0;
                                   NowTS when NowTS - LoadedTS < 5000 -> 5000 - (NowTS - LoadedTS);
                                   _ -> 0
                               end,
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#state{ref=Ref1,
                                 timerref = erlang:send_after(Timeout, self(), {timer,init,Ref1})};
                 _ -> State
             end,
    {noreply, State1};

%% when something changed in 'classes' collection on dms
handle_info({notify,SubscrId,EventInfo}, #state{subscrid=SubscrId,timerref=TimerRef}=State) ->
    Data = maps:get(<<"data">>,EventInfo),
    ClassName = maps:get(<<"classname">>,Data),
    Operation = maps:get(<<"operation">>,Data),
    State1 = case {ClassName,Operation} of
                 {?ClassesCN,O} when O=='update';O=='delete';O=='clear';O=='corrupt';O=='reload' ->
                     ?BU:cancel_timer(TimerRef),
                     Ref1 = make_ref(),
                     State#state{ref=Ref1,
                                 timerref = erlang:send_after(?TimeoutOnNotify, self(), {timer,'initial',Ref1})};
                 _ -> State
             end,
    {noreply,State1};

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% TODO: fixtures of all classes, not only <<"classes">>. fun_filter in
ensure_fixtures(#state{callback_module=CallbackModule,domain=Domain}=State) ->
    Fixtures = CallbackModule:fixtures(Domain),
    case lists:keytake(?ClassesCN,1,Fixtures) of
        false ->
            setup_fixtures(Fixtures,State),
            {ok,State};
        {value,{_,ClassesFixtures},Rest} ->
            setup_fixtures(Rest,State),
            setup_classes(ClassesFixtures,State);
        {value,{_,ClassesFixtures,_},Rest} ->
            setup_fixtures(Rest,State),
            setup_classes(ClassesFixtures,State)
    end.

%% @private
setup_classes(ClassesFixtures,#state{domain=Domain,hc=HC}=State) ->
    ReadOpts = #{},
    case ?DMS_CACHE_SYNC:read_cache_sync(Domain,?ClassesCN,ReadOpts,HC,5000) of
        {error,_}=Err -> Err;
        {ok,not_changed} -> {ok,State};
        {ok,ClassesItems,HC1} ->
            case ?DMS_FIXTURE_HELPER:setup_classes(Domain,ClassesFixtures,ClassesItems) of
                {ok,_} -> {ok,State#state{hc=HC1}};
                {error,_}=Err -> Err
            end end.

%% @private
setup_fixtures(Fixtures,#state{domain=Domain}) ->
    lists:foldl(fun({CN,FixtureItems,FunFilterMap}, ok) ->
                        ?DMS_FIXTURE_HELPER:setup_fixtures(Domain,CN,FixtureItems,FunFilterMap);
                   ({CN,FixtureItems,FunFilterMap,FunShrinkFixtureCmp}, ok) ->
                       ?DMS_FIXTURE_HELPER:setup_fixtures(Domain,CN,FixtureItems,FunFilterMap,FunShrinkFixtureCmp);
                   (_,Acc) -> Acc
                end, ok, Fixtures).