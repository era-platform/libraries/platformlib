%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc General fixturer supervisor.
%%%      Could be added directly or by leader.
%%%      Args:
%%%         CallbackModule
%%%             app() -> ok.
%%%             fixtures (Domain) -> [{CN::binary(),[map()],/*FunFilterMap*/}], when FunFilterMap only for non classes
%%%             log(Fmt,Args) -> ok.

-module(platformlib_dms_fixturer_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1]).
-export([init/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------
%% OTP start_link, when regname is automatically generated
%% ---------------------------------------
-spec start_link(CallbackModule::atom()) -> {ok,Pid::pid()} | ignore | {error,Reason::term()}.
%% ---------------------------------------
start_link(CallbackModule) ->
    supervisor:start_link(?MODULE, #{callback_module => CallbackModule}).

%% ====================================================================
%% Callback functions (supervisor)
%% ====================================================================

init(Opts) ->
    CallbackModule = ?BU:get_by_key(callback_module,Opts),
    App = CallbackModule:app(),
    DTreeOpts = #{stored_callback_module => CallbackModule,
                  resync_timeout => 300000},
    ChildSpec = [{?DTREE_SUPV, {?DTREE_SUPV, start_link, [?DomainTreeCallback,DTreeOpts]}, permanent, 1000, supervisor, [?DTREE_SUPV]}],
    CallbackModule:log("~ts. Fixturer supervisor inited", [App]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.
