%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Supervisor of leader-election service for dms fixturer
%%%    Use it when need automatic multi-domain active-passive fixturer usage
%%%    Start params:
%%%        CallbackModule
%%%        RegName
%%%    CallbackModule should export:
%%%      - app() -> ok
%%%      - get_nodes/0
%%%      - log/2 (Fmt,Args)
%%%      - fixtures (Domain) -> [{CN::binary(),[map()],/*FunFilterMap*/}], when FunFilterMap only for non classes
%%%    Optional app module callbacks:
%%%      - before_start/0
%%%      - after_start/0
%%%      - before_stop/0
%%%      - after_stop/0

-module(platformlib_dms_fixturer_leader_supv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(supervisor).

-export([start_link/1, start_link/2,
         reg_name_supv/1,
         reg_name_srv/1]).
-export([init/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(SupvKey, fixturer_supv).

%% ====================================================================
%% API functions
%% ====================================================================

%% ---------------------------------------
%% OTP start_link, when regname is automatically generated
%% ---------------------------------------
-spec start_link(CallbackModule::atom()) -> {ok,Pid::pid()} | ignore | {error,Reason::term()}.
%% ---------------------------------------
start_link(CallbackModule) ->
    start_link(CallbackModule,reg_name_srv(CallbackModule)).

%% ---------------------------------------
%% OTP start_link, when regname is passed
%% ---------------------------------------
-spec start_link(CallbackModule::atom(),RegName::atom()) -> {ok,Pid::pid()} | ignore | {error,Reason::term()}.
%% ---------------------------------------
start_link(CallbackModule,RegName) when is_atom(CallbackModule), is_atom(RegName) ->
    case check_exports(CallbackModule) of
        ok ->
            Opts = #{callback_module => CallbackModule,
                     reg_name => RegName},
            SupvName = reg_name_supv(CallbackModule),
            supervisor:start_link({local,SupvName}, ?MODULE, Opts);
        {error,_}=Err -> Err
    end.

%% @private
reg_name_supv(Module) ->
    ?BU:to_atom_new(?BU:strbin("~ts_leader_supv",[Module])).

%% @private
reg_name_srv(Module) ->
    ?BU:to_atom_new(?BU:strbin("~ts_leader_srv",[Module])).

%% ====================================================================
%% Callback functions (supervisor)
%% ====================================================================

init(Opts) ->
    CallbackModule = maps:get(callback_module,Opts),
    RegName = maps:get(reg_name,Opts),
    FunStart = fun(_RemoteState) -> start_fixturer_supv(CallbackModule,RegName) end,
    FunStop = fun() -> stop_fixturer_supv(CallbackModule,RegName) end,
    StartArgs = [{regname, RegName},
                 {fun_nodes, fun CallbackModule:get_nodes/0},
                 {fun_start, FunStart},
                 {fun_stop, FunStop},
                 {ping_timeout, 60000},
                 {heartbeat_timeout, 20000},
                 {autosort, true},
                 {waittimes, 2},
                 {monitor, true},
                 {dbl_restart_master, true},
                 {heartbeat, true},
                 {takeover, false},
                 {fun_takeover_state, undefined},
                 {fun_out, fun CallbackModule:log/2}],
    ChildSpec = [{RegName, {?LeaderSrv, start_link, [StartArgs]}, permanent, 1000, worker, [?LeaderSrv]}],
    CallbackModule:log("LEADER '~ts'. Fixturer supervisor inited (~p)", [RegName, self()]),
    {ok, {{one_for_one, 10, 2}, ChildSpec}}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
check_exports(CallbackModule) ->
    Fs = [{get_nodes,0},
          {log,2},
          {app,0},
          {fixtures,1}],
    Res = lists:foldl(fun({Fname,Arity}=Finfo,true) ->
                            case ?BU:function_exported(CallbackModule,Fname,Arity) of
                                true -> true;
                                false -> {false,Finfo}
                            end;
                         (_,Acc) -> Acc
                      end, true, Fs),
    case Res of
        true -> ok;
        {false,Finfo} ->
            {Fname,Arity} = Finfo,
            {error, ?BU:str("Expected fixturer leader callback module should export '~ts'/~p",[Fname,Arity])}
    end.

%% @private
start_fixturer_supv(CallbackModule,RegName) ->
    CallbackModule:log("LEADER '~ts'. Starting fixturer master...", [RegName]),
    Fok = fun() ->
                CallbackModule:log("LEADER '~ts'. Fixturer master started", [RegName]),
                do_after_start(CallbackModule),
                ok
          end,
    ChildSpec = {?SupvKey, {?DMS_FIXTURER_SUPV,start_link,[CallbackModule]}, permanent, 1000, supervisor, [?DMS_FIXTURER_SUPV]},
    SupvName = reg_name_supv(CallbackModule),
    do_before_start(CallbackModule),
    case catch supervisor:start_child(SupvName, ChildSpec) of
        {'EXIT',Err} ->
            CallbackModule:log("LEADER '~ts'. Fixturer master start exception: ~120tp. Stop node..", [RegName, Err]),
            ?BU:stop_node(1000);
        {error,_}=Err ->
            CallbackModule:log("LEADER '~ts'. Fixturer master start error: ~120tp, Stop node..", [RegName, Err]),
            ?BU:stop_node(1000);
        {ok,_} -> Fok();
        {ok,_,_} -> Fok()
    end.

% @private
stop_fixturer_supv(CallbackModule,RegName) ->
    SupvName = reg_name_supv(CallbackModule),
    do_before_stop(CallbackModule),
    catch supervisor:terminate_child(SupvName,?SupvKey),
    catch supervisor:delete_child(SupvName,?SupvKey),
    do_after_stop(CallbackModule),
    CallbackModule:log("LEADER '~ts'. Fixturer master stopped", [RegName]).

%% ----------------------------

% callback before start
do_before_start(CallbackModule) ->
    call(CallbackModule,before_start,[]).

% callback after start
do_after_start(CallbackModule) ->
    call(CallbackModule,after_start,[]).

% callback before stop
do_before_stop(CallbackModule) ->
    call(CallbackModule,before_stop,[]).

% callback after start
do_after_stop(CallbackModule) ->
    call(CallbackModule,after_stop,[]).

%% ------------------------
%% @private
%% callback on some
call(CallbackModule,Fun,Args) ->
    case ?BU:function_exported(CallbackModule,Fun,length(Args)) of
        true -> erlang:apply(CallbackModule,Fun,Args);
        false -> ok
    end.
