%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.06.2021
%%% @doc DomainTree callback module for fixturer

-module(platformlib_dms_fixturer_domaintree_callback).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([app/1,
         log/3,
         domain_child_spec/2,
         filter_domain/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

app(_Opts) -> 'fixturer'.

log(Opts,Fmt,Args) ->
    CallbackModule = maps:get(stored_callback_module,Opts),
    App = CallbackModule:app(),
    CallbackModule:log(?BU:str("~ts. ~ts",[App,Fmt]),Args).

domain_child_spec(Opts,Domain) ->
    CallbackModule = maps:get(stored_callback_module,Opts),
    StartArgs = [Opts#{callback_module => CallbackModule, domain => Domain}],
    #{start => {?DMS_FIXTURER_DSRV,start_link,StartArgs},
      restart => permanent,
      shutdown => 1000,
      type => worker,
      modules => [?DMS_FIXTURER_DSRV]}.

filter_domain(_Opts,_Domain,_DomainItem) -> true.


%% ====================================================================
%% Internal functions
%% ====================================================================