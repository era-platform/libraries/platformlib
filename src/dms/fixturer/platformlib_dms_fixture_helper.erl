%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 28.05.2021
%%% @doc Classes fixture setup helper

-module(platformlib_dms_fixture_helper).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([setup_classes/2, setup_classes/3]).
-export([setup_fixtures/4, setup_fixtures/5]).
-export([ensure_fixtures/4]).
-export([worth_result/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------------------------------------------
%% CLASSES COLLECTION
%% -----------------------------------------------------------------

%% --------------------------------------
%% Ensure fixtures of controlled classes (classes, storages), return worth result
%% --------------------------------------
-spec setup_classes(Domain::binary() | atom(), FixtureItems::[map()]) ->
    {ok,done | already_done} | {error,replace_failed}.
%% --------------------------------------
setup_classes(Domain,FixtureItems) ->
    case ?DMS_CACHE_SYNC:read_cache_sync(Domain,?ClassesCN,#{},auto,5000) of
        {ok,LoadedItems,_} -> setup_classes(Domain,FixtureItems,LoadedItems);
        _ -> {error,read_failed}
    end.

%% --------------------------------------
%% Ensure fixtures of controlled classes (classes, storages), return worth result
%% --------------------------------------
-spec setup_classes(Domain::binary() | atom(), FixtureItems::[map()], LoadedItems::[map()]) ->
    {ok,done | already_done} | {error,replace_failed}.
%% --------------------------------------
setup_classes(Domain,FixtureItems,LoadedItems) ->
    {Ids,CNs} = lists:unzip(lists:map(fun(Item) -> {maps:get(<<"id">>,Item),maps:get(<<"classname">>,Item)} end, FixtureItems)),
    FunFilter = fun(Item) -> lists:member(maps:get(<<"id">>,Item),Ids) orelse lists:member(maps:get(<<"classname">>,Item),CNs) end,
    LoadedItems1 = lists:filter(FunFilter,LoadedItems),
    LoadedItems2 = shrink_fields(LoadedItems1,FixtureItems),
    LoadedItems3 = shrink_opts(LoadedItems2,FixtureItems),
    LoadedItems4 = shrink_properties(LoadedItems3,FixtureItems),
    ensure_fixtures(Domain,?ClassesCN,FixtureItems,LoadedItems4).

%% @private
%% Remove added fields in loaded items.
%%   If field is added by ext process or by validator - it's not a problem. Generally avoid change of props, set by fixture.
shrink_fields(LoadedItems,Fixtures) ->
    lists:map(fun(Item) ->
                    Id = maps:get(<<"id">>,Item),
                    case lists:filter(fun(FixtItem) -> maps:get(<<"id">>,FixtItem)==Id end,Fixtures) of
                        [] -> Item;
                        [FixtItem] ->
                            maps:with(maps:keys(FixtItem), Item)
                    end end,LoadedItems).

%% @private
%% Remove added opts in loaded items.
%%   If opt is added by ext process or by validator - it's not a problem. Generally avoid change of props, set by fixture.
shrink_opts(LoadedItems,Fixtures) ->
    lists:map(fun(Item) ->
                    Id = maps:get(<<"id">>,Item),
                    case lists:filter(fun(FixtItem) -> maps:get(<<"id">>,FixtItem)==Id end,Fixtures) of
                        [] -> Item;
                        [FixtItem] ->
                            ItemOpts = maps:get(<<"opts">>,Item),
                            FixtOpts = maps:get(<<"opts">>,FixtItem),
                            Item#{<<"opts">> => maps:with(maps:keys(FixtOpts),ItemOpts)}
                    end end,LoadedItems).

%% @private
%% Remove added props in loaded items.
%%   If prop is added by ext process or by validator - it's not a problem. Generally avoid change of props, set by fixture.
shrink_properties(LoadedItems,Fixtures) ->
    lists:map(fun(Item) ->
                    Id = maps:get(<<"id">>,Item),
                    case lists:filter(fun(FixtItem) -> maps:get(<<"id">>,FixtItem)==Id end,Fixtures) of
                        [] -> Item;
                        [FixtItem] ->
                            ItemProps = maps:get(<<"properties">>,Item),
                            FixtProps = maps:get(<<"properties">>,FixtItem),
                            FixtPropNames = lists:map(fun(FixtProp) -> maps:get(<<"name">>,FixtProp) end, FixtProps),
                            FunFilter = fun(Prop) -> lists:member(maps:get(<<"name">>,Prop),FixtPropNames) end,
                            ShrinkedProps = lists:filter(fun(Prop) -> FunFilter(Prop) end, ItemProps),
                            Item#{<<"properties">> => ShrinkedProps}
                    end end,LoadedItems).

%% -----------------------------------------------------------------
%% ANY COLLECTION
%% -----------------------------------------------------------------

%% --------------------------------------
%% Ensure fixtures of controlled classes (classes, storages), return worth result
%% --------------------------------------
-spec setup_fixtures(Domain::binary() | atom(), ClassName::binary(), FixtureItems::[map()], FunFilterMap::function())
      -> {ok,done | already_done} | {error,replace_failed}.
%% --------------------------------------
setup_fixtures(Domain,ClassName,FixtureItems,FunFilterMap) when is_function(FunFilterMap,1) ->
    setup_fixtures(Domain,ClassName,FixtureItems,FunFilterMap,undefined).

%% --------------------------------------
%% Ensure fixtures of controlled classes (classes, storages), return worth result
%% --------------------------------------
-spec setup_fixtures(Domain::binary() | atom(), ClassName::binary(), FixtureItems::[map()],
                     FunFilterMap::function(),FunShrinkFixtureCmp::function() | undefined)
      -> {ok,done | already_done} | {error,replace_failed}.
%% --------------------------------------
setup_fixtures(Domain,ClassName,FixtureItems,FunFilterMap,FunShrinkFixtureCmp) when is_function(FunFilterMap,1) ->
    case ?DMS_CACHE_SYNC:read_cache_sync(Domain,ClassName,#{},auto,5000) of
        {ok,LoadedItems,_} ->
            LoadedItems1 = lists:filtermap(FunFilterMap, LoadedItems),
            ensure_fixtures(Domain,ClassName,FixtureItems,FunShrinkFixtureCmp,LoadedItems1);
        _ -> {error,read_failed}
    end.

%% --------------------------------------
%% Fixture sync helper
%%    Checks if loaded items list contains fixtures of the same type. Replaces if not.
%%    LoadedItems should be already filtered and mapped to direct juxtapose, that implement direct comparing and deletion.
%%       So LoadedItems should contain only items, that intersects with fixtures for subject.
%% --------------------------------------
-spec ensure_fixtures(Domain::binary() | atom(),ClassName::binary(),Fixtures::[map()],LoadedItems::[map()])
      -> {ok,done | already_done} | {error,replace_failed}.
%% --------------------------------------
ensure_fixtures(Domain,ClassName,Fixtures,LoadedItems) ->
    ensure_fixtures(Domain,ClassName,Fixtures,undefined,LoadedItems).

%% --------------------------------------
-spec ensure_fixtures(Domain::binary() | atom(),ClassName::binary(),Fixtures::[map()],FunShrinkFixtureCmp::function() | undefined,LoadedItems::[map()])
      -> {ok,done | already_done} | {error,replace_failed}.
%% --------------------------------------
ensure_fixtures(Domain,ClassName,Fixtures,FunShrinkFixtureCmp,LoadedItems) ->
    {ComparedFixtures,OriginalFixtures} =
        case FunShrinkFixtureCmp of
            undefined -> {Fixtures,[]};
            Fun -> {lists:map(Fun, Fixtures), Fixtures}
        end,
    case ?BU:juxtapose_ex(LoadedItems,ComparedFixtures,fun(Item) -> maps:get(<<"id">>,Item) end) of
        {ok,[],[],_Unchanged,[]} -> {ok,already_done};
        {ok,Created,Deleted,_Unchanged,ModifiedEx} ->
            ?LOG('$trace',"Fixture helper '~ts' found job in '~ts':~n\tCreated: ~120tp~n\tDeleted: ~120tp~n\tModified: ~120tp~n\tFixtures: ~120tp~n\tFound: ~120tp",[ClassName,Domain,Created,Deleted,ModifiedEx,Fixtures,LoadedItems]),
            Res1 = delete(Domain,ClassName,Deleted),
            Res2 = replace(Domain,ClassName,restore_fixtures(lists:map(fun({_Old,New}) -> New end,ModifiedEx),OriginalFixtures)),
            Res3 = create(Domain,ClassName,restore_fixtures(Created,OriginalFixtures)),
            worth_result([Res1,Res2,Res3])
    end.

%% @private
restore_fixtures(Shrinked,[]) -> Shrinked;
restore_fixtures(Shrinked,Original) ->
    Map = lists:foldl(fun(Item,Acc) -> Acc#{maps:get(<<"id">>,Item) => Item} end, #{}, Original),
    lists:map(fun(Item) -> maps:get(maps:get(<<"id">>,Item),Map) end, Shrinked).

%% @private
delete(_,_,[]) -> {ok,already_done};
delete(Domain,ClassName,Items) ->
    ?LOG('$info',"Removing invalid fixtures in collection '~ts' in domain '~ts'",[ClassName,Domain]),
    F = fun(Item,Acc) ->
            Id = maps:get(<<"id">>,Item),
            Args = {'delete',#{class => ClassName,
                               object => {'entity',Id,[]},
                               initiator => {'fixturer',ClassName},
                               qs => [] }},
            GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
            case catch ?GLOBAL:gen_server_call(GlobalName,{crud,ClassName,Args}) of
                {ok,_} -> true andalso Acc;
                {'EXIT',Reason} ->
                    ?LOG('$crash',"Fixture helper call to '~ts' ('delete', cn='~ts', id='~120tp') crash: ~n\t~120tp",[?MsvcDms,ClassName,Id,Reason]),
                    false;
                T ->
                    ?LOG('$error',"Fixture helper call to '~ts' ('delete', cn='~ts', id='~120tp') error: ~n\t~120tp",[?MsvcDms,ClassName,Id,T]),
                    false
            end end,
    case lists:foldl(F, true, Items) of
        true -> {ok,done};
        false -> {error,replace_failed}
    end.

%% @private
replace(_,_,[]) -> {ok,already_done};
replace(Domain,ClassName,Items) ->
    ?LOG('$info',"Replacing invalid fixtures in collection '~ts' in domain '~ts'",[ClassName,Domain]),
    do_replace(Domain,ClassName,Items).

%% @private
create(_,_,[]) -> {ok,already_done};
create(Domain,ClassName,Items) ->
    ?LOG('$info',"Creating fixtures in collection '~ts' in domain '~ts'",[ClassName,Domain]),
    do_replace(Domain,ClassName,Items).

%% @private
do_replace(Domain,ClassName,Items) ->
    F = fun(Item,Acc) ->
            Id = maps:get(<<"id">>,Item),
            Args = {'replace',#{class => ClassName,
                                object => {'entity',Id,[]},
                                content => Item,
                                initiator => {'fixturer',ClassName},
                                qs => [] }},
            GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
            case catch ?GLOBAL:gen_server_call(GlobalName,{crud,ClassName,Args}) of
                {ok,Result} ->
                    ?LOG('$trace',"Replacing fixture '~ts' '~ts' result: ~n\t~120tp",[Domain,ClassName,Result]),
                    true andalso Acc;
                {'EXIT',Reason} ->
                    ?LOG('$crash',"Fixture helper call to '~ts' ('replace', cn='~ts', id='~120tp') crash: ~n\t~120tp",[?MsvcDms,ClassName,Id,Reason]),
                    false;
                T ->
                    ?LOG('$error',"Fixture helper call to '~ts' ('replace', cn='~ts', id='~120tp') error: ~n\t~120tp",[?MsvcDms,ClassName,Id,T]),
                    false
            end end,
    case lists:foldl(F, true, Items) of
        true -> {ok,done};
        false -> {error,replace_failed}
    end.

%% ====================================================================
%% Routines functions
%% ====================================================================

worth_result(Results) when is_list(Results), length(Results) > 0 ->
    [{_,WorthRes}|_] = lists:sort(fun(A,B) -> A=<B end, [{srtidx(Res),Res} || Res <- Results]),
    WorthRes.

%% @private
srtidx({ok,already_done}) -> 4;
srtidx({ok,done}) -> 3;
srtidx({error,replace_failed}) -> 2;
srtidx({error,read_failed}) -> 1;
srtidx(_) -> 0.
