%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.06.2021
%%% @doc Helper for crud requests to dms or other data appservers

-module(platformlib_dms_crud).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([crud/2,
         crud_cast/3]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(Timeout, 30000).

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------
%% Apply crud operation to dms or other appserver of class.
%% Map0 should contain
%%    'object' :: collection | {entity,Id::binary(),Opts::[tuple()]} | {path,PathParts::[binary()]}
%%    'qs' :: list() | map()
%%    'initiator' :: {user,UserId::binary()}
%%    'content' :: map() | list() | binary() % json body, required only on operations: create | replace | update | delete | lookup
%% Result of crud operation is passed to FunReply and then return back.
%% ------------------------------------------
-spec crud({Domain::binary(),
            Operation::read|create|replace|update|delete|clear|lookup,
            ClassItem::map(),CN::binary(),
            Map0::map(),
            Opts::list()},
           FunReply::function())
      -> Result::term() | {error,{Code::integer()|atom(),Reason::binary()}}.
%% ------------------------------------------
crud({Domain,Operation,ClassItem,CN,Map0,_Opts},FunReply) ->
    Map = Map0#{'domain' => Domain,
                'classname' => CN,
                'src' => src()},
    CrudReq = {crud,CN,{Operation,Map}},
    AppServer = case is_map(ClassItem) of
                    true -> maps:get(<<"appserver">>,maps:get(<<"opts">>,ClassItem,#{}),<<>>);
                    _ -> <<>>
                end,
    case AppServer of
        <<>> ->
            call_dms({Domain,CrudReq},FunReply);
        SvcName ->
            GlobalName1 = SvcName,
            case ?GN_REG:whereis_name(GlobalName1) of
                Pid when is_pid(Pid) -> call_global({GlobalName1,CrudReq},FunReply);
                undefined ->
                    GlobalName2 = ?GN_NAMING:get_globalname(SvcName,Domain),
                    case ?GN_REG:whereis_name(GlobalName2) of
                        Pid when is_pid(Pid) -> call_global({GlobalName2,CrudReq},FunReply);
                        undefined -> FunReply({error,{503,<<"App server not registered">>}})
                    end end end.

%% ------------------------------
crud_cast({Domain,Operation,ClassItem,CN,Map0,_Opts}, {FromPid,Ref}, FunReply) ->
    Map = Map0#{'domain' => Domain,
                'classname' => CN,
                'src' => src()},
    CrudReq = {crud,CN,{Operation,Map}},
    AppServer = case is_map(ClassItem) of
                    true -> maps:get(<<"appserver">>,maps:get(<<"opts">>,ClassItem,#{}),<<>>);
                    _ -> <<>>
                end,
    CastMsg = {'$call',{FromPid,Ref},CrudReq},
    case AppServer of
        <<>> ->
            cast_dms({Domain,CastMsg},FunReply);
        SvcName ->
            GlobalName1 = SvcName,
            case ?GN_REG:whereis_name(GlobalName1) of
                Pid when is_pid(Pid) -> cast_global({GlobalName1,CastMsg},FunReply);
                undefined ->
                    GlobalName2 = ?GN_NAMING:get_globalname(SvcName,Domain),
                    case ?GN_REG:whereis_name(GlobalName2) of
                        Pid when is_pid(Pid) -> cast_global({GlobalName2,CastMsg},FunReply);
                        undefined -> FunReply({error,{503,<<"App server not registered">>}})
                    end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% @private
src() -> {?PCFG:get_current_site(),node(),self()}.

%% @private
call_dms({Domain,CrudReq},FunReply) ->
    % TODO ICP: get group
    % TODO ICP: check dms prior
    % TODO ICP: sync response, async wait
    Timeout = 30000,
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    Reply = (catch ?GLOBAL:gen_server_call(GlobalName, CrudReq, Timeout)),
    FunReply(Reply).

%% @private
cast_dms({Domain,CastMsg},FunReply) ->
    % TODO ICP: get group
    % TODO ICP: check dms prior
    % TODO ICP: sync response, async wait
    GlobalName = ?GN_NAMING:get_globalname(?MsvcDms,Domain),
    Reply = (catch ?GLOBAL:gen_server_cast(GlobalName, CastMsg)),
    FunReply(Reply).

%% @private
call_global({GlobalName,CrudReq},FunReply) ->
    Reply = case ?GN_REG:whereis_name(GlobalName) of
                undefined ->
                    {error,{internal_error,<<"Appserver is down">>}};
                Pid when is_pid(Pid) ->

                    Ref = make_ref(),
                    Msg = {'$call', {self(),Ref}, CrudReq},
                    MonRef = erlang:monitor(process,Pid),
                    Pid ! Msg,
                    receive
                        {'$reply',Ref,Result} ->
                            erlang:demonitor(MonRef),
                            Result;
                        {'DOWN',MonRef,process,Pid,_} ->
                            {error,{internal_error,<<"Appserver is down">>}}
                    after ?Timeout ->
                        {error,{internal_error,<<"Appserver timeout">>}}
                    end end,
    FunReply(Reply).

%% @private
cast_global({GlobalName,CastMsg},FunReply) ->
    case ?GN_REG:whereis_name(GlobalName) of
        undefined ->
            Reply = {error,{internal_error,<<"Appserver is down">>}},
            FunReply(Reply);
        Pid when is_pid(Pid) ->
            % TODO: may be monitor Pid too
            Pid ! CastMsg,
            FunReply(ok)
    end.