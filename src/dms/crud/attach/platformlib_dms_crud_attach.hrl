%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 16.06.2021
%%% @doc

%% -------------------------------------
-define(TypeS3, <<"s3">>).
-define(TypeNFS, <<"nfs">>).
-define(TypeFS, <<"fs">>).
-define(TypeFSync, <<"fsync">>).

-define(TempFileTTL, 300). % in seconds

%% -------------------------------------
-define(AttachUtils, platformlib_dms_crud_attach_utils).
-define(AttachS3, platformlib_dms_crud_attach_storage_s3).
-define(AttachNFS, platformlib_dms_crud_attach_storage_nfs).
-define(AttachFS, platformlib_dms_crud_attach_storage_fs).
-define(AttachFSync, platformlib_dms_crud_attach_storage_fsync).