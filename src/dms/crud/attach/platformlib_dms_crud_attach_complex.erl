%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.06.2021
%%% @doc Complex operations with attachments and also update/use entity

-module(platformlib_dms_crud_attach_complex).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    download_complex/5,
    upload_complex/6,
    upload_multi_complex/6,
    delete_complex/5,
    clear_complex/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------
%% Download file and return {ok,PathToFile,Entity}
%% Opts::
%%    filename :: binary() | string() % Used when multi attachment property to select file.
%%    prepath :: binary() % used for transactionlog or history storage type of class.
%%    initiator :: {Type::atom(),Id::binary()} | undefined % used for audit log of read operations on dms
%% ------------------------------------------
-spec download_complex(Domain::binary() | atom(), ClassItem::map(), Id::binary(), Property::binary(), Opts::map()) ->
        {ok,PathToFile::binary(),Entity::map()}.
%% ------------------------------------------
download_complex(Domain,ClassItem,Id,Property,Opts) ->
    FunReply = fun({ok,PathToFile},Entity) -> {ok,PathToFile,Entity};
                  ({error,_}=Err,_) -> Err
               end,
    FunMulti = fun(Entity) ->
                    StoreFilename = maps:get('filename',Opts,?BU:to_list(?BU:newid())),
                    case lists:filter(fun(FileInfo) -> maps:get(<<"name">>,FileInfo)==StoreFilename end, maps:get(Property,Entity,[])) of
                        [CheckFileInfo] -> FunReply(?DMS_ATTACH:download(Domain,ClassItem,Id,Property,StoreFilename,CheckFileInfo),Entity);
                        [] -> {error,{not_found,<<"File not found (P2)">>}}
                    end end,
    FunSingle = fun(Entity) ->
                    StoreFilename = <<"content">>,
                    CheckFileInfo = case maps:get(Property,Entity,null) of
                                        Value when is_map(Value) -> Value;
                                        _ -> #{}
                                    end,
                    FunReply(?DMS_ATTACH:download(Domain,ClassItem,Id,Property,StoreFilename,CheckFileInfo),Entity)
                end,
    read_entity(Domain,ClassItem,Id,Property,Opts,FunSingle,FunMulti).

%% ------------------------------------------
%% Upload file to storage and update property of class entity.
%% Opts::
%%    filename :: binary() | string() % Used when multi attachment property to select file.
%%    prepath :: binary() % used for transactionlog or history storage type of class.
%%    initiator :: {Type::atom(),Id::binary()} | undefined % used for audit log of read operations on dms
%% ------------------------------------------
-spec upload_complex(Domain::binary() | atom(), ClassItem::map(), Id::binary(), Property::binary(), Opts::map(), FromFilePath::binary() | string()) ->
        {ok,FileInfo::map(),Entity::map()}.
%% ------------------------------------------
upload_complex(Domain,ClassItem,Id,Property,Opts,FromFilepath) ->
    FunGetFileInfo = fun(UserFilename,InStoragePath) ->
                            {ok,FileInfo0} = ?BU:file_info_hasha(FromFilepath),
                            FileInfo1 = maps:fold(fun(K,V,Acc) -> Acc#{?BU:to_binary(K) => V} end, #{}, FileInfo0),
                            FileInfo1#{<<"name">> => UserFilename,
                                       <<"link">> => InStoragePath}
                     end,
    FunUpdate = fun(FileInfo,Content) ->
                        case do_update_entity(Domain,ClassItem,Id,Opts,Content) of
                            {ok,Entity} -> {ok,FileInfo,Entity};
                            {error,_}=Err -> Err
                        end end,
    FunMulti = fun(Entity) ->
                        StoreFilename = maps:get('filename',Opts,filename:basename(FromFilepath)),
                        UserFilename = StoreFilename,
                        case ?DMS_ATTACH:upload(Domain,ClassItem,Id,Property,FromFilepath,StoreFilename) of
                            {ok,InStoragePath} ->
                                FileInfo = FunGetFileInfo(UserFilename,InStoragePath),
                                Attaches0 = maps:get(Property,Entity,[]),
                                Attaches1 = lists:filter(fun(FInfo) -> maps:get(<<"name">>,FInfo)/=StoreFilename end, Attaches0),
                                Attaches2 = lists:sort(fun(A,B) -> maps:get(<<"name">>,A) =< maps:get(<<"name">>,B) end, [FileInfo|Attaches1]),
                                FunUpdate(FileInfo,#{Property => {internal,Attaches2}});
                            {error,_}=Err -> Err
                        end end,
    FunSingle = fun(_Entity) ->
                        StoreFilename = <<"content">>,
                        UserFilename = filename:basename(FromFilepath),
                        case ?DMS_ATTACH:upload(Domain,ClassItem,Id,Property,FromFilepath,StoreFilename) of
                            {ok,InStoragePath} ->
                                FileInfo = FunGetFileInfo(UserFilename,InStoragePath),
                                FunUpdate(FileInfo,#{Property => {internal,FileInfo}});
                            {error,_}=Err -> Err
                        end end,
    read_entity(Domain,ClassItem,Id,Property,Opts,FunSingle,FunMulti).

%% ------------------------------------------
%% Upload multiple files to storage and update property of class entity. Uses their own filenames.
%% Opts::
%%    prepath :: binary() % used for transactionlog or history storage type of class.
%%    initiator :: {Type::atom(),Id::binary()} | undefined % used for audit log of read operations on dms
%% ------------------------------------------
-spec upload_multi_complex(Domain::binary() | atom(), ClassItem::map(), Id::binary(), Property::binary(), Opts::map(), FromFilepaths::[binary() | string()]) ->
        {ok,FileInfos::[map()],Entity::map()}.
%% ------------------------------------------
upload_multi_complex(Domain,ClassItem,Id,Property,Opts,FromFilepaths) ->
    FunGetFileInfo = fun(Filepath,UserFilename) ->
                            {ok,FileInfo0} = ?BU:file_info_hasha(Filepath),
                            FileInfo1 = maps:fold(fun(K,V,Acc) -> Acc#{?BU:to_binary(K) => V} end, #{}, FileInfo0),
                            FileInfo1#{<<"name">> => UserFilename}
                     end,
    FunUpdate = fun(FileInfos,Content) ->
                        case do_update_entity(Domain,ClassItem,Id,Opts,Content) of
                            {ok,Entity} -> {ok,FileInfos,Entity};
                            {error,_}=Err -> Err
                        end end,
    FunMulti = fun(Entity) ->
                        Ffilename = fun(FilePath) -> ?BU:to_binary(filename:basename(?BU:to_list(FilePath))) end,
                        FileNames = lists:map(fun(FilePath) -> Ffilename(FilePath) end, FromFilepaths),
                        FileInfos = lists:map(fun(FilePath) -> FunGetFileInfo(FilePath,Ffilename(FilePath)) end, FromFilepaths),
                        UploadResult = lists:foldl(fun({FromFilepath,FileInfo},{ok,AccInfos}) ->
                                                            AsFilename = filename:basename(FromFilepath),
                                                            case ?DMS_ATTACH:upload(Domain,ClassItem,Id,Property,FromFilepath,AsFilename) of
                                                                {ok,InStorageLnk} -> {ok,[FileInfo#{<<"link">> => InStorageLnk}|AccInfos]};
                                                                {error,_}=Err -> Err
                                                            end;
                                                      (_,Acc) -> Acc
                                                   end, {ok,[]}, lists:zip(FromFilepaths,FileInfos)),
                        case UploadResult of
                            {error,_}=Err -> Err;
                            {ok,FileInfos1} ->
                                Attaches0 = maps:get(Property,Entity,[]),
                                Attaches1 = lists:filter(fun(FInfo) -> not lists:member(maps:get(<<"name">>,FInfo),FileNames) end, Attaches0),
                                Attaches2 = lists:sort(fun(A,B) -> maps:get(<<"name">>,A) =< maps:get(<<"name">>,B) end, FileInfos1 ++ Attaches1),
                                FunUpdate(FileInfos,#{Property => {internal,Attaches2}})
                        end end,
    FunSingle = fun(_Entity) -> {error,{invalid_operation,<<"Cannot add multiple files to non-collection property">>}} end,
    read_entity(Domain,ClassItem,Id,Property,Opts,FunSingle,FunMulti).

%% ------------------------------------------
%% Delete file from storage and update property of class entity
%% Opts::
%%    filename :: binary() | string() % Used when multi attachment property to select file.
%%    prepath :: binary() % used for transactionlog or history storage type of class.
%%    initiator :: {Type::atom(),Id::binary()} | undefined % used for audit log of read operations on dms
%% ------------------------------------------
-spec delete_complex(Domain::binary() | atom(), ClassItem::map(), Id::binary(), Property::binary(), Opts::map()) ->
        {ok,Entity::map()}.
%% ------------------------------------------
delete_complex(Domain,ClassItem,Id,Property,Opts) ->
    FunUpdate = fun(Content) ->
                        case do_update_entity(Domain,ClassItem,Id,Opts,Content) of
                            {ok,Entity} -> {ok,Entity};
                            {error,_}=Err -> Err
                        end end,
    FunMulti = fun(Entity) ->
                        StoreFilename = maps:get('filename',Opts,?BU:to_list(?BU:newid())),
                        case ?DMS_ATTACH:delete(Domain,ClassItem,Id,Property,StoreFilename) of
                            ok ->
                                Attaches0 = maps:get(Property,Entity,[]),
                                Attaches1 = lists:filter(fun(FInfo) -> maps:get(<<"name">>,FInfo)/=StoreFilename end, Attaches0),
                                FunUpdate(#{Property => {internal,Attaches1}});
                            {error,_}=Err -> Err
                        end end,
    FunSingle = fun(_Entity) ->
                        StoreFilename = <<"content">>,
                        case ?DMS_ATTACH:delete(Domain,ClassItem,Id,Property,StoreFilename) of
                            ok -> FunUpdate(#{Property => {internal,null}});
                            {error,_}=Err -> Err
                        end end,
    read_entity(Domain,ClassItem,Id,Property,Opts,FunSingle,FunMulti).

%% ------------------------------------------
%% Clear all files from storage path (collection property) and update property of class entity
%% Opts::
%%    prepath :: binary() % used for transactionlog or history storage type of class.
%%    initiator :: {Type::atom(),Id::binary()} | undefined % used for audit log of read operations on dms
%% ------------------------------------------
-spec clear_complex(Domain::binary() | atom(), ClassItem::map(), Id::binary(), Property::binary(), Opts::map()) ->
    {ok,Entity::map()}.
%% ------------------------------------------
clear_complex(Domain,ClassItem,Id,Property,Opts) ->
    FunUpdate = fun(Content) ->
                    case do_update_entity(Domain,ClassItem,Id,Opts,Content) of
                        {ok,Entity} -> {ok,Entity};
                        {error,_}=Err -> Err
                    end end,
    FunMulti = fun(Entity) ->
                    Attaches0 = maps:get(Property,Entity,[]),
                    Res = lists:foldl(fun(FileInfo, Acc) ->
                                            Filename = maps:get(<<"name">>,FileInfo),
                                            case ?DMS_ATTACH:delete(Domain,ClassItem,Id,Property,Filename) of
                                                ok -> [Filename|Acc];
                                                {error,_} -> Acc
                                            end
                                      end, [], Attaches0),
                    Attaches1 = case length(Res)==length(Attaches0) of
                                    true -> [];
                                    false -> lists:filter(fun(FInfo) -> not lists:member(maps:get(<<"name">>,FInfo), Res) end, Attaches0)
                    end,
                    FunUpdate(#{Property => {internal,Attaches1}})
               end,
    FunSingle = fun(_Entity) -> {error,{405,<<"CLEAR is not available on non-collection property">>}} end,
    read_entity(Domain,ClassItem,Id,Property,Opts,FunSingle,FunMulti).

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -----------------------------------
%% Return entity from dms (or other appserver)
%% -----------------------------------
read_entity(Domain,ClassItem,Id,Property,Opts,FunSingle,FunMulti) ->
    case do_read_entity(Domain,ClassItem,Id,Opts) of
        {ok,Entity} ->
            case lists:filter(fun(Prop) -> maps:get(<<"name">>,Prop)==Property end, maps:get(<<"properties">>,ClassItem)) of
                [Prop] ->
                    Multi = maps:get(<<"multi">>,Prop,false),
                    case maps:get(<<"data_type">>,Prop,undefined) of
                        <<"attachment">> when Multi -> FunMulti(Entity);
                        <<"attachment">> -> FunSingle(Entity);
                        _ -> {error,{invalid_params,<<"Expected property of type 'attachment'">>}}
                    end;
                [] -> {error,{invalid_params,<<"Expected existing property of type 'attachment'">>}}
            end;
        {error,_}=Err -> Err
    end.

%% @private
do_read_entity(Domain,ClassItem,Id,Opts) when is_map(Opts) ->
    CN = maps:get(<<"classname">>,ClassItem),
    FunReply = fun(Reply) -> Reply end,
    Map0 = build_crud_map(Id,Opts),
    case catch ?DMS_CRUD:crud({Domain,'read',ClassItem,CN,Map0,[]}, FunReply) of
        {'EXIT',Reason} -> {error,Reason};
        {error,_}=Err -> Err;
        {ok,Entity} -> {ok,Entity}
    end.

%% @private
do_update_entity(Domain,ClassItem,Id,Opts,Content) when is_map(Opts), is_map(Content) ->
    CN = maps:get(<<"classname">>,ClassItem),
    FunReply = fun(Reply) -> Reply end,
    Map0 = (build_crud_map(Id,Opts))#{content => Content},
    case catch ?DMS_CRUD:crud({Domain,'update',ClassItem,CN,Map0,[]}, FunReply) of
        {'EXIT',Reason} -> {error,Reason};
        {error,_}=Err -> Err;
        {ok,Entity} -> {ok,Entity}
    end.

%% @private
build_crud_map(Id,Opts) ->
    Map0 = #{'qs' => [],
             'object' => case maps:get(prepath,Opts,undefined) of
                             undefined -> {'entity',Id,[]};
                             Prepath -> {'entity',Id,[{path,Prepath}]}
                         end},
    case maps:get('initiator',Opts,undefined) of
        {_,_}=Initiator -> Map0#{'initiator' => Initiator};
        _ -> Map0
    end.