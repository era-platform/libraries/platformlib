%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.06.2021
%%% @doc Utilities for work with attachments

-module(platformlib_dms_crud_attach_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    build_attachment_path/5, build_attachment_path/4,
    node_temp_path/0,
    get_server_path/1,
    get_storages/2,
    get_prefix/1
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("platformlib_dms_crud_attach.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------
%% get call record relative path
%% -----------------------------
build_attachment_path(Domain,ClassItem,Id,Property,Filename) ->
    filename:join([
        build_attachment_path(Domain,ClassItem,Id,Property),
        ?BU:to_unicode_list(Filename)]).

build_attachment_path(Domain,ClassItem,Id,Property) ->
    filename:join([
        ?BU:to_unicode_list(Domain),
        ?BU:to_unicode_list(maps:get(<<"classname">>,ClassItem)),
        ?BU:to_unicode_list(Id),
        ?BU:to_unicode_list(Property)]).

%% -----------------------------
%% return temp path for node
%% -----------------------------
node_temp_path() ->
    filename:join([
        ?BU:to_unicode_list(get_server_path(<<"temp">>)),
        ?BU:to_unicode_list(node())]).

%% -----------------------------
%% return path to server's configured dir
%% TODO: optimize by caching or direct function
%% -----------------------------
get_server_path(ConfigKey) ->
    SrvInfo = ?PCFG:get_server_info(),
    SrvParams = maps:get(<<"params">>,SrvInfo),
    ServerPaths = maps:get(<<"paths">>,SrvParams),
    case maps:get(ConfigKey,ServerPaths,undefined) of
        undefined -> throw({error,{internal_error,?BU:strbin("Invalid configuration. Server path not found: '~ts'",[ConfigKey])}});
        <<"alias://",_/binary>>=Alias ->
            case ?PCFG:get_alias_value(Alias) of
                {ok,Value} -> ?BU:to_unicode_list(Value);
                {error,_} -> throw({error,{internal_error,?BU:strbin("Invalid configuration. Alias not found: '~ts'",[Alias])}})
            end;
        Value -> ?BU:to_list(Value)
    end.

%% ----------------------------------------
%% Return file storages for class
%% ----------------------------------------
-spec get_storages(Domain::binary()|atom(), ClassItem::map()) -> {ok,Type::binary(),[StorageItem::map()]}.
%% ----------------------------------------
get_storages(Domain,ClassItem) ->
    Opts = maps:get(<<"opts">>,ClassItem),
    case maps:get(<<"filestorage_instance">>,Opts,<<>>) of
        Instance when is_binary(Instance), Instance /= <<>> ->
            ReadOpts = #{<<"filter">> => [<<"&&">>,[<<"==">>,[<<"property">>,<<"instance">>],Instance],[<<"in">>,[<<"property">>,<<"type">>],[<<"list">>,?TypeS3,?TypeNFS,?TypeFS,?TypeFSync]]]},
            case ?DMS_CACHE:read_cache(Domain,?StoragesCN,ReadOpts,auto) of
                {ok,[_|_]=Storages0,_} ->
                    Storages = [Storage#{'x_domain' => Domain} || Storage <- Storages0],
                    Res = lists:foldl(fun(Storage,{S3,NFS,FS,FSync}) ->
                                            case maps:get(<<"type">>,Storage) of
                                                ?TypeS3 -> {[Storage|S3],NFS,FS,FSync};
                                                ?TypeNFS -> {S3,[Storage|NFS],FS,FSync};
                                                ?TypeFS -> {S3,NFS,[Storage|FS],FSync};
                                                ?TypeFSync -> {S3,NFS,FS,[Storage|FSync]}
                                            end end, {[],[],[],[]}, Storages),
                    case Res of
                        {[],[],[],FSyncs} -> {ok,?TypeFSync,FSyncs};
                        {[],NFSs,[],_} -> {ok,?TypeNFS,NFSs};
                        {[],_,FSs,_} -> {ok,?TypeFS,FSs};
                        {S3s,_,_,_} -> {ok,?TypeS3,S3s}
                    end;
                {ok,[],_} -> {error,{invalid_params,<<"filestorage_instance is not found">>}};
                _ -> {error,{internal_error,<<"storages read error">>}}
            end;
        _ -> {error,{invalid_params,<<"filestorage_instance is not defined">>}}
    end.

%% ----------------------------------------
%% Return actual prefix. Default if not set
%% ----------------------------------------
get_prefix(Prefix) when is_binary(Prefix) ->
    case string:trim(Prefix) of
        <<>> -> get_default_prefix();
        Trimmed -> Trimmed
    end;
get_prefix(_) -> get_default_prefix().

%% @private
get_default_prefix() -> "class_attachments".

%% ====================================================================
%% Internal functions
%% ====================================================================