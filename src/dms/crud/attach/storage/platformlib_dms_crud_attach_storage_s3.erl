%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.06.2021
%%% @doc Implementation of file storage type S3 (http request to S3 service, AWS or other)

-module(platformlib_dms_crud_attach_storage_s3).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([
    download/8,
    upload/7,
    delete/6
]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("../platformlib_dms_crud_attach.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% -----------------------------
%% Download file from S3 storage
%% -----------------------------
download(Domain,ClassItem,Id,Property,Filename,ToFilepath,CheckFileInfo,Storages) ->
    StorePath = ?AttachUtils:build_attachment_path(Domain,ClassItem,Id,Property,Filename),
    case ?STORAGE_S3:download(Storages,StorePath,ToFilepath,CheckFileInfo) of
        {ok,_}=Ok ->
            ?FILE_AUTODELETE_SRV:register_filepath(ToFilepath,?TempFileTTL),
            Ok;
        {error,_}=Err -> Err
    end.

%% -----------------------------
%% Upload file to S3 storage
%% -----------------------------
upload(Domain,ClassItem,Id,Property,FromFilepath,AsFilename,Storages) ->
    StorePath = ?AttachUtils:build_attachment_path(Domain,ClassItem,Id,Property,AsFilename),
    ?STORAGE_S3:upload(Storages,StorePath,FromFilepath).

%% -----------------------------
%% Delete file from S3 storage
%% -----------------------------
delete(Domain,ClassItem,Id,Property,Filename,Storages) ->
    StorePath = ?AttachUtils:build_attachment_path(Domain,ClassItem,Id,Property,Filename),
    ?STORAGE_S3:delete(Storages,StorePath).