%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.06.2021
%%% @doc GenServer for autodeletion of temp files, downloaded from file storage by requests

-module(platformlib_dms_crud_attach_autodelete_srv).
-author('Peter Bukashin <tbotc@yandex.ru>').

-behaviour(gen_server).

-export([start_link/1]).
-export([init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2, code_change/3]).

-export([register_filepath/2]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

-record(reg,{
    filepath :: string(),
    ts :: non_neg_integer(),
    ttl :: non_neg_integer(),
    ref :: reference(),
    timerref :: reference()
}).

-record(state, {
    seed_directory :: string(),
    syncref :: reference(),
    regs = #{}:: map(),
    ref :: reference(),
    timerref :: reference()
}).

-define(SeedTimeout, 3600000 + ?BU:random(300000)).

%% ====================================================================
%% API functions
%% ====================================================================

start_link(Opts) ->
    gen_server:start_link({local,?MODULE}, ?MODULE, Opts, []).

%% ------------------------------
%% register file for auto deletion, ttl in seconds
%% ------------------------------
register_filepath(Filepath,TTL) ->
    gen_server:cast(?MODULE,{register,?BU:to_list(Filepath),TTL}).

%% ====================================================================
%% Callback functions
%% ====================================================================

%% ------------------------------
%% Init
%% ------------------------------
init(Opts) ->
    SyncRef = make_ref(),
    Ref = make_ref(),
    State1 = #state{seed_directory=?BU:to_list(maps:get('seed_directory',Opts,"")),
                    syncref=SyncRef,
                    ref=Ref,
                    timerref=erlang:send_after(?SeedTimeout, self(), {timer_seed,SyncRef,Ref})},
    ?LOG('$info', "~ts. CRUD temp files autodelete srv inited", [?APP]),
    {ok, State1}.

%% ------------------------------
%% Call
%% ------------------------------

handle_call(_Request, _From, State) ->
    {noreply, State}.

%% ------------------------------
%% Cast
%% ------------------------------

%% register filepath to autodelete, also enlarges ttl
handle_cast({register,Filepath,TTL},#state{syncref=SyncRef,regs=Regs}=State) ->
    State1 = case maps:get(Filepath,Regs,undefined) of
                 #reg{ts=TS0,ttl=TTL0,timerref=TimerRef}=Reg ->
                     NowTS = ?BU:timestamp(),
                     Timeout0 = TTL0*1000 - (NowTS-TS0),
                     case TTL*1000 of
                         Timeout when Timeout > Timeout0 ->
                             ?BU:cancel_timer(TimerRef),
                             RefF = make_ref(),
                             Reg1 = Reg#reg{ts=NowTS,
                                            ttl=TTL,
                                            ref=RefF,
                                            timerref=erlang:send_after(Timeout, self(), {timer_expire,SyncRef,RefF,Filepath})},
                             State#state{regs=Regs#{Filepath => Reg1}};
                         _ -> State
                     end;
                 undefined ->
                     RefF = make_ref(),
                     Reg1 = #reg{filepath=Filepath,
                                 ts=?BU:timestamp(),
                                 ttl=TTL,
                                 ref=RefF,
                                 timerref=erlang:send_after(TTL*1000, self(), {timer_expire,SyncRef,RefF,Filepath})},
                     State#state{regs=Regs#{Filepath => Reg1}}
             end,
    {noreply, State1};

%% other
handle_cast(_Request, State) ->
    {noreply, State}.

%% ------------------------------
%% Info
%% ------------------------------

%% on file expiration
handle_info({timer_expire,SyncRef,RefF,Filepath}, #state{syncref=SyncRef,regs=Regs}=State) ->
    State1 = case maps:get(Filepath,Regs,undefined) of
                 #reg{ref=RefF} ->
                     delete_file(Filepath),
                     State#state{regs=maps:remove(Filepath,Regs)};
                 _ -> State
             end,
    {noreply,State1};

%% general seed empty directories timer
handle_info({timer_seed,SyncRef,Ref}, #state{syncref=SyncRef,ref=Ref}=State) ->
    State1 = seed_empty_directories(State),
    {noreply,State1};

%% other
handle_info(_Info, State) ->
    {noreply, State}.

%% ------------------------------
%% Terminate
%% ------------------------------
terminate(_Reason, _State) ->
    ok.

%% ------------------------------
%% Code change
%% ------------------------------
code_change(_OldVsn, State, _Extra) ->
    {ok, State}.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% -------------------
delete_file(Filepath) ->
    try file:delete(Filepath)
    catch E:R:ST -> ?LOG('$warning',"File '~ts' ttl autodelete failed: {~120tp, ~120tp},~n\tStack: ~120tp",[Filepath,E,R,ST])
    end.

%% -------------------
seed_empty_directories(#state{seed_directory=Dir}=State) ->
    try ?BU:directory_delete_empties(Dir)
    catch E:R:ST -> ?LOG('$warning',"Seed directory failed: {~120tp, ~120tp},~n\tStack: ~120tp",[E,R,ST])
    end,
    State.