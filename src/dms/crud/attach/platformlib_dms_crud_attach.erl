%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 17.06.2021
%%% @doc Common routines to work with attachments of dms classes

-module(platformlib_dms_crud_attach).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([get_temp_path/4, get_temp_path/5]).

-export([
    download/6, download/5,
    upload/6,
    delete/5]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").
-include("platformlib_dms_crud_attach.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

%% ------------------------------------------
%% Build and return local temp file name for attachment of class entity
%% ------------------------------------------
get_temp_path(Domain,ClassItem,Id,Property,Filename) ->
    filename:join([
        ?AttachUtils:node_temp_path(),
        "class_attachments",
        ?AttachUtils:build_attachment_path(Domain,ClassItem,Id,Property,Filename)]).

%% ------------------------------------------
%% Build and return local temp directory of class property for attachments
%% ------------------------------------------
get_temp_path(Domain,ClassItem,Id,Property) ->
    filename:join([
        ?AttachUtils:node_temp_path(),
        "class_attachments",
        ?AttachUtils:build_attachment_path(Domain,ClassItem,Id,Property)]).

%% ====================================================================
%% Only storage operation functions
%% ====================================================================

%% ------------------------------------------
%% Download file from file storage of class to temp directory and return absolute path to file on local disk.
%% Specific optimizations:
%%   Temp file path is unique and static. So if file is found there and has the same attributes as registered in class entity, then returns path to it without download.
%%   If file storage of class is located on local mounted drive, then returns absolute path without download.
%% Specific of auto deletion of temp files:
%%   Temp directory is cleared automatically on restart of container or machine.
%%   However downloaded file is available only for 5 minutes after last usage, then it would be automatically deleted.
%%   If several processes request for same file, then they enlarge expire time for this file.
%% ------------------------------------------
-spec download(Domain::binary() | atom(),ClassItem::map(),Id::binary(),Property::binary(),Filename::binary()|string(),CheckFileInfo::map()) ->
    {ok,ToFilepath::binary()} | {error,Reason::term()}.
%% ------------------------------------------
download(Domain,ClassItem,Id,Property,Filename,CheckFileInfo) when not is_map(CheckFileInfo) ->
    download(Domain,ClassItem,Id,Property,Filename);
download(Domain,ClassItem,Id,Property,Filename,CheckFileInfo) ->
    ToFilepath = get_temp_path(Domain,ClassItem,Id,Property,Filename),
    case ?BU:file_info_hasha(ToFilepath) of
        {error,_} -> download(Domain,ClassItem,Id,Property,Filename);
        {ok,Info} ->
            HashA = maps:get(<<"hasha">>,CheckFileInfo,undefined),
            case maps:get('hasha',Info) of
                HashA ->
                    ?FILE_AUTODELETE_SRV:register_filepath(ToFilepath,?TempFileTTL),
                    {ok,?BU:to_binary(ToFilepath)};
                _ ->
                    ToFilepath = get_temp_path(Domain,ClassItem,Id,Property,Filename),
                    case ?AttachUtils:get_storages(Domain,ClassItem) of
                        {error,_}=Err -> Err;
                        {ok,?TypeS3,Storages} -> ?AttachS3:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,CheckFileInfo,Storages);
                        {ok,?TypeNFS,Storages} -> ?AttachNFS:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,CheckFileInfo,Storages);
                        {ok,?TypeFS,Storages} -> ?AttachFS:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,CheckFileInfo,Storages);
                        {ok,?TypeFSync,Storages} -> ?AttachFSync:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,CheckFileInfo,Storages)
                    end end end.

%% download to temp forcely
download(Domain,ClassItem,Id,Property,Filename) ->
    ToFilepath = get_temp_path(Domain,ClassItem,Id,Property,Filename),
    case ?AttachUtils:get_storages(Domain,ClassItem) of
        {error,_}=Err -> Err;
        {ok,?TypeS3,Storages} -> ?AttachS3:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,undefined,Storages);
        {ok,?TypeNFS,Storages} -> ?AttachNFS:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,undefined,Storages);
        {ok,?TypeFS,Storages} -> ?AttachFS:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,undefined,Storages);
        {ok,?TypeFSync,Storages} -> ?AttachFSync:download(Domain,ClassItem,Id,Property,Filename,ToFilepath,undefined,Storages)
    end.

%% ------------------------------------------
%% Upload file to file storage of class from some temp directory
%% Returns in-storage link to file
%% ------------------------------------------
-spec upload(Domain::binary() | atom(),ClassItem::map(),Id::binary(),Property::binary(),FromFilepath::binary()|string(),AsFilename::binary()|string()) ->
    {ok,InStoragePath::string()} | {error,Reason::term()}.
%% ------------------------------------------
upload(Domain,ClassItem,Id,Property,FromFilepath,AsFilename) ->
    case ?BLfilelib:is_regular(FromFilepath) of
        false -> {error,{invalid_operation,<<"File not found (P1)">>}};
        true ->
            case ?AttachUtils:get_storages(Domain,ClassItem) of
                {error,_}=Err -> Err;
                {ok,?TypeS3,Storages} -> ?AttachS3:upload(Domain,ClassItem,Id,Property,FromFilepath,AsFilename,Storages);
                {ok,?TypeNFS,Storages} -> ?AttachNFS:upload(Domain,ClassItem,Id,Property,FromFilepath,AsFilename,Storages);
                {ok,?TypeFS,Storages} -> ?AttachFS:upload(Domain,ClassItem,Id,Property,FromFilepath,AsFilename,Storages);
                {ok,?TypeFSync,Storages} -> ?AttachFSync:upload(Domain,ClassItem,Id,Property,FromFilepath,AsFilename,Storages)
            end end.

%% ------------------------------------------
%% Upload file to file storage of class from some temp directory
%% ------------------------------------------
-spec delete(Domain::binary() | atom(),ClassItem::map(),Id::binary(),Property::binary(),Filename::binary()|string()) ->
    ok | {error,Reason::term()}.
%% ------------------------------------------
delete(Domain,ClassItem,Id,Property,Filename) ->
    case ?AttachUtils:get_storages(Domain,ClassItem) of
        {error,_}=Err -> Err;
        {ok,?TypeS3,Storages} -> ?AttachS3:delete(Domain,ClassItem,Id,Property,Filename,Storages);
        {ok,?TypeNFS,Storages} -> ?AttachNFS:delete(Domain,ClassItem,Id,Property,Filename,Storages);
        {ok,?TypeFS,Storages} -> ?AttachFS:delete(Domain,ClassItem,Id,Property,Filename,Storages);
        {ok,?TypeFSync,Storages} -> ?AttachFSync:delete(Domain,ClassItem,Id,Property,Filename,Storages)
    end.

%% ====================================================================
%% Internal functions
%% ====================================================================



