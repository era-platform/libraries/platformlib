%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Pavel Abramov, Peter Bukashin <tbotc@yandex.ru>
%%% @doc 01.08.2017 #352
%%% @todo

-module(calltracer_pid).
-author(['Pavel Abramov','Peter Bukashin <tbotc@yandex.ru>']).

-export([start/1, start/2, stop/0]).
-export([loop/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(TU, calltracer_utils).
-define(WriterProcName, ptracer).

-record(state1, {pidwriter}).
-record(state2, {descriptor, pidstatedict, lastpid, statefun}).
-record(stackitem, {curmod, curfun, size}).

% messages
-define(START_WRITE, <<"start_write">>).
-define(STOP_WRITE, <<"stop_write">>).
-define(START_PID, <<"start_write_pid">>).
-define(MSG, <<"new_trace_message">>).

% opts key
-define(PTRACE_MODULES, <<"modules">>).
-define(PTRACE_STATEFUN, <<"statefunctions">>).

%% ====================================================================
%% Public functions
%% ====================================================================

start(Pid) ->
    start(Pid, #{}).

start(Pid,Opts) ->
    Modules = maps:get(?PTRACE_MODULES,Opts,[]),
    Writer = case whereis(?WriterProcName) of
                 undefined ->
                     WriterPid = start_writer_process(Opts),
                     Fun = fun(Msg, #state1{pidwriter=WP}=State1) -> WP ! {?MSG,Msg}, State1 end,
                     dbg:tracer(process,{Fun,#state1{pidwriter=WriterPid}}),
                     case Modules of
                         [] ->  dbg:tpl('_', []);
                         _ -> lists:foreach(fun(M) -> dbg:tpl(M, [{'_',[],[{message,{process_dump}}]}]) end, Modules)
                     end,
                     WriterPid;
                 W -> W
             end,
    dbg:p(Pid, [c, call, return_to, timestamp]),
    Writer ! {?START_PID, Pid}.

stop() ->
    stop_writer_process(),
    dbg:stop_clear(),
    ok.

%% ====================================================================
%% Internal functions
%% ====================================================================

%% --------------------------------------------------------------------
%% @private
start_writer_process(Opts) ->
    FileName = "trace_pid.txt",
    {_FilePath,FDescriptor} = ?TU:ensure_trace_file(FileName),
    StateFuns = maps:get(?PTRACE_STATEFUN, Opts, []),
    State = #state2{descriptor=FDescriptor,statefun=StateFuns,pidstatedict=dict:new()},

    register(?WriterProcName, Pid=spawn_link(?MODULE, loop, [State])),
    Pid ! ?START_WRITE,
    Pid.

%% @private
stop_writer_process() ->
    case whereis(?WriterProcName) of
        Pid when is_pid(Pid) ->
            Pid ! ?STOP_WRITE, ok;
        _ -> ok
    end.

%% ---------------------------------------------------------------------
%% @spawned
loop(#state2{descriptor=F,pidstatedict=Dict}=State) ->
    receive
        {?MSG,{trace_ts,Pid,call,{Mod,Fun,Term},Timestamp}} -> call(Pid,Mod,Fun,Term,localtime_ms(Timestamp),[],State);
        {?MSG,{trace_ts,Pid,call,{Mod,Fun,Term},ProcDump,Timestamp}} -> call(Pid,Mod,Fun,Term,localtime_ms(Timestamp),ProcDump,State);
        {?MSG,{trace,Pid,call,{Mod,Fun,Term}}} -> call(Pid,Mod,Fun,Term,localtime_ms(),[],State);
        {?MSG,{trace_ts,Pid,return_to,{Mod,Fun,_},_}} -> return(Pid,Mod,Fun,State);
        {?MSG,{trace_ts,Pid,return_to,{Mod,Fun,_},_,_}} -> return(Pid,Mod,Fun,State);
        {?MSG,{trace_ts,Pid,return_to,undefined,_}} -> return(Pid,State);
        {?MSG,{trace,Pid,return_to,{Mod,Fun,_}}} -> return(Pid,Mod,Fun,State);

        ?START_WRITE ->
            {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
            io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    trace started~n", [H,M,S,Ms]),
            io:format(F, "---------------------------------------------------------------------~n",[]),
            ?MODULE:loop(State);

        {?START_PID, Pid} ->
            case dict:find(Pid, Dict) of
                error ->
                    {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
                    io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    trace  pid (~p) started~n", [H,M,S,Ms,Pid]);
                _ -> ok
            end,
            ?MODULE:loop(State);

        ?STOP_WRITE ->
            {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
            io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    trace stopped~n", [H,M,S,Ms]),
            io:format(F, "---------------------------------------------------------------------~n",[]),
            file:close(F);

        Term ->
            {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
            io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    unknown message received~n            ~120p~n~n~n",[H,M,S,Ms, Term]),
            ?MODULE:loop(State)
    end.

%% ---------------------------------------------------------------------
%% @private
localtime_ms() ->
    Now = os:timestamp(),
    localtime_ms(Now).
%% @private
localtime_ms(Now) ->
    {_, _, Micro} = Now,
    {Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(Now),
    {Date, {Hours, Minutes, Seconds, Micro div 1000 rem 1000}}.

%% ---------------------------------------------------------------------
%% @private
call(Pid,Mod,Fun,Ar,Time,ProcDump,#state2{descriptor=F,pidstatedict=Dict,lastpid=LastPrintedPid,statefun=StateFuns}=State) ->
    {Print, FirstDump, Stack1} = case dict:find(Pid, Dict) of
                          {ok, {_Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}=Item|Tail]}} ->  {false, <<>>, [Item#stackitem{size=Size+1}|Tail]};
                          {ok, {{Mod,Fun}=_Last, Stack}} when is_list(Stack) -> {false, <<>>, [#stackitem{curmod=Mod, curfun=Fun, size=1}|Stack]};
                          {ok, {_Last, Stack}} when is_list(Stack) -> {true, <<>>, [#stackitem{curmod=Mod, curfun=Fun, size=1}|Stack]};
                          error -> {true, ProcDump, [#stackitem{curmod=Mod, curfun=Fun, size=1}]}
                      end,
    Dict1 = dict:store(Pid, {{Mod,Fun}, Stack1}, Dict),
    case Print of
        false -> ?MODULE:loop(State#state2{pidstatedict=Dict1});
        true ->
            case lists:member(Fun, StateFuns) of
                true -> write_to_file(F,Pid,LastPrintedPid,Mod,Fun,Ar,Time,integer_to_list((length(Stack1)-1)*2),FirstDump);
                false -> write_to_file(F,Pid,LastPrintedPid,Mod,Fun,[],Time,integer_to_list((length(Stack1)-1)*2),FirstDump)
            end,
            ?MODULE:loop(State#state2{pidstatedict=Dict1, lastpid=Pid})
    end.

%% ---------------------------------------------------------------------
%% @private
return(Pid,#state2{pidstatedict=Dict}=State) ->
    Dict1 = case dict:find(Pid, Dict) of
                {ok, {{Mod,Fun}=Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}=Item|Tail]}} when Size > 1 ->
                    dict:store(Pid, {Last,[Item#stackitem{size=Size-1}|Tail]}, Dict);
                {ok, {{Mod,Fun}, [#stackitem{curmod=Mod, curfun=Fun, size=Size}]}} when Size =< 1 -> dict:erase(Pid, Dict);
                {ok, {_Last, [#stackitem{curmod=Mod1, curfun=Fun1, size=Size}|Tail]}} when Size =< 1 -> dict:store(Pid, {{Mod1,Fun1},Tail}, Dict);
                {ok, {_Last, [#stackitem{curmod=Mod1, curfun=Fun1, size=Size}=Item|Tail]}} when Size > 1 ->
                    dict:store(Pid, {{Mod1,Fun1},[Item#stackitem{size=Size-1}|Tail]}, Dict);
                {ok, {_Last, Stack}} when is_list(Stack) ->    Dict;
                error -> Dict
            end,
    ?MODULE:loop(State#state2{pidstatedict=Dict1}).
%% @private
return(Pid,Mod,Fun,#state2{pidstatedict=Dict}=State) ->
    Dict1 = case dict:find(Pid, Dict) of
                {ok, {Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}=Item|Tail]}} when Size > 1 ->
                    dict:store(Pid, {Last,[Item#stackitem{size=Size-1}|Tail]}, Dict);
                {ok, {_Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}]}} when Size =< 1 -> dict:erase(Pid, Dict);
                {ok, {Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}|Tail]}} when Size =< 1 -> dict:store(Pid, {Last,Tail}, Dict);
                {ok, {_Last, Stack}} when is_list(Stack) ->    Dict;
                error -> Dict
            end,
    ?MODULE:loop(State#state2{pidstatedict=Dict1}).

%% ---------------------------------------------------------------------
%% @private
write_to_file(F,Pid,Pid,Mod,Fun,Ar,Time,Shift,_Dump) ->
    write_to_file(F,Pid,Mod,Fun,Ar,Time,Shift);
write_to_file(F,Pid,_,Mod,Fun,Ar,Time,Shift,<<>>) ->
    io:format(F, "---------------------------------------------------------------------~n", []),
    write_to_file(F,Pid,Mod,Fun,Ar,Time,Shift);
write_to_file(F,Pid,_,Mod,Fun,Ar,Time,Shift,Dump) ->
    L = binary:bin_to_list(Dump),
    io:format(F, "---------------------------------------------------------------------~n", []),
    io:format(F, "Process dump:~n~s~n", [L]),
    io:format(F, "---------------------------------------------------------------------~n", []),
    write_to_file(F,Pid,Mod,Fun,Ar,Time,Shift).
write_to_file(F,Pid,Mod,Fun,[],{_,{H,M,S,Ms}},Shift) ->
    io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    ~120p    ~"++ Shift ++ "c~120p:~120p~n", [H,M,S,Ms,Pid,32,Mod,Fun]);
write_to_file(F,Pid,Mod,Fun,Ar,{_,{H,M,S,Ms}},Shift) ->
    io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    ~120p    ~"++ Shift ++ "c~120p:~120p~n   ---Args--->\t~120p~n", [H,M,S,Ms,Pid,32,Mod,Fun,Ar]).



