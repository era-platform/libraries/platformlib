%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc
%%% @todo

-module(calltracer_args).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/1, startglobal/1, startmulti/1, addmulti/1,
         clearfile/0,
         stop/0, stop/1]).
-export([loop/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(TU, calltracer_utils).

%% ====================================================================
%% Public functions
%% ====================================================================

start({TraceModule, Func, Ar}) ->
    startinternal({TraceModule, Func, Ar}, [local], {onemodule});
start({TraceModule, Func}) ->
    startinternal({TraceModule, Func, '_'}, [local], {onemodule});
start({TraceModule}) ->
    startinternal({TraceModule, '_', '_'}, [local], {onemodule});
start(TraceModule) ->
    start({TraceModule}).

startglobal({TraceModule, Func, Ar}) ->
    startinternal({TraceModule, Func, Ar}, [global], {onemodule});
startglobal({TraceModule, Func}) ->
    startinternal({TraceModule, Func, '_'}, [global], {onemodule});
startglobal({TraceModule}) ->
    startinternal({TraceModule, '_', '_'}, [global], {onemodule});
startglobal(TraceModule) ->
    startglobal({TraceModule}).

startmulti({TraceModule, Func, Ar}) ->
    startinternal({TraceModule, Func, Ar}, [local], {multimodule, restart});
startmulti({TraceModule, Func}) ->
    startinternal({TraceModule, Func, '_'}, [local], {multimodule, restart});
startmulti({TraceModule}) ->
    startinternal({TraceModule, '_', '_'}, [local], {multimodule, restart});
startmulti(TraceModule) ->
    startmulti({TraceModule}).

addmulti({TraceModule, Func, Ar}) ->
    startinternal({TraceModule, Func, Ar}, [local], {multimodule, add});
addmulti({TraceModule, Func}) ->
    startinternal({TraceModule, Func, '_'}, [local], {multimodule, add});
addmulti({TraceModule}) ->
    startinternal({TraceModule, '_', '_'}, [local], {multimodule, add});
addmulti(TraceModule) ->
    addmulti({TraceModule}).

%% @private
startinternal({TraceModule, Func, Ar}, Opts, {onemodule}) ->
    prepareProc(TraceModule),
    {ok, whereis(getname()), configTrace({TraceModule, Func, Ar}, Opts)};
startinternal({TraceModule, Func, Ar}, Opts, {multimodule, Mode}) ->
    case Mode of
        add -> ok;
        restart -> stopalltraces()
    end,
    prepareProc(multi),
    {ok,whereis(getname()),configTrace({TraceModule, Func, Ar}, Opts)}.

%% @private
prepareProc(Procfile) ->
    PName = getname(),
    case whereis(PName) of
        undefined ->
            stopinternal(int),
            register(PName, spawn_link(?MODULE, loop, [startinternal_module(Procfile)]));
        _Pid ->
            PName ! {getmodule, self(), Ref=make_ref()},
            receive
                {currentmodule, Procfile, _FileName, Ref} -> ok;
                {currentmodule, _Module, _FileName, Ref} ->
                    stopinternal(int),
                    register(PName, spawn_link(?MODULE, loop, [startinternal_module(Procfile)]))
            after
                1000 ->
                    stopinternal(int),
                    register(PName, spawn_link(?MODULE, loop, [startinternal_module(Procfile)]))
            end
    end.

%% @private
configTrace({TraceModule, Func, Ar}, Opts) ->
    erlang:trace(all, false, [call]),
    erlang:trace(all, false, [all]),
    {
        erlang:trace(all, true, [call, {tracer, whereis(getname())}]),
        erlang:trace_pattern({TraceModule, Func, Ar}, [{'_',[],[{return_trace}]}], Opts)
    }.

%% @private
startinternal_module(TraceModule) ->
    FileName = "trace_args.txt",
    {FilePath,FDescriptor} = ?TU:ensure_trace_file(FileName),
    {FDescriptor,TraceModule,FilePath}.

%% --------------------------------
clearfile() ->
    PName = getname(),
    case whereis(PName) of
        undefined -> not_started;
        _ -> PName ! clearfile, ok
    end.

%% --------------------------------
stop() ->
    stopinternal(ext).
stop({TraceModule, Func, Ar}) ->
    case erlang:module_loaded(TraceModule) of
        false -> module_not_loaded;
        true ->
            erlang:trace_pattern({TraceModule, Func, Ar}, false, [local]),
            erlang:trace_pattern({TraceModule, Func, Ar}, false, [global])
    end;
stop({TraceModule, Func}) ->
    stop({TraceModule, Func, '_'});
stop({TraceModule}) ->
    stop({TraceModule, '_', '_'});
stop(TraceModule) ->
    stop({TraceModule}).

%% @private
stopinternal(Dir) ->
    stopalltraces(),
    PName = getname(),
    case whereis(PName) of
        undefined -> process_not_started;
        _ ->
            case Dir of
                ext -> PName ! stop, ok;
                int ->
                    PName ! {stop, self(), Ref=make_ref()},
                    receive
                        {stopped, Ref} -> ok
                    after
                        1000 -> ok
                    end
            end, ok
    end.

%% @private
stopalltraces() ->
    erlang:trace(all, false, [call]),
    erlang:trace_pattern({'_', '_', '_'}, false, [global]),
    erlang:trace_pattern({'_', '_', '_'}, false, [local]).

%% ====================================================================
%% Internal loop functions
%% ====================================================================

%% --------------------------------
getname() ->
    ?MODULE.

%% --------------------------------
loop({F,CurrentModule,CurrentFile}=State) ->
    receive
        {trace,Pid,call,{Mod,Fun,Term}} ->
            {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
            io:format(F, "~n~2..0B:~2..0B:~2..0B.~3..0B    (~p)    ----> ~p:~p~n            ~120p ~n~n", [H,M,S,Ms, Pid,Mod,Fun,Term]),
            ?MODULE:loop(State);
        {trace,Pid,return_from,{Mod,Fun,Ar},Term} ->
            {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
            io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    (~p)       <- ~p:~p/~p returned~n            ~120p ~n~n", [H,M,S,Ms, Pid,Mod,Fun,Ar,Term]),
            ?MODULE:loop(State);

        {getmodule,PidFrom,Ref} ->
            PidFrom ! {currentmodule, CurrentModule, CurrentFile, Ref},
            ?MODULE:loop(State);

        clearfile ->
            file:close(F),
            {ok, F1} = file:open(CurrentFile, write),
            ?MODULE:loop({F1,CurrentModule,CurrentFile});

        stop ->
            stoploop(State);
        {stop, PidFrom, Ref} ->
            stoploop(State),
            unregister(getname()),
            PidFrom ! {stopped, Ref};

        Term ->
            {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
            io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    unknown message received~n            ~120p~n~n~n",[H,M,S,Ms, Term]),
            ?MODULE:loop(State)
    end.

%% @private
stoploop({F,_CurModule,_CurFile}) ->
    {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
    io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    trace stopped~n", [H,M,S,Ms]),
    file:close(F).

%% @private
localtime_ms() ->
    Now = os:timestamp(),
    localtime_ms(Now).

%% @private
localtime_ms(Now) ->
    {_, _, Micro} = Now,
    {Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(Now),
    {Date, {Hours, Minutes, Seconds, Micro div 1000 rem 1000}}.