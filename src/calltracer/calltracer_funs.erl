%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2020 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @doc

-module(calltracer_funs).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([start/1,
         start_sp/2,
         start_nonsystem/0,
         startfilter/1,
         startfilters/1,
         addmulti/1,
         clearfile/0,
         stop/0, stop/1]).

-export([loop1/1,
         loop2/1,
         getstack/0]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

-define(TU, calltracer_utils).

-record(state1, {pidwriter}).
-record(state2, {descriptor, currentmodule, currentfile, pidstatedict, lastpid}).
-record(stackitem, {curmod, curfun, size}).
% state's state is dict() by Pid
% Pid's value in dict() is list() of stack calls
% stack call's item is #stackitem{}

%% ====================================================================
%% Public functions
%% ====================================================================

getstack() ->
    PName2 = getname2(),
    case whereis(PName2) of
        undefined ->
            not_started;
        _Pid ->
            PName2 ! {getstack, self(), Ref=make_ref()},
            receive
                {stack, Stack, Ref} -> Stack
            after
                1000 -> error
            end
    end.

%% ---------------------------
start(F) when is_function(F,1) ->
    add(code:all_loaded(), F).

%% ---------------------------
start_sp(F,Timeout) when is_function(F,1) ->
    spawn(fun() -> start_sp_1(F,Timeout) end).
%% @private
start_sp_1(F,Timeout) ->
    start(F),
    timer:sleep(Timeout).

%% ---------------------------
start_nonsystem() ->
    RootDir = string:to_lower(code:root_dir()),
    F = fun(_Module, Path) when is_list(Path) ->
                case lists:prefix(RootDir, lists:map(fun($\\)->$/;(A)->A end, string:to_lower(Path))) of
                    true -> false;
                    false -> true
                end;
           (_,_) -> false
        end,
    add(code:all_loaded(), F).

%% ---------------------------
startfilters(Filters) ->
    F = fun(Module, _Path) ->
            MStr = atom_to_list(Module),
            lists:any(fun(A) -> lists:prefix(A, MStr) end, Filters)
        end,
    add(code:all_loaded(), F).

%% ---------------------------
startfilter(Filter) ->
    F = fun(Module, _Path) ->
            MStr = atom_to_list(Module),
            lists:prefix(Filter, MStr)
        end,
    add(code:all_loaded(), F).

%% ---------------------------
add([], _F) -> ok;
add([{Module, _Path} | Rest], F) ->
    case F(Module) of
        true ->
            io:format("~p~n", [Module]),
            addmulti({Module});
        false ->
            ok
    end,
    add(Rest, F).

%% ---------------------------
addmulti({TraceModule, Func, Ar}) ->
    startinternal({TraceModule, Func, Ar}, [local], {multimodule, add});
addmulti({TraceModule, Func}) ->
    startinternal({TraceModule, Func, '_'}, [local], {multimodule, add});
addmulti({TraceModule}) ->
    startinternal({TraceModule, '_', '_'}, [local], {multimodule, add});
addmulti(TraceModule) ->
    addmulti({TraceModule}).

%% ---------------------------
%% @private
startinternal({TraceModule, Func, Ar}, Opts, {onemodule}) ->
    prepareProc(TraceModule),
    PName1 = getname1(),
    {ok, whereis(PName1), configTrace({TraceModule, Func, Ar}, Opts)};
%%
startinternal({TraceModule, Func, Ar}, Opts, {multimodule, Mode}) ->
    case Mode of
        add -> ok;
        restart -> stopalltraces()
    end,
    prepareProc(multi3),
    PName1 = getname1(),
    {ok,whereis(PName1),configTrace({TraceModule, Func, Ar}, Opts)}.

%% @private
prepareProc(Procfile) ->
    PName1 = getname1(),
    PName2 = getname2(),
    case whereis(PName1) of
        undefined ->
            stopinternal(int),
            register(PName1, Pid1=spawn_link(?MODULE, loop1, [startinternal1_module(Procfile)])),
            register(PName2, Pid2=spawn_link(?MODULE, loop2, [startinternal2_module(Procfile)])),
            Pid1 ! {set_writer, Pid2};
        _Pid ->
            PName2 ! {getmodule, self(), Ref=make_ref()},
            receive
                {currentmodule, Procfile, _FileName, Ref} -> ok;
                {currentmodule, _Module, _FileName, Ref} ->
                    stopinternal(int),
                    register(PName1, Pid1 = spawn_link(?MODULE, loop1, [startinternal1_module(Procfile)])),
                    register(PName2, Pid2 = spawn_link(?MODULE, loop2, [startinternal2_module(Procfile)])),
                    Pid1 ! {set_writer, Pid2}
            after
                1000 ->
                    stopinternal(int),
                    register(PName1, Pid1 = spawn_link(?MODULE, loop1, [startinternal1_module(Procfile)])),
                    register(PName2, Pid2 = spawn_link(?MODULE, loop2, [startinternal2_module(Procfile)])),
                    Pid1 ! {set_writer, Pid2}
            end
    end.

%% @private
configTrace({TraceModule, Func, Ar}, Opts) ->
    erlang:trace(all, false, [call]),
    erlang:trace(all, false, [all]),
    PName1 = getname1(),
    {
        erlang:trace(all, true, [call, {tracer, whereis(PName1)}]),
        erlang:trace_pattern({TraceModule, Func, Ar}, [{'_',[],[{return_trace}]}], Opts)
    }.

%% @private
startinternal1_module(_) ->
    #state1{pidwriter=undefined}.

%% @private
startinternal2_module(TraceModule) ->
    FileName = "trace_funs.txt",
    {FilePath,FDescriptor} = ?TU:ensure_trace_file(FileName),
    #state2{descriptor=FDescriptor,currentmodule=TraceModule,currentfile=FilePath,pidstatedict=dict:new()}.

%% --------------------------------
clearfile() ->
    PName2 = getname2(),
    case whereis(PName2) of
        undefined -> not_started;
        _ -> PName2 ! clearfile, ok
    end.

%% --------------------------------
stop() -> stopinternal(ext).
stop({TraceModule, Func, Ar}) ->
    case erlang:module_loaded(TraceModule) of
        false -> module_not_loaded;
        true ->
            erlang:trace_pattern({TraceModule, Func, Ar}, false, [local]),
            erlang:trace_pattern({TraceModule, Func, Ar}, false, [global])
    end;
stop({TraceModule, Func}) ->
    stop({TraceModule, Func, '_'});
stop({TraceModule}) ->
    stop({TraceModule, '_', '_'});
stop(TraceModule) ->
    stop({TraceModule}).

%% @private
stopinternal(Dir) ->
    stopalltraces(),
    PName1 = getname1(),
    PName2 = getname2(),
    stopproc(PName1,Dir),
    stopproc(PName2,Dir).

%% @private
stopproc(PName,Dir) ->
    case whereis(PName) of
        undefined -> process_not_started;
        _ ->
            case Dir of
                ext -> PName ! stop, ok;
                int ->
                    PName ! {stop, self(), Ref=make_ref()},
                    receive
                        {stopped, Ref} -> ok
                    after
                        1000 -> ok
                    end
            end, ok
    end.

%% @private
stopalltraces() ->
    erlang:trace(all, false, [call]),
    erlang:trace_pattern({'_', '_', '_'}, false, [global]),
    erlang:trace_pattern({'_', '_', '_'}, false, [local]).

%% ====================================================================
%% Internal loop functions
%% ====================================================================

%% @private
getname1() -> calltracer3_1.
%% @private
getname2() -> calltracer3_2.

%% -----------------------------------------
loop1(#state1{pidwriter=PidWriter}=State) ->
    receive
        {trace,_,_,_}=M ->
            PidWriter ! {msg,M,localtime_ms()},
            ?MODULE:loop1(State);
        {trace,_,_,_,_}=M ->
            PidWriter ! {msg,M,localtime_ms()},
            ?MODULE:loop1(State);
        {set_writer,Pid} ->
            ?MODULE:loop1(State#state1{pidwriter=Pid});

        stop -> ok;
        {stop, PidFrom, Ref} ->
            PName1 = getname1(),
            unregister(PName1),
            PidFrom ! {stopped, Ref};

        _Term -> ?MODULE:loop2(State)
    end.

%% -----------------------------------------
loop2(#state2{descriptor=F,currentmodule=CurrentModule,currentfile=CurrentFile,pidstatedict=Dict,lastpid=LastPrintedPid}=State) ->
    receive
        {msg,{trace,Pid,call,{Mod,Fun,_Term}},Time} ->
            {Print, Stack1} = case dict:find(Pid, Dict) of
                {ok, {_Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}=Item|Tail]}} ->  {false, [Item#stackitem{size=Size+1}|Tail]};
                {ok, {{Mod,Fun}=_Last, Stack}} when is_list(Stack) -> {false, [#stackitem{curmod=Mod, curfun=Fun, size=1}|Stack]};
                {ok, {_Last, Stack}} when is_list(Stack) -> {true, [#stackitem{curmod=Mod, curfun=Fun, size=1}|Stack]};
                error -> {true, [#stackitem{curmod=Mod, curfun=Fun, size=1}]}
            end,
            Dict1 = dict:store(Pid, {{Mod,Fun}, Stack1}, Dict),
            case Print of
                false -> ?MODULE:loop2(State#state2{pidstatedict=Dict1});
                true ->
                    case Pid == LastPrintedPid of true-> ok; false -> io:format(F, "------------------------------~n", []) end,
                    {{_Y,_M,_D},{H,M,S,Ms}} = Time,
                    %io:format(F, "~n~2..0B:~2..0B:~2..0B.~3..0B    ~p    ----> ~"++ integer_to_list((length(Stack1)-1)*2) ++ "c~p:~p~n", [H,M,S,Ms, Pid,32,Mod,Fun]),
                    io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    ~p    ~"++ integer_to_list((length(Stack1)-1)*2) ++ "c~p:~p~n", [H,M,S,Ms, Pid,32,Mod,Fun]),
                    ?MODULE:loop2(State#state2{pidstatedict=Dict1, lastpid=Pid})
            end;

        {msg,{trace,Pid,return_from,{Mod,Fun,Ar},_Term},Time} ->
            {Print, Shift, Dict1} = case dict:find(Pid, Dict) of
                {ok, {Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}=Item|Tail]=Stack}} when Size > 1 -> {false, (length(Stack)-1)*2, dict:store(Pid, {Last,[Item#stackitem{size=Size-1}|Tail]}, Dict)};
                {ok, {_Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}]=Stack}} when Size =< 1 -> {false, (length(Stack)-1)*2, dict:erase(Pid, Dict)};
                {ok, {Last, [#stackitem{curmod=Mod, curfun=Fun, size=Size}|Tail]=Stack}} when Size =< 1 -> {false, (length(Stack)-1)*2, dict:store(Pid, {Last,Tail}, Dict)};
                {ok, {_Last, Stack}} when is_list(Stack) -> {true, (length(Stack)-1)*2, Dict};
                error -> {false, 50, Dict}
            end,
            case Print of
                true ->
                    case Pid == LastPrintedPid of true-> ok; false -> io:format(F, "------------------------------~n", []) end,
                    {{_Y,_M,_D},{H,M,S,Ms}} = Time,
                    io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    ~p    ~"++ integer_to_list(Shift) ++ "c<- ~p:~p/~p returned~n", [H,M,S,Ms, Pid,32,Mod,Fun,Ar]),
                    ?MODULE:loop2(State#state2{pidstatedict=Dict1, lastpid=Pid});
                false ->
                    ?MODULE:loop2(State#state2{pidstatedict=Dict1})
            end;

        {getmodule,PidFrom,Ref} ->
            PidFrom ! {currentmodule, CurrentModule, CurrentFile, Ref},
            ?MODULE:loop2(State);

        {getstack,PidFrom,Ref} ->
            PidFrom ! {stack, Dict, Ref},
            ?MODULE:loop2(State);

        clearfile ->
            file:close(F),
            {ok, F1} = file:open(CurrentFile, write),
            ?MODULE:loop2(State#state2{descriptor=F1, pidstatedict=dict:new()});

        stop ->
            stoploop2(State);
        {stop, PidFrom, Ref} ->
            stoploop2(State),
            PName2 = getname2(),
            unregister(PName2),
            PidFrom ! {stopped, Ref};

        Term ->
            {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
            io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    unknown message received~n            ~120p~n~n~n",[H,M,S,Ms, Term]),
            ?MODULE:loop2(State)
    end.

%% -----------------------------------------
%% @private
stoploop2(#state2{descriptor=F}) ->
    {{_Y,_M,_D},{H,M,S,Ms}} = localtime_ms(),
    io:format(F, "~2..0B:~2..0B:~2..0B.~3..0B    trace stopped~n", [H,M,S,Ms]),
    file:close(F).

%% @private
localtime_ms() ->
    Now = os:timestamp(),
    localtime_ms(Now).

%% @private
localtime_ms(Now) ->
    {_, _, Micro} = Now,
    {Date, {Hours, Minutes, Seconds}} = calendar:now_to_local_time(Now),
    {Date, {Hours, Minutes, Seconds, Micro div 1000 rem 1000}}.