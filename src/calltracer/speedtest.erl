%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 26.05.2021
%%% @doc

-module(speedtest).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([times/2]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

times(N,Fun) when is_integer(N), N>0, is_function(Fun,0) ->
    timer:tc(fun() -> lists:foreach(fun(_) -> Fun() end, lists:seq(1,N)) end);

times(N,Fun) when is_integer(N), N>0, is_function(Fun,1) ->
    timer:tc(fun() -> lists:foreach(fun(I) -> Fun(I) end, lists:seq(1,N)) end).

%% ====================================================================
%% Internal functions
%% ====================================================================