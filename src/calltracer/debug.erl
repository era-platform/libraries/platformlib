%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 18.06.2021
%%% @doc

-module(debug).
-author('Peter Bukashin <tbotc@yandex.ru>').

-compile([export_all,nowarn_export_all]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% API functions
%% ====================================================================

%% -----------------------
%% returns total queue len of all active procs
get_queuelen() ->
    lists:foldl(fun(P,Acc) ->
        case process_info(P, message_queue_len) of
            undefined -> Acc;
            {message_queue_len,Y} -> Acc + Y
        end
                end, 0, processes()).

%% -----------------------
%% prints total queue len of all active procs
print_queuelen() ->
    R = get_queuelen(),
    ?OUT('$force', "DEBUG. Current queue length: ~p", [R]).

%% -----------------------
%% returns overloaded procs (>100) with queuelen
get_overloaded() -> get_overloaded(100).
get_overloaded(MsgCount) ->
    F = fun(P,Acc) ->
        case process_info(P, message_queue_len) of
            undefined -> Acc;
            {message_queue_len,Y} ->
                case Y of
                    N when N > MsgCount -> [{N,P}|Acc];
                    _ -> Acc
                end
        end end,
    lists:reverse(lists:sort(lists:foldl(F, [], processes()))).

%% -----------------------
%% kills all overloaded procs
kill_overloaded() -> kill_overloaded(100).
kill_overloaded(MsgCount) ->
    F = fun(P) ->
        case process_info(P, message_queue_len) of
            undefined -> ok;
            {message_queue_len,Y} ->
                case Y of
                    N when N > MsgCount -> exit(P,kill);
                    _ -> ok
                end
        end end,
    lists:foreach(F, processes()).

%% -----------------------
%% prints queue len of overloaded procs (>100)
print_overloaded() ->
    ?OUT('$force', "DEBUG. Overloaded procs: ~n~15p", [get_overloaded()]).

%% -----------------------
%% prints queue len of overloaded procs (>100)
print_topoverloaded() ->
    case get_overloaded() of
        [] -> ?OUT('$force', "DEBUG. Overloaded procs not found");
        [{_,Pid}|_] ->
            I = process_info(Pid),
            R = case lists:keyfind(messages, 1, I) of
                    {_,[]} -> I;
                    {_,List} when length(List) < 5 -> I;
                    {_,[M1, M2, M3, M4, M5|_]} -> lists:keystore(messages, 1, I, {messages, [M1, M2, M3, M4, M5,{"..."}]})
                end,
            ?OUT('$force', "DEBUG. Top overloaded proc ~p~n~p", [Pid, R])
    end.

%% ------------------------
%% Reductions of processes
%% ------------------------

current_reductions() ->
    lists:foldl(fun(Pid,Acc) -> case erlang:process_info(Pid,reductions) of undefined -> Acc; {_,N} -> N + Acc end end, 0, erlang:processes()).

%% ---
current_diff_reductions() ->
    current_diff_reductions(1000,10).

current_diff_reductions(Sleep) ->
    current_diff_reductions(Sleep,10).

current_diff_reductions(Sleep,Limit) ->
    %
    R1 = lists:foldl(fun(Pid,Acc) -> case erlang:process_info(Pid,reductions) of undefined -> Acc; {_,N} -> [{Pid,N}|Acc] end end, [], erlang:processes()),
    timer:sleep(Sleep),
    R2 = lists:foldl(fun(Pid,Acc) -> case erlang:process_info(Pid,reductions) of undefined -> Acc; {_,N} -> [{Pid,N}|Acc] end end, [], erlang:processes()),
    %
    Self = self(),
    Fname = get_name_fun(),
    R3 = lists:filtermap(fun({Pid,_}) when Pid==Self -> false;
        ({Pid,N}) ->
            case lists:keyfind(Pid,1,R1) of
                {_,Nr1} ->
                    case N-Nr1==0 of
                        true -> false;
                        false -> {true,{N-Nr1,Pid,Fname(Pid)}}
                    end;
                false ->
                    {true,{N,Pid,Fname(Pid)}}
            end end, R2),
    R4 = lists:reverse(lists:sort(fun({A,_,_},{B,_,_}) when A=<B -> true; (_,_) -> false end,R3)),
    L = erlang:length(R4),
    {R5,_}=
        case L < Limit of
            true -> lists:split(L,R4);
            false -> lists:split(Limit,R4)
        end,
    R5.

process_name(Pid) ->
    Fname = get_name_fun(),
    {_,Links} = process_info(Pid,links),
    {_,GLPid} = process_info(Pid,group_leader),
    lists:foldr(fun({T,P},Acc) -> [{T,P,Fname(Pid)}|Acc] end, [], [{pid,Pid}, {group_leader,GLPid} | lists:map(fun(X) -> {link,X} end, Links)]).

get_name_fun() ->
    RPids = case lists:keyfind(?REGLIB, 1, application:which_applications()) of
                false -> dict:new();
                _ -> dict:from_list(lists:foldl(fun({N,Pids},Acc) -> lists:foldl(fun(Pid,Acc0) -> [{Pid,N}|Acc0] end, Acc, Pids) end, [], ?REGLIB:registered_pids()))
            end,
    fun(Pid) ->
        Opts = case catch erlang:process_info(Pid,dictionary) of
                   {'EXIT',_} -> [];
                   [] -> [];
                   {_,D} ->
                       O1 = [],
                       O2 = case lists:keyfind('$ancestors',1,D) of
                                false -> O1;
                                A -> [A|O1]
                            end,
                       case lists:keyfind('$initial_call',1,D) of
                           false -> O2;
                           I -> [I|O2]
                       end end,
        case catch erlang:process_info(Pid,registered_name) of
            {'EXIT',_} -> [];
            {registered_name,_}=R -> [R|Opts];
            [] ->
                case dict:find(Pid,RPids) of
                    error -> [];
                    {_,Name} -> [{global_name, Name}|Opts]
                end end end.

%% ====================================================================
%% Internal functions
%% ====================================================================
