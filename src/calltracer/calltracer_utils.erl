%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 24.05.2021
%%% @doc

-module(calltracer_utils).
-author('Peter Bukashin <tbotc@yandex.ru>').

-export([ensure_trace_file/1]).

%% ====================================================================
%% Define
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public functions
%% ====================================================================

ensure_trace_file(FileName) ->
    {ok,CWD} = file:get_cwd(),
    FilePath = filename:join([CWD,basiclib_log:get_log_dir("trace"),FileName]),
    ?BLfilelib:ensure_dir(FilePath),
    {ok, F} = file:open(FilePath, write),
    io:format(F, "---------------------------------------------------------------------~n",[]),
    {FilePath,F}.

%% ====================================================================
%% Internal functions
%% ====================================================================