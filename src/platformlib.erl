%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 01.05.2021
%%% @doc platformlib application.
%%%   It is active on all nodes of platform microservices
%%%   It setup basiclib, loggerlib itself.
%%%   Application could be setup by application:set_env:
%%%      log_destination
%%%          descriptor to define log file folder and prefix.
%%%          Default: {'env','plf'}
%%% TODO: log file setup from opts.
%%% TODO: subscribe_helper
%%% TODO: start basiclib, loggerlib, setup log paths
%%% TODO: block connects to/from unknown nodes.
%%% TODO: configuration, domains

-module(platformlib).
-author('Peter <tbotc@yandex.ru>').

-behaviour(application).

-export([start/0,
         stop/0]).

-export([start/2,
         stop/1]).

%% ====================================================================
%% Defines
%% ====================================================================

-include("app.hrl").

%% ====================================================================
%% Public
%% ====================================================================

%% -----------------------------------------------------
%% LOADER FACADE
%% -----------------------------------------------------

%% -------------------------------------
%% Starts application
%% -------------------------------------
start() ->
    case application:ensure_all_started(?APP, permanent) of
        {ok, _Started} -> ok;
        Error -> Error
    end.

%% -------------------------------------
%% Stops application
%% -------------------------------------
stop() -> application:stop(?APP).

%% ===================================================================
%% Callback functions (application)
%% ===================================================================

%% -------------------------------------
%% @doc Starts application
%% -------------------------------------
start(_, _) ->
    setup_start_ts(),
    setup_dependencies(),
    ensure_deps_started(),
    ?SUPV:start_link().

%% -------------------------------------
%% @doc Stops application.
%% -------------------------------------
stop(_State) ->
    ok.

%% ====================================================================
%% Internal
%% ====================================================================

%% --------------------------------------
%% @private
%% setup start_utc to applicaiton's env for monitor purposes
%% --------------------------------------
setup_start_ts() ->
    application:set_env(?APP,start_utc,?BU:timestamp()).

%% --------------------------------------
%% @private
%% setup deps application props
%% --------------------------------------
setup_dependencies() ->
    % basiclib
    %?BU:set_env(?BASICLIB, 'node_index', fun(FileKey,Arg) -> ?MODULE:write(FileKey,Arg) end),
    ?BU:set_env(?BASICLIB, 'log_destination', {env,?BASICLIB}),
    ?BU:set_env(?BASICLIB, 'max_loglevel', 4), % TODO: update from settings ($info)
    %?BU:set_env(?BASICLIB, 'node_index', ),
    % logger
    ?BU:set_env(?LOGGERLIB, 'log_destination', {env,log}),
    ?BU:set_env(?LOGGERLIB, 'basic_log_dir', "log"),
    ?BU:set_env(?LOGGERLIB, 'min_free_disk_space', 3), % TODO: update from settings
    ?BU:set_env(?LOGGERLIB, 'role_log_store_days', 2), % TODO: update from settings
    ?BU:set_env(?LOGGERLIB, 'role_log_store_max_size', 10485760), % TODO: update from settings
    ?BU:set_env(?LOGGERLIB, 'move_to_filestorage_function', fun(_FilePath) -> ok end),
    % cfglib
    ?BU:set_env(?CFGLIB, 'log_destination', {env,cfg}),
    % reglib
    ?BU:set_env(?REGLIB, 'log_destination', {env,gnr}),
    ?BU:set_env(?REGLIB, 'get_server_nodes_function', fun ?GN_CB:get_server_nodes_function/0),
    ?BU:set_env(?REGLIB, 'get_sync_nodes_function', fun ?GN_CB:get_sync_nodes_function/0),
    ok.

%% --------------------------------------
%% @private
%% starts deps applications, that are not linked to app, and to ensure
%% --------------------------------------
ensure_deps_started() ->
    {ok,_} = application:ensure_all_started(?BASICLIB, permanent),
    {ok,_} = application:ensure_all_started(?LOGGERLIB, permanent),
    {ok,_} = application:ensure_all_started(?CFGLIB, permanent),
    {ok,_} = application:ensure_all_started(?REGLIB, permanent),
    ok.
