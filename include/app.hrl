%%% coding: utf-8
%%% -------------------------------------------------------------------
%%%
%%% Copyright (c) 2021 Peter Bukashin.
%%%
%%% This file is provided to you under the Apache License,
%%% Version 2.0 (the "License"); you may not use this file
%%% except in compliance with the License.  You may obtain
%%% a copy of the License at
%%%
%%%   http://www.apache.org/licenses/LICENSE-2.0
%%%
%%% Unless required by applicable law or agreed to in writing,
%%% software distributed under the License is distributed on an
%%% "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
%%% KIND, either express or implied.  See the License for the
%%% specific language governing permissions and limitations
%%% under the License.
%%%
%%% -------------------------------------------------------------------
%%% @author Peter Bukashin <tbotc@yandex.ru>
%%% @date 04.03.2021
%%% @doc

%% ====================================================================
%% Types
%% ====================================================================

%% ====================================================================
%% Constants and defaults
%% ====================================================================

-define(StoragesCN, <<"storages">>).
-define(ClassesCN, <<"classes">>).
-define(DomainsCN, <<"domains">>).

-define(MsvcDms, <<"dms">>).
-define(MsvcSubscr, <<"subscr">>).
-define(MsvcDc, <<"dc">>).

-define(AttachesTempDirectory, ?BU:str("/tmp/era/~ts/attaches",[node()])).

%% ====================================================================
%% Define modules
%% ====================================================================

-define(BASICLIB, basiclib).
-define(LOGGERLIB, loggerlib).
-define(CFGLIB, cfglib).
-define(REGLIB, reglib).
-define(S3LIB, s3lib).

-define(APP, platformlib).

-define(SUPV, platformlib_supv).

-define(LeaderSrv, platformlib_leader_srv).

-define(PCFG, platformlib_config).

-define(GN_REG, platformlib_globalnames_registrar).
-define(GN_CB, platformlib_globalnames_reglib_callbacks).
-define(GN_NAMING, platformlib_globalnames_naming).
-define(GLOBAL, platformlib_globalnames_global).

-define(DMS_CACHE, platformlib_dms_cache).
-define(DMS_CACHE_SYNC, platformlib_dms_cache_sync).
-define(DMS_SUBSCR, platformlib_dms_subscribe_helper).
-define(DMS_FIXTURER_SUPV, platformlib_dms_fixturer_supv).
-define(DomainTreeCallback, platformlib_dms_fixturer_domaintree_callback).
-define(DMS_FIXTURER_DSRV, platformlib_dms_fixturer_domain_srv).
-define(DMS_FIXTURE_HELPER, platformlib_dms_fixture_helper).
-define(DMS_CRUD, platformlib_dms_crud).
-define(DMS_ATTACH, platformlib_dms_crud_attach).
-define(FILE_AUTODELETE_SRV, platformlib_dms_crud_attach_autodelete_srv).

-define(DTREE_SUPV, platformlib_domaintree_supv).
-define(DTREE_DSUPV, platformlib_domaintree_domain_supv).
-define(DTREE_DSRV, platformlib_domaintree_domain_srv).

-define(StorageUtils, platformlib_filestorage_drivers_utils).
-define(STORAGE_S3, platformlib_filestorage_drivers_s3).
-define(STORAGE_NFS, platformlib_filestorage_drivers_nfs).
-define(STORAGE_FS, platformlib_filestorage_drivers_fs).
-define(STORAGE_FSync, platformlib_filestorage_drivers_fsync).
-define(FS, platformlib_filestorage_drivers_fs_client).
-define(COPIER, platformlib_filestorage_copier_srv).

%% ------
%% From basiclib
%% ------
-define(BU, basiclib_utils).
-define(BLsrv, basiclib_srv).
-define(BLmulticall, basiclib_multicall).
-define(BLstore, basiclib_store).
-define(BLmonitor, basiclib_monitor_srv).
-define(BLping, basiclib_ping).
-define(BLrpc, basiclib_rpc).
-define(BLlog, basiclib_log).
-define(BLfile, basiclib_wrapper_file).
-define(BLfilelib, basiclib_wrapper_filelib).

%% -------
%% From cfglib
%% -------
-define(CFGF, cfglib_f).

%% -------
%% From s3lib
%% -------
-define(S3, s3lib).

%% ====================================================================
%% Define logs
%% ====================================================================

-define(LOGFILE, {env,?APP}).

-define(LOG(Level,Fmt,Args), ?BLlog:write(Level, ?LOGFILE, {Fmt,Args})).
-define(LOG(Level,Text), ?BLlog:write(Level, ?LOGFILE, Text)).

-define(OUT(Level,Fmt,Args), ?BLlog:writeout(Level, ?LOGFILE, {Fmt,Args})).
-define(OUT(Level,Text), ?BLlog:writeout(Level, ?LOGFILE, Text)).

%% ====================================================================
%% Define other
%% ====================================================================
